package com.mpose.com.mpose.salesinvoice.actionlistener;

import com.mpose.com.mpose.pos.models.CustomerSalesInvoice;

import java.util.List;

import io.realm.RealmResults;

/**
 * Created by lp-ritesh on 18/7/17.
 */

public interface ISalesInvoiceActionListener<T> {

    void setView(T t);

    void getCustomerInvoiceList();

    void sortListByTimeStamp();

    RealmResults<CustomerSalesInvoice> getFilteredList(String input);

    List<CustomerSalesInvoice> getAllInvoicesList();

    void checkSalesInvoicesSyncStatus();
}

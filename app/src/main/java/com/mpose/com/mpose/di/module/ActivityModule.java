package com.mpose.com.mpose.di.module;

import android.app.Activity;

import com.mpose.com.mpose.di.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lp-ritesh on 23/6/17.
 */

@Module
public class ActivityModule {

    public final Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    @PerActivity
    Activity activity() {
        return this.activity;
    }

}
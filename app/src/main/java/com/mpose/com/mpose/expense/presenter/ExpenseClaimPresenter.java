package com.mpose.com.mpose.expense.presenter;

import android.content.Context;

import com.mpose.com.mpose.common.Presenter;
import com.mpose.com.mpose.di.component.DashboardComponent;
import com.mpose.com.mpose.expense.actionlistener.ExpenseClaimActionListener;
import com.mpose.com.mpose.expense.models.addexpense.CustomerExpenseModel;
import com.mpose.com.mpose.expense.views.ExpenseClaimView;

import java.util.Collections;
import java.util.Comparator;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by lp-ritesh on 21/8/17.
 */

public class ExpenseClaimPresenter extends Presenter implements
        ExpenseClaimActionListener<ExpenseClaimView> {

    private Context mContext;
    private DashboardComponent mDashboardComponent;
    private Realm mRealm;
    private ExpenseClaimView mExpenseClaimView;
    private RealmList<CustomerExpenseModel> mRealmList;

    @Inject
    public ExpenseClaimPresenter(Context mContext) {
        this.mContext = mContext;
        mRealm = Realm.getDefaultInstance();
        mRealmList = new RealmList<>();
    }

    @Override
    public void setView(ExpenseClaimView expenseClaimView) {
        this.mExpenseClaimView = expenseClaimView;
    }

    @Override
    public RealmList<CustomerExpenseModel> getAllExpenseClaimList() {
        RealmResults<CustomerExpenseModel> modelRealmResults = mRealm.where(CustomerExpenseModel.class).findAll();
        mRealmList.clear();
        mRealmList.addAll(modelRealmResults);
        return mRealmList;
    }

    @Override
    public void fetchSortedList() {
        try {
            Collections.sort(mRealmList, new Comparator<CustomerExpenseModel>() {
                @Override
                public int compare(CustomerExpenseModel obj1, CustomerExpenseModel obj2) {
                    return obj2.getExpenseTimeStamp().compareTo(obj1.getExpenseTimeStamp());
                }
            });
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
        mExpenseClaimView.callAdapter(mRealmList);
    }

    @Override
    public double getTotalClaimAmount() {
        return calculateTotal();
    }

    private double calculateTotal() {

        double total = 0;
        RealmResults<CustomerExpenseModel> modelRealmResults = mRealm.where(CustomerExpenseModel.class).findAll();
        for (CustomerExpenseModel model : modelRealmResults) {
            total = total + model.getClaimAmount();
        }
        return total;
    }
}

package com.mpose.com.mpose.dashboard.model.report;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by lp-ritesh on 25/7/17.
 */




public class ReportDetailModel extends RealmObject{

    @SerializedName("message")
    private ReportStatusMessage message;

    public ReportStatusMessage getMessage() {
        return message;
    }

    public void setMessage(ReportStatusMessage message) {
        this.message = message;
    }
}
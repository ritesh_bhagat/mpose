package com.mpose.com.mpose.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mpose.com.mpose.MposeApplication;
import com.mpose.com.mpose.R;
import com.mpose.com.mpose.eventbus.AddItemCountEvent;
import com.mpose.com.mpose.eventbus.CheckUserSelectedEvent;
import com.mpose.com.mpose.eventbus.ListReachedEndEvent;
import com.mpose.com.mpose.eventbus.ManageViewEvent;
import com.mpose.com.mpose.pos.models.Customer;
import com.mpose.com.mpose.pos.models.CustomerSalesInvoice;
import com.mpose.com.mpose.pos.models.Product;
import com.mpose.com.mpose.utils.AppConstants;
import com.mpose.com.mpose.utils.CommonAlertDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;


/**
 * Created by lp-ritesh on 1/7/17.
 */

public class POSAdapter extends RecyclerView.Adapter<POSAdapter.ViewHolder> {

    private static final String TAG = POSAdapter.class.getSimpleName();
    private Context mContext;
    private List<Product> mAllProductsDataList = new ArrayList<>();
    private Map<String, CustomerSalesInvoice> mAllCartItemsList = new HashMap<>();
    private int lastPosition = -1;
    public static boolean mIsCalledFromGrid = false;
    private boolean isUserSelected = false;
    private CommonAlertDialog commonAlertDialog;

    private Realm realm;


    public POSAdapter(Context context, List<Product> productList) {
        this.mContext = context;
        this.mAllProductsDataList = productList;
        EventBus.getDefault().register(this);
        realm = Realm.getDefaultInstance();

    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        EventBus.getDefault().unregister(this);
    }

    public void readProductData() {
        RealmResults<Product> realmResults = realm.where(Product.class).findAll();
        mAllProductsDataList.addAll(realmResults);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (viewType == 0) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.gridview_item_row, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_row_item, parent, false);
        }
        return new ViewHolder(itemView);
    }

    public void newProductList(List<Product> newProductList) {
        this.mAllProductsDataList = newProductList;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Product allProducts = mAllProductsDataList.get(position);
        holder.productPrice.setText(mContext.getResources().getString(R.string.rs) + " " + allProducts.getPrice());
        holder.productName.setText(allProducts.getItemName());

        Glide.with(mContext).load(allProducts.getImage())
                .placeholder(R.drawable.placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(false)
                .into(holder.productImage);


        holder.productQuantity.setText(String.valueOf(allProducts.getQty()));

        RealmQuery<Product> realmQuery = realm.where(Product.class)
                .contains(AppConstants.PRODUCT_NAME_FIELD_NAME, allProducts.getName());
        int count = realmQuery.findFirst().getItemCount();
        holder.totalProductCount.setText(String.valueOf(count));

        holder.itemLayout.setTag(allProducts);
        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isCustomerExistInDB()) {
                    showAlertDialogForCustNotExist();
                    return;
                }

                if (!isCustomerSelected()) {
                    showAlertDialog();
                    return;
                }

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        final Product product = (Product) holder.minusButton.getTag();
                        product.setItemCount(product.getItemCount() + 1);
                        EventBus.getDefault().post(new AddItemCountEvent(allProducts.getName(), true, 0, true));
                    }
                });
            }
        });

        holder.plusButton.setTag(allProducts);
        holder.plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isCustomerExistInDB()) {
                    showAlertDialogForCustNotExist();
                    return;
                }

                if (!isCustomerSelected()) {
                    showAlertDialog();
                    return;
                }

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        final Product product = (Product) holder.minusButton.getTag();
                        product.setItemCount(product.getItemCount() + 1);
                        EventBus.getDefault().post(new AddItemCountEvent(allProducts.getName(), true, 0, true));
                    }
                });

            }
        });

        holder.minusButton.setTag(allProducts);
        holder.minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (allProducts.getItemCount() <= 0) {
                    return;
                }
                if (!isCustomerExistInDB()) {
                    showAlertDialogForCustNotExist();
                    return;
                }

                if (!isCustomerSelected()) {
                    showAlertDialog();
                    return;
                }

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        final Product product = (Product) holder.minusButton.getTag();
                        product.setItemCount(product.getItemCount() - 1);
                        EventBus.getDefault().post(new AddItemCountEvent(allProducts.getName(), true, 0, true));
                    }
                });
            }
        });

        if (position >= mAllProductsDataList.size() - 1) {
            EventBus.getDefault().post(new ListReachedEndEvent(mAllProductsDataList.size(), false));
        }
    }

    private boolean isCustomerSelected() {
        return ((MposeApplication) mContext.getApplicationContext()).isCustomerSelected();

    }

    private boolean isCustomerExistInDB() {
        boolean flag = false;
        String name = ((MposeApplication) mContext.getApplicationContext()).getCustomerName();
        if (TextUtils.isEmpty(name)) {
            return flag;
        }
        RealmQuery<Customer> query = realm.where(Customer.class).contains(AppConstants.CUSTOMER_NAME_FIELD_NAME, name);
        RealmResults<Customer> results = query.findAll();
        for (Customer customer : results) {
            if (customer.getCustomerName().toLowerCase().trim().equalsIgnoreCase(name.toLowerCase().trim())) {
                flag = true;
            }
        }
        return flag;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    private void showAlertDialog() {

        CommonAlertDialog.ShowAlertDialog(mContext, mContext.getString(R.string.select_user),
                mContext.getString(R.string.select_user_message),
                R.style.dialog_animation_slide_down_up);
    }

    private void showAlertDialogForCustNotExist() {
        CommonAlertDialog.ShowAlertDialog(mContext, mContext.getString(R.string.select_user),
                mContext.getString(R.string.user_does_not_exists),
                R.style.dialog_animation_slide_down_up);

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void checkUserSelection(CheckUserSelectedEvent checkUserSelectedEvent) {
        isUserSelected = checkUserSelectedEvent.isSelected();
    }

    public void setList(List<Product> allCartItemsList) {
        this.mAllProductsDataList = allCartItemsList;
    }

    @Override
    public int getItemViewType(int position) {
        if (mIsCalledFromGrid) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void manageView(ManageViewEvent manageViewEvent) {
        mIsCalledFromGrid = manageViewEvent.isIsCalledFromGrid();
    }

    public int calculateTotal(String name) {
        int total = 0;

        RealmQuery<Product> realmQuery = realm.where(Product.class)
                .contains(AppConstants.PRODUCT_NAME_FIELD_NAME, name);

        return total;
    }


    @Override
    public int getItemCount() {
        return mAllProductsDataList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.txt_product_name)
        TextView productName;

        @Bind(R.id.txt_list_product_qty)
        TextView productQuantity;

        @Bind(R.id.txt_item_cost)
        TextView productPrice;

        @Bind(R.id.image_grid_plus_btn)
        ImageView plusButton;

        @Bind(R.id.image_grid_minus_btn)
        ImageView minusButton;


        @Bind(R.id.txt_total_product_count)
        TextView totalProductCount;

        @Bind(R.id.img_product)
        ImageView productImage;

        @Bind(R.id.list_add_item_layout)
        LinearLayout itemLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}

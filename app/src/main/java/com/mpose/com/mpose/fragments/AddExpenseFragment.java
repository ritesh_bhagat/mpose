package com.mpose.com.mpose.fragments;

import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.mpose.com.mpose.R;
import com.mpose.com.mpose.dashboard.model.possetting.ExpenseTypeModel;
import com.mpose.com.mpose.di.HasComponent;
import com.mpose.com.mpose.di.component.DashboardComponent;
import com.mpose.com.mpose.eventbus.ExpenseClaimCallEvent;
import com.mpose.com.mpose.eventbus.ShowCustomeToolbarEvent;
import com.mpose.com.mpose.expense.actionlistener.AddExpenseActionListener;
import com.mpose.com.mpose.expense.views.AddExpenseView;

import org.greenrobot.eventbus.EventBus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.RealmResults;

/**
 * Created by abc on 8/18/2017.
 */

public class AddExpenseFragment extends BaseFragment implements AddExpenseView,
        HasComponent<DashboardComponent>, AdapterView.OnItemSelectedListener {

    @Bind(R.id.txt_from_employee_name_value)
    TextView employeeName;

    @Bind(R.id.sp_exapnse_type)
    Spinner spinnerExpenseType;

    @Bind(R.id.edit_expense_claim_value)
    EditText edExpenseClaimAmount;

    @Bind(R.id.edit_description_detail)
    EditText edDescriptionDetials;

    @Bind(R.id.edit_date_of_expense)
    EditText edDateOfExpanse;

    @Bind(R.id.add_expense_button_save)
    Button expenseSave;

    private static final String TAG = AddExpenseFragment.class.getSimpleName();


    private DashboardComponent mDashboardComponent;
    private RealmResults<ExpenseTypeModel> expanseTypeList;

    private String mExpenseType;


    @Inject
    AddExpenseActionListener mAddExpenseActionListener;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_expense, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.getComponent(DashboardComponent.class).inject(this);

        mAddExpenseActionListener.setView(this);
        String empName = mAddExpenseActionListener.getLoggedInUserName();
        setEmployeeName(empName);

        mAddExpenseActionListener.getAllExpenseTypes();

        addValuesToExpenseTypeSpinner(expanseTypeList);

        spinnerExpenseType.setOnItemSelectedListener(this);

        setActionBarTitle();

        if (isAdded()) {
            EventBus.getDefault().post(new ShowCustomeToolbarEvent(false));
        }
    }

    private void setActionBarTitle() {
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources()
                    .getString(R.string.add_expense_claim_title));
        }
    }

    @Override
    public void setValuesToSpinner(List<String> list) {
        if (list.size() <= 0) {
            list.add("No Expense Type Available");
        }
        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, list);
        spinnerExpenseType.setAdapter(stringArrayAdapter);
    }

    @OnClick(R.id.add_expense_button_save)
    public void expenseButtonSave() {
        if (!isValidate()) {
            return;
        }
        populateAddExpenseDetails();
    }

    private void populateAddExpenseDetails() {
        mAddExpenseActionListener.saveExpenseDetialsToDB();
    }

    private boolean isValidate() {
        boolean varified = false;

        if (TextUtils.isEmpty(edExpenseClaimAmount.getText())) {
            edExpenseClaimAmount.setError("Plese enter claim amount");
            return varified;
        }
        if (TextUtils.isEmpty(edDescriptionDetials.getText())) {
            edDescriptionDetials.setError("Please enter description");
            return varified;
        }

        if (TextUtils.isEmpty(edDateOfExpanse.getText())) {
            edDateOfExpanse.setError("Please Enter DOE");
            return varified;
        }

        return true;
    }

    @Override
    public String getDescription() {
        return edDescriptionDetials.getText().toString();
    }

    @Override
    public String getDateOfExpense() {
        return edDateOfExpanse.getText().toString();
    }

    @Override
    public String getFromEmployee() {
        return employeeName.getText().toString();
    }

    @Override
    public String getExpenseType() {
        return mExpenseType;
    }

    @Override
    public double getClaimAmount() {
        String amount = edExpenseClaimAmount.getText().toString();
        return round(Double.parseDouble(amount), 2);
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    private void addValuesToExpenseTypeSpinner(RealmResults<ExpenseTypeModel> expanseTypeList) {

    }

    public void setEmployeeName(String name) {
        employeeName.setText(name);
    }


    @Override
    public void onExpenseCreationSuceess() {
        EventBus.getDefault().post(new ExpenseClaimCallEvent());

//        CommonAlertDialog.ShowDialog(getActivity(), "Expense", "Expense Entry created", 0);
    }

    @OnClick(R.id.edit_date_of_expense)
    public void openDatePicker() {
        openDatePickerOnClick();
    }

    public void openDatePickerOnClick() {
        final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar newCalendar = Calendar.getInstance();
        int theme;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            theme = R.style.DialogTheme;
        else
            theme = R.style.PreLollipopDialogTheme;
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), theme, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                newDate.setTime(newDate.getTime());
                newDate.set(Calendar.HOUR_OF_DAY, 0);
                newDate.set(Calendar.MINUTE, 0);
                newDate.set(Calendar.SECOND, 0);
                newDate.set(Calendar.MILLISECOND, 0);
                edDateOfExpanse.setText(dateFormat.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void redirectToLogin() {

    }

    @Override
    public DashboardComponent getComponent() {
        return mDashboardComponent;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mExpenseType = parent.getSelectedItem().toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

package com.mpose.com.mpose.dashboard.model.possetting;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class POSSettingData extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    private int id;


    @SerializedName("selling_price_list")
    private String sellingPriceList;
    @SerializedName("taxes")
    private RealmList<POSSettingTaxis> taxes;
    @SerializedName("expense_type")
    private RealmList<RealmString> expenseType;
    @SerializedName("modes_of_payment")
    private RealmList<ModeOfPaymentModel> modesOfPayment;
    @SerializedName("warehouse")
    private String warehouse;
    @SerializedName("timezone")
    private String timezone;

    public String getSellingPriceList() {
        return sellingPriceList;
    }

    public void setSellingPriceList(String sellingPriceList) {
        this.sellingPriceList = sellingPriceList;
    }

    public RealmList<POSSettingTaxis> getTaxes() {
        return taxes;
    }

    public void setTaxes(RealmList<POSSettingTaxis> taxes) {
        this.taxes = taxes;
    }

    public RealmList<RealmString> getExpenseType() {
        return expenseType;
    }

    public void setExpenseType(RealmList<RealmString> expenseType) {
        this.expenseType = expenseType;
    }

    public RealmList<ModeOfPaymentModel> getModesOfPayment() {
        return modesOfPayment;
    }

    public void setModesOfPayment(RealmList<ModeOfPaymentModel> modesOfPayment) {
        this.modesOfPayment = modesOfPayment;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}


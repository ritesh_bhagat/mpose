package com.mpose.com.mpose.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mpose.com.mpose.R;
import com.mpose.com.mpose.adapters.RecyclerViewAdapter;
import com.mpose.com.mpose.dashboard.actionlistener.AllProductListActionListener;
import com.mpose.com.mpose.dashboard.actionlistener.IListViewItemActionListner;
import com.mpose.com.mpose.dashboard.presenter.ListViewItemPresenter;
import com.mpose.com.mpose.dashboard.view.ListViewItemView;
import com.mpose.com.mpose.di.HasComponent;
import com.mpose.com.mpose.di.component.DashboardComponent;
import com.mpose.com.mpose.pos.models.Product;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by lp-ritesh on 1/7/17.
 */

public class ListViewItemFragment extends BaseFragment implements ListViewItemView,
        HasComponent<DashboardComponent>, AllProductListActionListener {

    @Bind(R.id.item_recycler_view)
    RecyclerView recyclerView;


    private RecyclerViewAdapter recyclerViewAdapter;

    private List<Product> allItemsArrayList = new ArrayList<>();

    @Inject
    IListViewItemActionListner iListViewItemActionListner;

    private ListViewItemPresenter listViewItemPresenter;
    private DashboardComponent mDashboardComponent;

    public ListViewItemFragment() {
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getComponent(DashboardComponent.class).inject(this);
//        EventBus.getDefault().register(this);
//       allItemsArrayList  = getArguments().getParcelableArray("AllProductList");


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerViewAdapter = new RecyclerViewAdapter(getActivity(), allItemsArrayList);
        recyclerView.setAdapter(recyclerViewAdapter);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void redirectToLogin() {

    }

    @Override
    public DashboardComponent getComponent() {
        return mDashboardComponent;
    }


    @Override
    public void onDestroy() {
//        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void allProductList(List<Product> allProductsDatas) {

    }
}

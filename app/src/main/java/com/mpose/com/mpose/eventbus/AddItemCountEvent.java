package com.mpose.com.mpose.eventbus;

/**
 * Created by lp-ritesh on 14/7/17.
 */

public class AddItemCountEvent {

    private String mName;
    private String mId;

    private boolean mIsAdd;
    private int mItemCount;
    private boolean mIsEdited;


    public AddItemCountEvent(String name, boolean isAdd, int itemCount, boolean isEdited) {
        this.mName = name;
        this.mIsAdd = isAdd;
        this.mItemCount = itemCount;
        this.mIsEdited = isEdited;
    }

    public String getName() {
        return mName;
    }

    public boolean isIsAdd() {
        return mIsAdd;
    }

    public int getItemCount() {
        return mItemCount;
    }

    public boolean isIsEdited() {
        return mIsEdited;
    }
}


package com.mpose.com.mpose.expense.models.addexpense;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lp-ritesh on 21/8/17.
 */

public class CustomerExpenseModel extends RealmObject {

    @PrimaryKey
    @SerializedName("expenseId")
    Integer expenseID;

    @SerializedName("postingDate")
    String postingDate;

    @SerializedName("fromEmployee")
    String fromEmployee;

    @SerializedName("expenseType")
    String expenseType;

    @SerializedName("claimAmount")
    double claimAmount;

    @SerializedName("description")
    String claimDescription;

    @SerializedName("current_timestamp")
    private String currentTimeStamp;

    @SerializedName("device_id")
    private String deviceID;

    @SerializedName("isExpenseIDSync")
    private Boolean isExpenseIdSync = false;

    @SerializedName("isPaymentInitialte")
    private Boolean isPaymentInitialte = false;

    @SerializedName("expenseTimestamp")
    private String expenseTimeStamp;

    public Boolean getPaymentInitialte() {
        return isPaymentInitialte;
    }

    public void setPaymentInitialte(Boolean paymentInitialte) {
        isPaymentInitialte = paymentInitialte;
    }

    public String getExpenseTimeStamp() {
        return expenseTimeStamp;
    }

    public void setExpenseTimeStamp(String expenseTimeStamp) {
        this.expenseTimeStamp = expenseTimeStamp;
    }

    public Boolean getExpenseIdSync() {
        return isExpenseIdSync;
    }

    public void setExpenseIdSync(Boolean expenseIdSync) {
        isExpenseIdSync = expenseIdSync;
    }


    public String getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(String postingDate) {
        this.postingDate = postingDate;
    }

    public String getFromEmployee() {
        return fromEmployee;
    }

    public void setFromEmployee(String fromEmployee) {
        this.fromEmployee = fromEmployee;
    }

    public String getExpenseType() {
        return expenseType;
    }

    public void setExpenseType(String expenseType) {
        this.expenseType = expenseType;
    }

    public double getClaimAmount() {
        return claimAmount;
    }

    public void setClaimAmount(double claimAmount) {
        this.claimAmount = claimAmount;
    }

    public String getClaimDescription() {
        return claimDescription;
    }

    public void setClaimDescription(String claimDescription) {
        this.claimDescription = claimDescription;
    }

    public String getStringExpenseID() {
        return deviceID + "-" + currentTimeStamp + "-" + expenseID;
    }

    public Integer getExpenseID() {
        return expenseID;
    }

    public void setExpenseID(Integer expenseID) {
        this.expenseID = expenseID;
    }

    public String getCurrentTimeStamp() {
        return currentTimeStamp;
    }

    public void setCurrentTimeStamp(String currentTimeStamp) {
        this.currentTimeStamp = currentTimeStamp;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    @Override
    public String toString() {
        return "CustomerExpenseModel{" +
                "postingDate='" + postingDate + '\'' +
                ", fromEmployee='" + fromEmployee + '\'' +
                ", expenseType='" + expenseType + '\'' +
                ", claimAmount=" + claimAmount +
                ", claimDescription='" + claimDescription + '\'' +
                '}';
    }
}

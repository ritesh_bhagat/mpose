package com.mpose.com.mpose.eventbus;

/**
 * Created by lp-ritesh on 24/7/17.
 */

public class CustomerInvoiceCallEvent {

    private int mInvoiceNumber;
    private double mGrandTotal;
    private String mStatus;

    public CustomerInvoiceCallEvent(int invoiceNumber, double grandTotal, String status) {
        this.mInvoiceNumber = invoiceNumber;
        this.mGrandTotal = grandTotal;
        this.mStatus = status;
    }

    public int getmInvoiceNumber() {
        return mInvoiceNumber;
    }

    public void setmInvoiceNumber(int mInvoiceNumber) {
        this.mInvoiceNumber = mInvoiceNumber;
    }

    public double getmGrandTotal() {
        return mGrandTotal;
    }

    public void setmGrandTotal(double mGrandTotal) {
        this.mGrandTotal = mGrandTotal;
    }

    public String getmStatus() {
        return mStatus;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }
}

package com.mpose.com.mpose.fragments;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.mpose.com.mpose.R;
import com.mpose.com.mpose.activity.BaseActivity;
import com.mpose.com.mpose.dashboard.actionlistener.IDashboardActionListener;
import com.mpose.com.mpose.dashboard.model.possetting.POSSettingModel;
import com.mpose.com.mpose.dashboard.model.report.ReportDetailModel;
import com.mpose.com.mpose.dashboard.view.DashboardView;
import com.mpose.com.mpose.di.HasComponent;
import com.mpose.com.mpose.di.component.DashboardComponent;
import com.mpose.com.mpose.eventbus.ShowCustomeToolbarEvent;
import com.mpose.com.mpose.utils.CommonAlertDialog;
import com.mpose.com.mpose.utils.DayAxisValueFormatter;
import com.mpose.com.mpose.utils.MyAxisValueFormatter;

import org.greenrobot.eventbus.EventBus;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

/**
 * Created by lp-ritesh on 6/7/17.
 */

public class DashboardFragment extends BaseFragment implements OnChartGestureListener,
        OnChartValueSelectedListener, HasComponent<DashboardComponent>, DashboardView {

    private static final String TAG = DashboardFragment.class.getSimpleName();

    private MaterialDialog mMaterialDialog;

    @Bind(R.id.barchart)
    BarChart mChart;

    @Bind(R.id.line_chart_layout)
    RelativeLayout mChartRelativeaLayout;

    @Bind(R.id.chart_spinner)
    Spinner mWeekSpinner;

    @Bind(R.id.et_start)
    EditText mStartDate;

    @Bind(R.id.et_end)
    EditText mEndDate;

    @Inject
    IDashboardActionListener mDashboardActionListener;

    private DashboardComponent mDashboardComponent;
    private Realm mRealm;

    private ArrayList<BarEntry> mChartEntries = new ArrayList<>();
    private List<String> mPostingDatesList = new ArrayList<>();
    private List<Float> mNumberOfSale = new ArrayList<>();
    private List<String> mWeekReport = new ArrayList<>();

    private static final int ANIMATION_TIME = 1000;

    private ReportDetailModel mReportDetailModel;

    private BarDataSet barDataSet;
    private BarData barData;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    private void initMaterialDialog() {
        mMaterialDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.loading_data)
                .content(R.string.please_wait)
                .progress(true, 0)
                .progressIndeterminateStyle(false)
                .build();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.getComponent(DashboardComponent.class).inject(this);
        mRealm = Realm.getDefaultInstance();

        mDashboardActionListener.setView(this);

        setActionBarTitle();

        initMaterialDialog();

        initChart();

        addItemsToSpinner();

        setSpinnerAdapter();

        mDashboardActionListener.resume();

        if (isAdded()) {
            EventBus.getDefault().post(new ShowCustomeToolbarEvent(false));
        }
    }

    private void setSpinnerAdapter() {
        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, mWeekReport);
        mWeekSpinner.setAdapter(stringArrayAdapter);

    }

    private void setActionBarTitle() {
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_activity_dashboard);
        }
    }

    @Override
    public void showChart(ArrayList<BarEntry> mChartEntries, List<String> mPostingDatesList) {
        manageChart(mChartEntries, mPostingDatesList);
    }

    @Override
    public String getStartDate() {
        return mStartDate.getText().toString();
    }

    @Override
    public String getEndDate() {
        return mEndDate.getText().toString();
    }

    private void addItemsToSpinner() {
        mWeekReport.clear();
        mWeekReport.add("Weekly Report");
    }

    private void manageChart(ArrayList<BarEntry> mChartEntries, List<String> mPostingDatesList) {

        Log.d(TAG, "manageChart: mChartEntries" + mChartEntries.size() + "mPostingDatesList" + mPostingDatesList.size());

        IAxisValueFormatter xAxisFormatter = new DayAxisValueFormatter(mChart, mPostingDatesList);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(true);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);
        xAxis.setTextSize(16f);
        xAxis.setValueFormatter(xAxisFormatter);

        IAxisValueFormatter custom = new MyAxisValueFormatter();


        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setLabelCount(8, false);
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setTextSize(16f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(8, false);
        rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(15f);
        rightAxis.setTextSize(16f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(14f);
        l.setXEntrySpace(4f);

        barDataSet = new BarDataSet(mChartEntries, "Weekly report");


        barDataSet.setValueTextSize(16f);
        barData = new BarData(barDataSet);
        mChart.getXAxis().setValueFormatter(xAxisFormatter);
        mChart.setData(barData);
        mChart.invalidate();

        hideLoading();
    }

    private void initChart() {
        showLoading();
        mChart.setOnChartValueSelectedListener(this);
        mChart.setDrawBarShadow(false);
        mChart.getDescription().setEnabled(false);
        mChart.setPinchZoom(true);
        mChart.setDrawGridBackground(false);
        mChart.getAxisRight().setEnabled(false);
        mChart.animateXY(ANIMATION_TIME, ANIMATION_TIME);
        mChart.getAxisLeft().setAxisMinimum(0f);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onChartGestureStart(MotionEvent me,
                                    ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartGestureEnd(MotionEvent me,
                                  ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartLongPressed(MotionEvent me) {

    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {

    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {

    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {

    }


    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }

    @Override
    public DashboardComponent getComponent() {
        return mDashboardComponent;
    }

    @Override
    public void showLoading() {
        mMaterialDialog.show();
    }

    @Override
    public void hideLoading() {
        if (mMaterialDialog.isShowing()) {
            mMaterialDialog.dismiss();
        }
    }

    @Override
    public void showError(String message) {
    }

    @Override
    public void showSessionExpiryDialog() {
        showAlertDialog();
    }

    private void showAlertDialog() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getResources().getString(R.string.session_expire));
        builder.setMessage(getResources().getString(R.string.session_expire_message));
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // continue with delete
                redirectToLogin();
            }
        });
        builder.setIcon(R.mipmap.ic_alert);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void redirectToLogin() {
        ((BaseActivity) getActivity()).navigateToLoginScreen(getActivity());
    }

    @Override
    public void onPosSettingSuccessResponse(final POSSettingModel body) {
    }

    @Override
    public void createCustomerDB() {

    }

    @Override
    public void showNetworkConnectionError() {
        if (getActivity() != null) {
            CommonAlertDialog.ShowAlertDialog(getActivity(), getResources().getString(R.string.internet_connection_title),
                    getResources().getString(R.string.internet_connection_message), R.style.dialog_animation_fade);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mRealm.close();
        if (mMaterialDialog.isShowing()) {
            mMaterialDialog.dismiss();
        }
    }

    @Override
    public void showOnFailureAlert() {
        CommonAlertDialog.ShowAlertDialog(getActivity(), getResources().getString(R.string.connection_timeout_title),
                getResources().getString(R.string.connection_timeout_message), 0);
    }

    @OnClick(R.id.et_start)
    public void onStartDateClicked() {
        final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Calendar newCalendar = Calendar.getInstance();
        int theme;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            theme = R.style.DialogTheme;
        else
            theme = R.style.PreLollipopDialogTheme;
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), theme, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                newDate.setTime(newDate.getTime());
                newDate.set(Calendar.HOUR_OF_DAY, 0);
                newDate.set(Calendar.MINUTE, 0);
                newDate.set(Calendar.SECOND, 0);
                newDate.set(Calendar.MILLISECOND, 0);
                if (newDate.getTime().after(Calendar.getInstance().getTime())) {
                    if (getContext() != null) {
                        Toast.makeText(getContext(), getResources().getString(R.string.invalid_date_error), Toast.LENGTH_SHORT).show();
                    }
                    mStartDate.setText("");
                } else {
                    mStartDate.setText(dateFormat.format(newDate.getTime()));
                    mEndDate.setText("");
                    validateAndReload();
                }
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
        datePickerDialog.show();
    }

    @OnClick(R.id.et_end)
    public void onEndDateClicked() {
        final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Calendar newCalendar = Calendar.getInstance();
        int theme;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            theme = R.style.DialogTheme;
        else
            theme = R.style.PreLollipopDialogTheme;
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), theme, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                newDate.setTime(newDate.getTime());
                if (newDate.getTime().after(Calendar.getInstance().getTime())) {
                    if (getContext() != null) {
                        Toast.makeText(getContext(), getResources().getString(R.string.invalid_date_error), Toast.LENGTH_SHORT).show();
                    }
                    mEndDate.setText("");
                } else {
                    newDate.set(Calendar.HOUR_OF_DAY, 23);
                    newDate.set(Calendar.MINUTE, 59);
                    newDate.set(Calendar.SECOND, 59);
                    mEndDate.setText(dateFormat.format(newDate.getTime()));
                    validateAndReload();
                }
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
        datePickerDialog.show();
    }

    private void validateAndReload() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        if (!mStartDate.getText().toString().isEmpty() && !mEndDate.getText().toString().isEmpty()) {
            try {
                if (dateFormat.parse(mEndDate.getText().toString()).before(dateFormat.parse(mStartDate.getText().toString()))) {
                    Toast.makeText(getContext(), getString(R.string.date_validation_error), Toast.LENGTH_LONG).show();
                    mStartDate.setText("");
                    mEndDate.setText("");
                } else {
                    if (!mStartDate.getText().toString().isEmpty() && !mEndDate.getText().toString().isEmpty()) {
                        mDashboardActionListener.resume();
                    } else {
                        Toast.makeText(getContext(), getString(R.string.date_empty_error), Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.search_item).setVisible(false);
        menu.findItem(R.id.img_create_customer).setVisible(false);
    }
}

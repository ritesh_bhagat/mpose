package com.mpose.com.mpose.dashboard.model.report;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class ReportData extends RealmObject {

    @SerializedName("posting_date")
    private String postingDate;
    @SerializedName("total_sales")
    private Double totalSales;

    public String getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(String postingDate) {
        this.postingDate = postingDate;
    }

    public Double getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(Double totalSales) {
        this.totalSales = totalSales;
    }


}
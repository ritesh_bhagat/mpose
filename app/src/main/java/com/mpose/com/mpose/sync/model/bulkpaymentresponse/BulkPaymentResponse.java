package com.mpose.com.mpose.sync.model.bulkpaymentresponse;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by lp-ritesh on 3/8/17.
 */

public class BulkPaymentResponse extends RealmObject {

    @SerializedName("message")
    private RealmList<BulkPaymentMessage> message;

    public RealmList<BulkPaymentMessage> getMessage() {
        return message;
    }

    public void setMessage(RealmList<BulkPaymentMessage> message) {
        this.message = message;
    }


}


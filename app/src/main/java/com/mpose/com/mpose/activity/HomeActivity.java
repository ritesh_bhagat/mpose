package com.mpose.com.mpose.activity;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.percent.PercentFrameLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.mpose.com.mpose.MposeApplication;
import com.mpose.com.mpose.R;
import com.mpose.com.mpose.di.HasComponent;
import com.mpose.com.mpose.di.component.DaggerDashboardComponent;
import com.mpose.com.mpose.di.component.DashboardComponent;
import com.mpose.com.mpose.eventbus.CustomerInvoiceCallEvent;
import com.mpose.com.mpose.eventbus.CustomerNameEvent;
import com.mpose.com.mpose.eventbus.ExpenseClaimCallEvent;
import com.mpose.com.mpose.eventbus.NavigationDrawerEvent;
import com.mpose.com.mpose.eventbus.SalesInvoiceCallEvent;
import com.mpose.com.mpose.eventbus.ShowCustomeToolbarEvent;
import com.mpose.com.mpose.eventbus.UnpaidSalesPaymentEvent;
import com.mpose.com.mpose.fragments.AddExpenseFragment;
import com.mpose.com.mpose.fragments.CustomerSalesInvoiceFragment;
import com.mpose.com.mpose.fragments.DashboardFragment;
import com.mpose.com.mpose.fragments.ExpenseClaimFragment;
import com.mpose.com.mpose.fragments.POSFragment;
import com.mpose.com.mpose.fragments.SalesInvoiceFragment;
import com.mpose.com.mpose.fragments.SyncFragment;
import com.mpose.com.mpose.fragments.UnpaidSalesInvoiceFragment;
import com.mpose.com.mpose.home.actionlistener.IHomeActionListener;
import com.mpose.com.mpose.home.view.HomeView;
import com.mpose.com.mpose.manager.SharedPrefConstants;
import com.mpose.com.mpose.manager.SharedPreferenceManager;
import com.mpose.com.mpose.receivers.AutoSyncBroadcastReceiver;
import com.mpose.com.mpose.receivers.DbUpdateReceiver;
import com.mpose.com.mpose.retrofit.ApiClient;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkapi.SalesRequestData;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkresponse.SalesInvoiceResponseData;
import com.mpose.com.mpose.sync.model.bulkpaymentrequest.BulkPaymentData;
import com.mpose.com.mpose.utils.AppConstants;
import com.mpose.com.mpose.utils.CommonAlertDialog;
import com.mpose.com.mpose.utils.NetworkConncetionUtility;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmList;

public class HomeActivity extends BaseActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        HomeView, HasComponent<DashboardComponent> {

    private static final String TAG = HomeActivity.class.getSimpleName();

    @Bind(R.id.toolbar)
    public Toolbar mToolbar;

    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @Bind(R.id.nav_view)
    NavigationView mNavigationView;

    @Bind(R.id.frame_container)
    PercentFrameLayout mFrameLayout;

    @Bind(R.id.log_out)
    ImageView mImageLogOut;

    @Bind(R.id.custom_content_toolbar)
    View mCustomToolbar;

    @Inject
    IHomeActionListener mHomeActionListener;

    private TextView mUserFullName;

    private List<String> mSpinnerList;
    private DashboardComponent mDashboardComponent;

    private boolean mDoubleBackToExitPressedOnce;

    private Realm mRealm;
    private MaterialDialog mMaterialDialog;
    private BroadcastReceiver mBroadcastReceiver;
    private BroadcastReceiver mAlarmBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard2);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);

        initializeInjector();
        this.getComponent().inject(this);
        mRealm = Realm.getDefaultInstance();

        setSupportActionBar(mToolbar);

        initMaterialDialog();

        setNavigationDrawer();

        mNavigationView.setNavigationItemSelectedListener(this);


        setOptionsToSpinner();

        setSpinnerAdapter();

        mHomeActionListener.setView(this);


        setNavigationHeaderComponent();

        showDashboardView();

        setNavigationHeaderComponent();

        getIntentsFromLogin();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void showCustomToolbar(ShowCustomeToolbarEvent event) {
        if (event.isVisible()) {
            mCustomToolbar.setVisibility(View.VISIBLE);
        } else {
            mCustomToolbar.setVisibility(View.GONE);
        }

    }

    private void getIntentsFromLogin() {
        Bundle bundle = getIntent().getExtras();
        String name = bundle.getString(AppConstants.USER_FULL_NAME);
        boolean isCalledFromLogin = bundle.getBoolean(AppConstants.LOGIN_CALL);
        if (isCalledFromLogin) {
            setUserFullName(name);
        } else {
            String nameFromDB = mHomeActionListener.getUserName();
            setUserFullName(nameFromDB);
        }
    }

    private void setUserFullName(String name) {
        mUserFullName.setText(name);
    }

    private void setNavigationHeaderComponent() {
        View header = mNavigationView.getHeaderView(0);
        mUserFullName = (TextView) header.findViewById(R.id.txt_user_name);
        TextView userDesignation = (TextView) header.findViewById(R.id.txt_user_designation);

    }

    public void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) HomeActivity.this.getSystemService(
                    Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(mNavigationView.getWindowToken(), 0);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onSuccessSaveCustomerIdToDB(String name) {
        callEventToShowName(name);
    }

    private void callEventToShowName(String name) {
        SharedPreferenceManager.getSharedPreferenceManager(HomeActivity.this)
                .setSelectedCustomer(name);
        EventBus.getDefault().post(new CustomerNameEvent(name));
        hideKeyboard();
        ((MposeApplication) getApplicationContext()).setCustomerSelected(true);
        SharedPreferenceManager.getSharedPreferenceManager(HomeActivity.this).setIsCustomerSelected(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
        SharedPreferenceManager.getSharedPreferenceManager(this)
                .removeEntry(SharedPrefConstants.SELECTED_CUSTOMER);
        if (mMaterialDialog.isShowing()) {
            mMaterialDialog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void showDashboardView() {
        Fragment fragment = null;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragment = new DashboardFragment();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack("DashboardFragment");
        fragmentTransaction.commit();
        mToolbar.setTitle(getString(R.string.report_title));
    }

    private void initializeInjector() {
        this.mDashboardComponent = DaggerDashboardComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .build();
    }


    private void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }


    @Override
    public void setCustomerToDB(String name) {
        mHomeActionListener.saveCustomerIntoDB(name);
    }

    @Override
    public void showCustomerAlreadyPresentError() {
        CommonAlertDialog.ShowAlertDialog(HomeActivity.this, getString(R.string.customer_exist),
                getString(R.string.error_customer_already_present), R.style.dialog_animation_fade);
    }

    @Override
    public void notifyToAdapter() {
    }


    private void setNavigationDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();
    }

    private void setOptionsToSpinner() {
        mSpinnerList = new ArrayList<>();
        mSpinnerList.add(getString(R.string.select_items));
        mSpinnerList.add(getString(R.string.all_categories));

    }

    private void setSpinnerAdapter() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_row_item,
                R.id.text_spinner, mSpinnerList);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.spinner_row_item);
        // attaching data adapter to mSpinner
//        mSpinner.setAdapter(dataAdapter);
    }

    @OnClick(R.id.log_out)
    public void logOut() {
        if (NetworkConncetionUtility.getInstance(this).isConnected()) {
            mHomeActionListener.checkSalesInvoicesStatus();
        } else {
            CommonAlertDialog.ShowAlertDialog(HomeActivity.this, getResources().getString(R.string.internet_connection_title),
                    getResources().getString(R.string.internet_connection_message), R.style.dialog_animation_fade);
        }
    }

    @Override
    public void showAlertDialogBeforeLogout(boolean flag) {
        if (flag) {
            showAlertDialog();
        } else {
            callLogOutApi();
        }
    }

    public void callLogOutApi() {
        mHomeActionListener.logOut();
    }

    private void showAlertDialog() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(HomeActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(AppConstants.SALE_INVOICE_TITLE);
        builder.setMessage(getResources().getString(R.string.si_sync_alert_message));
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // continue with delete
                callLogOutApi();
            }
        });
        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setIcon(R.mipmap.ic_alert);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void showSessionExpiryDialog() {
        showAlertDialogForExpiry();
    }

    private void showAlertDialogForExpiry() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(HomeActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getResources().getString(R.string.session_expire));
        builder.setMessage(getResources().getString(R.string.session_expire_message));
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // continue with delete
                redirectToLogin();
            }
        });
        builder.setIcon(R.mipmap.ic_alert);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onSuccessLogout() {
        /// clear all the user related data only if user name is different

//        SharedPreferenceManager.getSharedPreferenceManager(this).clearAllPreferences();
        ApiClient.resetRetrofit();
        ((MposeApplication) getApplication()).setCustomerSelected(false);
        SharedPreferenceManager.getSharedPreferenceManager(this).setUserLoggedIn(false);
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNavigationDrawerClick(NavigationDrawerEvent navigationDrawerEvent) {

    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                getSupportFragmentManager().popBackStackImmediate();
            } else {
                if (mDoubleBackToExitPressedOnce) {
                    Intent a = new Intent(Intent.ACTION_MAIN);
                    a.addCategory(Intent.CATEGORY_HOME);
                    a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(a);
                }
                this.mDoubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Press BACK again to EXIT", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mDoubleBackToExitPressedOnce = false;
                    }
                }, 1000);
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        // Inflate menu to add items to action bar if it is present.
        inflater.inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Fragment fragment = null;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        int id = item.getItemId();
        switch (id) {
            case R.id.nav_dashboard:
                fragment = new DashboardFragment();
                showNetworkConnectionError();
                fragmentTransaction.addToBackStack("DashboardFragment");
                fragmentManager.popBackStack("DashboardFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                break;
            case R.id.nav_pos:
                fragment = new POSFragment();
                showNetworkConnectionError();
                fragmentTransaction.addToBackStack("POSFragment");
                break;
            case R.id.nav_sales_invoice:
                fragment = new SalesInvoiceFragment();
                showNetworkConnectionError();
                fragmentTransaction.addToBackStack("SalesInvoiceFragement");
                break;
            case R.id.nav_sync:
                if (NetworkConncetionUtility.getInstance(HomeActivity.this).isConnected()) {
                    callSyncFragment();
                    mHomeActionListener.populateSalesInvoiceCreationData();
                } else {
                    CommonAlertDialog.ShowAlertDialog(HomeActivity.this,
                            getResources().getString(R.string.internet_connection_title),
                            getResources().getString(R.string.internet_connection_message), R.style.dialog_animation_fade);
                }
                break;
            case R.id.nav_add_expense:
                fragment = new AddExpenseFragment();
                showNetworkConnectionError();
                fragmentTransaction.addToBackStack("AddExpenseFragment");
                break;

            case R.id.nav_sales_expense_claim:
                fragment = new ExpenseClaimFragment();
                showNetworkConnectionError();
                fragmentTransaction.addToBackStack("ExpenseClaimFragment");
                break;

            case R.id.nav_unpaid_sales_invoice:
                fragment = new UnpaidSalesInvoiceFragment();
                showNetworkConnectionError();
                fragmentTransaction.addToBackStack("UnpaidSalesInvoiceFragment");
                break;

            default:
                break;
        }
        if (fragment != null) {
            fragmentTransaction.replace(R.id.frame_container, fragment);
            fragmentTransaction.commit();
        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showNetworkConnectionError() {
        if (!NetworkConncetionUtility.getInstance(this).isConnected()) {
            CommonAlertDialog.ShowAlertDialog(HomeActivity.this,
                    getResources().getString(R.string.internet_connection_title),
                    getResources().getString(R.string.internet_connection_message),
                    R.style.dialog_animation_fade);
        }
    }

    private void callSyncFragment() {
        //showNetworkConnectionError();
        FragmentManager fm = getSupportFragmentManager();
        SyncFragment fr = new SyncFragment();
        fr.show(fm, AppConstants.SYNC_DIALOG_FRAGMENT);
    }

    @Override
    public void hideSyncDialog() {
        Fragment prev = getSupportFragmentManager().findFragmentByTag(AppConstants.SYNC_DIALOG_FRAGMENT);
        if (prev != null) {
            DialogFragment df = (DialogFragment) prev;
            df.dismiss();
        }
    }

    @Override
    public void showSyncDialog() {
        FragmentManager fm = getSupportFragmentManager();
        SyncFragment fr = new SyncFragment();
        fr.show(fm, AppConstants.SYNC_DIALOG_FRAGMENT);
    }

    @Override
    public void showSucessMessage() {
        CommonAlertDialog.ShowDialog(HomeActivity.this, getResources().getString(R.string.system_update_title),
                getResources().getString(R.string.system_update_message), R.style.dialog_animation_fade);
    }

    @Override
    public void showSyncErrorMessage() {
        CommonAlertDialog.ShowDialog(HomeActivity.this, getResources().getString(R.string.si_error_title),
                getResources().getString(R.string.si_error_message),
                R.style.dialog_animation_fade);

    }


    @Override
    public void showOnFailureAlert() {
        CommonAlertDialog.ShowAlertDialog(HomeActivity.this, getResources().getString(R.string.connection_timeout_title),
                getResources().getString(R.string.connection_timeout_message), 0);
    }

    @Override
    public void apiCallBulkPayment(List<BulkPaymentData> bulkPaymentDataList) {
    }


    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mAlarmBroadcastReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume: ");
        super.onResume();
        IntentFilter filter = new IntentFilter("AUTO_SYNC_ACTION");
        mBroadcastReceiver = new AutoSyncBroadcastReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, filter);
        callAlarm();
    }

    private void callAlarm() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, DbUpdateReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0,
                intent, PendingIntent.FLAG_ONE_SHOT);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 30 * 1000, pendingIntent);
            } else {
                alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 30 * 1000, pendingIntent);
            }
        } else {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 30 * 1000, pendingIntent);
        }
    }


    @Override
    public DashboardComponent getComponent() {
        return mDashboardComponent;
    }

    @Override
    public void showLoading() {
        mMaterialDialog.show();
    }

    @Override
    public void hideLoading() {
        if (mMaterialDialog.isShowing()) {
            mMaterialDialog.dismiss();
        }
    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void redirectToLogin() {
        ((BaseActivity) this).navigateToLoginScreen(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void callCustomerFragment(CustomerInvoiceCallEvent customerInvoiceCallEvent) {
        CustomerSalesInvoiceFragment customerSalesInvoiceFragment = new CustomerSalesInvoiceFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.INVOICE_NUMBER_FIELD_NAME, customerInvoiceCallEvent.getmInvoiceNumber());
        bundle.putDouble(AppConstants.GRAND_TOTAL_FIELD_NAME, customerInvoiceCallEvent.getmGrandTotal());
        bundle.putString(AppConstants.PAYMENT_STATUS, customerInvoiceCallEvent.getmStatus());
        customerSalesInvoiceFragment.setArguments(bundle);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction()
                .replace(R.id.frame_container, customerSalesInvoiceFragment);
        fragmentTransaction.addToBackStack("CustomerSalesInvoiceFragment");
        fragmentTransaction.commit();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void callSalesInvoiceFragment(SalesInvoiceCallEvent salesInvoiceCallEvent) {
        SalesInvoiceFragment customerSalesInvoiceFragment = new SalesInvoiceFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction()
                .replace(R.id.frame_container, customerSalesInvoiceFragment);
        fragmentTransaction.commit();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void callExpenseClaimFragment(ExpenseClaimCallEvent expenseClaimCallEvent) {
        ExpenseClaimFragment fragment = new ExpenseClaimFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction().replace(R.id.frame_container, fragment);
        ft.commit();
    }

    @Override
    public void callCreateSalesInvoiceApi(List<SalesRequestData> salesRequestDataList) {
        mHomeActionListener.callSalesInvoiceBulkApi(salesRequestDataList);
    }

    @Override
    public void changeStatusOfSyncInvoices(RealmList<SalesInvoiceResponseData> responseData) {
    }

    @Override
    public void callCreateSalesReturnInvoiceApi(List<SalesRequestData> salesRequestDataList) {
    }


    private void initMaterialDialog() {
        mMaterialDialog = new MaterialDialog.Builder(HomeActivity.this)
                .title(R.string.logging_out)
                .content(R.string.please_wait)
                .progress(true, 0)
                .progressIndeterminateStyle(false)
                .build();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleUnpaidSalesPayment(UnpaidSalesPaymentEvent event) {
        if (event.getBulkPaymentData() != null) {
            mHomeActionListener.populateUnpaidSalesInvoiceData(event.getBulkPaymentData());
        }
    }

}

package com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkresponse;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

public class SalesInvoiceResponseMessage extends RealmObject {

    @SerializedName("status")
    private String status;

    @SerializedName("data")
    private RealmList<SalesInvoiceResponseData> data;

    @SerializedName("traceback")
    private String traceback;

    @SerializedName("error")
    private String error;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public RealmList<SalesInvoiceResponseData> getData() {
        return data;
    }

    public void setData(RealmList<SalesInvoiceResponseData> data) {
        this.data = data;
    }

    public String getTraceback() {
        return traceback;
    }

    public void setTraceback(String traceback) {
        this.traceback = traceback;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
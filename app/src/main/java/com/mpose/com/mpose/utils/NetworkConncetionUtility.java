package com.mpose.com.mpose.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by lp-ritesh on 2/6/17.
 */

public class NetworkConncetionUtility {

    /**
     * The type wifi.
     */
    private static final int TYPE_WIFI = 1;

    /**
     * The type mobile.
     */
    private static final int TYPE_MOBILE = 2;

    /**
     * The type not connected.
     */
    private static final int TYPE_NOT_CONNECTED = 0;

    /**
     * The Constant STATUS_WIFI_ENABLED.
     */
    private static final String STATUS_WIFI_ENABLED = "Wifi enabled";

    /**
     * The Constant STATUS_MOBILE_DATA_ENABLED.
     */
    private static final String STATUS_MOBILE_DATA_ENABLED = "Mobile data enabled";

    /**
     * The Constant STATUS_NOT_CONNECTED.
     */
    private static final String STATUS_NOT_CONNECTED = "Not connected to Internet";


    /**
     * The s instance.
     */
    private static NetworkConncetionUtility sInstance;

    /**
     * The m context.
     */
    private Context mContext;

    /**
     * Instantiates a new network conncetion utility.
     *
     * @param context the context
     */
    private NetworkConncetionUtility(Context context) {

        mContext = context;
    }

    /**
     * Gets the single instance of NetworkConncetionUtility.
     *
     * @param context the context
     * @return single instance of NetworkConncetionUtility
     */
    public static synchronized NetworkConncetionUtility getInstance(Context context) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new NetworkConncetionUtility(context);
        }
        return sInstance;
    }

    /**
     * Gets the network connection type.
     *
     * @return the connection type
     */
    public int getNetworkConnectionType() {
        ConnectivityManager cm =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                return TYPE_WIFI;
            }

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                return TYPE_MOBILE;
            }
        }

        return TYPE_NOT_CONNECTED;
    }

    /**
     * Gets the connectivity status connected or not.
     *
     * @return the connection status
     */

    public boolean isConnected() {
        ConnectivityManager cm =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (wifiInfo != null && wifiInfo.isConnected() || (mobileInfo != null && mobileInfo.isConnected())) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }

    }

    /**
     * Gets the connectivity status string.
     *
     * @return the connectivity status string
     */
    public String getConnectivityStatusString() {
        int conn = getNetworkConnectionType();
        String status = null;
        if (conn == TYPE_WIFI) {
            status = STATUS_WIFI_ENABLED;
        } else if (conn == TYPE_MOBILE) {
            status = STATUS_MOBILE_DATA_ENABLED;
        } else if (conn == TYPE_NOT_CONNECTED) {
            status = STATUS_NOT_CONNECTED;
        }
        return status;
    }

}

package com.mpose.com.mpose.person.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lp-ritesh on 28/6/17.
 */

public class PersonData {

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private Object lastName;

    @SerializedName("modified_by")
    private String modifiedBy;

    @SerializedName("name")
    private String name;

    @SerializedName("parent")
    private Object parent;

    @SerializedName("creation")
    private String creation;

    @SerializedName("modified")
    private String modified;

    @SerializedName("doctype")
    private String doctype;

    @SerializedName("idx")
    private Object idx;

    @SerializedName("parenttype")
    private Object parenttype;

    @SerializedName("owner")
    private String owner;

    @SerializedName("docstatus")
    private Integer docstatus;

    @SerializedName("parentfield")
    private Object parentfield;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Object getLastName() {
        return lastName;
    }

    public void setLastName(Object lastName) {
        this.lastName = lastName;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getParent() {
        return parent;
    }

    public void setParent(Object parent) {
        this.parent = parent;
    }

    public String getCreation() {
        return creation;
    }

    public void setCreation(String creation) {
        this.creation = creation;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getDoctype() {
        return doctype;
    }

    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }

    public Object getIdx() {
        return idx;
    }

    public void setIdx(Object idx) {
        this.idx = idx;
    }

    public Object getParenttype() {
        return parenttype;
    }

    public void setParenttype(Object parenttype) {
        this.parenttype = parenttype;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Integer getDocstatus() {
        return docstatus;
    }

    public void setDocstatus(Integer docstatus) {
        this.docstatus = docstatus;
    }

    public Object getParentfield() {
        return parentfield;
    }

    public void setParentfield(Object parentfield) {
        this.parentfield = parentfield;
    }
}

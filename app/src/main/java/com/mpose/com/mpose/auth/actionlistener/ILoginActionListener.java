package com.mpose.com.mpose.auth.actionlistener;

import com.mpose.com.mpose.auth.model.login.DomainModel;
import com.mpose.com.mpose.auth.model.login.Login;

/**
 * Created by lp-ritesh on 5/7/17.
 */

public interface ILoginActionListener<T> {

    void setView(T t);

    void forgotPassword(String email);

    void domainGetCall(String strDomain);

    void populateDomainFromDB();

    void domainGetCallForForgotPassword(String strDomain);

    void populateLoginObject(String strUserName, String strPassword, String strDomain);
}

package com.mpose.com.mpose.dashboard.model.report;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

public class ReportStatusMessage extends RealmObject{

    @SerializedName("status")
    private String status;

    @SerializedName("data")
    private RealmList<ReportData> data = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public RealmList<ReportData> getData() {
        return data;
    }

    public void setData(RealmList<ReportData> data) {
        this.data = data;
    }

}
package com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkapi;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SalesRequestData {

    @SerializedName("invoice_id")
    private String invoiceId;

    @SerializedName("customer_name")
    private String customerName;

    @SerializedName("customer_id")
    private String customerId;

    @SerializedName("posting_date")
    private String postingDate;

    @SerializedName("items")
    private List<SalesInvoiceItem> items;

    @SerializedName("payments")
    private List<SalesInvoicePayment> payments;

    @SerializedName("taxes")
    private List<SalesInvoiceTaxis> taxes;

    @SerializedName("selling_price_list")
    private String sellingPriceList;

    @SerializedName("additional_discount_percentage")
    private Double additionalDiscountPercentage;

    @SerializedName("return_against")
    private String returnAgainstInvoiceNumber;

    @SerializedName("is_return")
    private Integer isReturn;


    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(String postingDate) {
        this.postingDate = postingDate;
    }

    public List<SalesInvoiceItem> getItems() {
        return items;
    }

    public void setItems(List<SalesInvoiceItem> items) {
        this.items = items;
    }

    public List<SalesInvoicePayment> getPayments() {
        return payments;
    }

    public void setPayments(List<SalesInvoicePayment> payments) {
        this.payments = payments;
    }

    public List<SalesInvoiceTaxis> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<SalesInvoiceTaxis> taxes) {
        this.taxes = taxes;
    }

    public String getSellingPriceList() {
        return sellingPriceList;
    }

    public void setSellingPriceList(String sellingPriceList) {
        this.sellingPriceList = sellingPriceList;
    }

    public Double getAdditionalDiscountPercentage() {
        return additionalDiscountPercentage;
    }

    public void setAdditionalDiscountPercentage(Double additionalDiscountPercentage) {
        this.additionalDiscountPercentage = additionalDiscountPercentage;
    }

    public String getReturnAgainstInvoiceNumber() {
        return returnAgainstInvoiceNumber;
    }

    public void setReturnAgainstInvoiceNumber(String returnAgainstInvoiceNumber) {
        this.returnAgainstInvoiceNumber = returnAgainstInvoiceNumber;
    }

    public Integer getReturn() {
        return isReturn;
    }

    public void setReturn(Integer aReturn) {
        isReturn = aReturn;
    }
}
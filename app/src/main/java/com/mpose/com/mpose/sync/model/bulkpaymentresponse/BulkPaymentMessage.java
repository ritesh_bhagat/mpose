package com.mpose.com.mpose.sync.model.bulkpaymentresponse;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class BulkPaymentMessage extends RealmObject {

    @SerializedName("status")
    private String status;
    @SerializedName("erp_payment_id")
    private String erpPaymentId;
    @SerializedName("pos_payment_id")
    private String posPaymentId;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErpPaymentId() {
        return erpPaymentId;
    }

    public void setErpPaymentId(String erpPaymentId) {
        this.erpPaymentId = erpPaymentId;
    }

    public String getPosPaymentId() {
        return posPaymentId;
    }

    public void setPosPaymentId(String posPaymentId) {
        this.posPaymentId = posPaymentId;
    }
}
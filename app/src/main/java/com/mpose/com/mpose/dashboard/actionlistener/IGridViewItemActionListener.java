package com.mpose.com.mpose.dashboard.actionlistener;

/**
 * Created by lp-ritesh on 5/7/17.
 */

public interface IGridViewItemActionListener<T> {
    void setView(T t);

    void getAllProducts();
}

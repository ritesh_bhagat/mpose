package com.mpose.com.mpose.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mpose.com.mpose.R;
import com.mpose.com.mpose.adapters.POSAdapter;
import com.mpose.com.mpose.dashboard.actionlistener.IGridViewItemActionListener;
import com.mpose.com.mpose.dashboard.presenter.GridViewItemPresenter;
import com.mpose.com.mpose.dashboard.view.GridViewItemView;
import com.mpose.com.mpose.di.HasComponent;
import com.mpose.com.mpose.di.component.DashboardComponent;
import com.mpose.com.mpose.pos.models.AllItems;
import com.mpose.com.mpose.pos.models.AllProducts;
import com.mpose.com.mpose.pos.models.CustomerSalesInvoice;
import com.mpose.com.mpose.pos.models.Product;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by lp-ritesh on 1/7/17.
 */

public class GridViewItemFragment extends BaseFragment implements GridViewItemView,
        HasComponent<DashboardComponent> {

    private static final String TAG = GridViewItemFragment.class.getSimpleName();
    @Bind(R.id.grid_recycler_view)
    RecyclerView gridRecyclerView;
    GridLayoutManager layoutManager;
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    private List<AllItems> allItemsArrayList = new ArrayList<>();
    private POSAdapter POSAdapter;
    private List<CustomerSalesInvoice> allCartItemsList = new ArrayList<>();

    private List<Product> allProductsList = new ArrayList<>();

    private DashboardComponent mDashboardComponent;

    @Inject
    IGridViewItemActionListener mGridViewItemActionListener;

    private GridViewItemPresenter gridViewItemPresenter;

    public GridViewItemFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getComponent(DashboardComponent.class).inject(this);
        Log.d(TAG, "Inside onCreate");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_grid, container, false);
        ButterKnife.bind(this, rootView);
        Log.d(TAG, "Inside onCreateView");
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "Inside onViewCreated");

        staggeredGridLayoutManager = new StaggeredGridLayoutManager(3, 1);
        gridRecyclerView.setLayoutManager(staggeredGridLayoutManager);
        gridRecyclerView.setItemAnimator(new DefaultItemAnimator());


        mGridViewItemActionListener.setView(this);

        mGridViewItemActionListener.getAllProducts();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void redirectToLogin() {

    }

    @Override
    public DashboardComponent getComponent() {
        return mDashboardComponent;
    }

    @Override
    public void onProductSucessResponse(AllProducts body) {
        Log.d(TAG, "onProductSuccessResponse grid");
        allProductsList = body.getMessage().getData();

//        POSAdapter = new POSAdapter(getActivity(), allProductsList, li);
//        gridRecyclerView.setAdapter(POSAdapter);

//        EventBus.getDefault().postSticky(new GetAllProductsEvent(allProductsList));

    }
}

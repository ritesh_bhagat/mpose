package com.mpose.com.mpose.eventbus;

import com.mpose.com.mpose.pos.models.CustomerSalesInvoice;
import com.mpose.com.mpose.pos.models.Product;

import java.util.List;
import java.util.Map;

/**
 * Created by lp-ritesh on 3/7/17.
 */

public class AddItemToCartEvent {


    private Map<String, CustomerSalesInvoice> mAllCartItemsList;
    private int mPosition;
    private int mTotal;
    private List<Product> mAllProductsDataList;


    public AddItemToCartEvent(Map<String, CustomerSalesInvoice> allCartItemsList, int position, int total) {
        this.mAllCartItemsList = allCartItemsList;
        this.mPosition = position;
        this.mTotal = total;
    }

    public Map<String, CustomerSalesInvoice> getAllCartItemsList() {
        return mAllCartItemsList;
    }

    public void setAllCartItemsList(Map<String, CustomerSalesInvoice> mAllCartItemsList) {
        this.mAllCartItemsList = mAllCartItemsList;
    }

    public int getmPosition() {
        return mPosition;
    }

    public void setmPosition(int mPosition) {
        this.mPosition = mPosition;
    }


    public int getmTotal() {
        return mTotal;
    }

    public void setmTotal(int mTotal) {
        this.mTotal = mTotal;
    }

    public List<Product> getmAllProductsDataList() {
        return mAllProductsDataList;
    }

    public void setmAllProductsDataList(List<Product> mAllProductsDataList) {
        this.mAllProductsDataList = mAllProductsDataList;
    }

}

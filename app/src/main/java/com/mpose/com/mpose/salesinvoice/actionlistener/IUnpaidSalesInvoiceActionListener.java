package com.mpose.com.mpose.salesinvoice.actionlistener;

import com.mpose.com.mpose.salesinvoice.models.unpaid.UnpaidSalesInvoice;
import com.mpose.com.mpose.salesinvoice.view.UnpaidSalesInvoiceView;

import java.util.List;

/**
 * Created by Anjali on 27-09-2017.
 */

public interface IUnpaidSalesInvoiceActionListener<T> {

    void setView(T t);

    void getUnPaidSalesInvoiceList();

}

package com.mpose.com.mpose.dashboard.model.possetting;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class POSSettingTaxis extends RealmObject{

    @SerializedName("charge_type")
    private String chargeType;
    @SerializedName("included_in_print_rate")
    private Integer includedInPrintRate;
    @SerializedName("rate")
    private Integer rate;
    @SerializedName("tax_amount")
    private Integer taxAmount;
    @SerializedName("description")
    private String description;
    @SerializedName("idx")
    private Integer idx;
    @SerializedName("account_head")
    private String accountHead;
    @SerializedName("row_id")
    private String rowId;

    public String getChargeType() {
        return chargeType;
    }

    public void setChargeType(String chargeType) {
        this.chargeType = chargeType;
    }

    public Integer getIncludedInPrintRate() {
        return includedInPrintRate;
    }

    public void setIncludedInPrintRate(Integer includedInPrintRate) {
        this.includedInPrintRate = includedInPrintRate;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Integer getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(Integer taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIdx() {
        return idx;
    }

    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    public String getAccountHead() {
        return accountHead;
    }

    public void setAccountHead(String accountHead) {
        this.accountHead = accountHead;
    }

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

}
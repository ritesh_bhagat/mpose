package com.mpose.com.mpose.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mpose.com.mpose.R;
import com.mpose.com.mpose.pos.models.Product;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by lp-ritesh on 1/7/17.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private static final String TAG = RecyclerViewAdapter.class.getSimpleName();
    private Context mContext;
    private List<Product> mAllItemsList;


    public RecyclerViewAdapter(Context context, List<Product> allItemsList) {
        this.mContext = context;
        this.mAllItemsList = allItemsList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_row_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

//        Product allItems = mAllItemsList.get(position);
//        holder.listItemName.setText(allItems.getItemName());
//        holder.listAvailableQuantity.setText(String.valueOf(allItems.getQty()));
//        holder.listProductCost.setText(allItems.getPrice());
    }

    @Override
    public int getItemCount() {
        return mAllItemsList.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

//        @Bind(R.id.list_image)
//        ImageView listImage;
//
//        @Bind(R.id.txt_list_item_name)
//        TextView listItemName;
//
//        @Bind(R.id.txt_list_avail_qty)
//        TextView listAvailableQuantity;
//
//        @Bind(R.id.txt_list_product_cost)
//        TextView listProductCost;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

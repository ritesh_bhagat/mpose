package com.mpose.com.mpose.utils;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by lp-ritesh on 3/8/17.
 */

public class DateTimeUtil {

    public DateTimeUtil() {
    }

    public static String currentDate(String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        Date date = new Date();
        System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
        return dateFormat.format(date);
    }

    public static Date convertStringDateToTimeStamp(String currentDate, String format) {
        try {
            DateFormat dateFormat = new SimpleDateFormat(format);
            Date date = dateFormat.parse(currentDate);
            System.out.println(date);
            return date;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static String getCurrentTimeStamp() {
        Long tsLong = System.currentTimeMillis() / 1000;
        return tsLong.toString();
    }

}

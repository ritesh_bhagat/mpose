package com.mpose.com.mpose.auth.model.login;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by lp-ritesh on 28/6/17.
 */

public class Login extends RealmObject {

    @SerializedName("usr")
    private String userName;

    @SerializedName("pwd")
    private String password;

    @SerializedName("full_name")
    private String fullName;

    @SerializedName("message")
    private String message;

    @SerializedName("home_page")
    private String homePage;

    @SerializedName("domain")
    private String domainName;

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getHomePage() {
        return homePage;
    }

    public void setHomePage(String homePage) {
        this.homePage = homePage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Login{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", fullName='" + fullName + '\'' +
                ", message='" + message + '\'' +
                ", homePage='" + homePage + '\'' +
                ", domainName='" + domainName + '\'' +
                '}';
    }
}

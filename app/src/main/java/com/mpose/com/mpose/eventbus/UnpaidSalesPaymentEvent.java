package com.mpose.com.mpose.eventbus;

import com.mpose.com.mpose.sync.model.bulkpaymentrequest.BulkPaymentData;

/**
 * Created by Anjali on 03-10-2017.
 */

public class UnpaidSalesPaymentEvent {

    private BulkPaymentData mBulkPaymentData;

    public UnpaidSalesPaymentEvent(BulkPaymentData bulkPaymentData) {
        mBulkPaymentData = bulkPaymentData;
    }

    public BulkPaymentData getBulkPaymentData() {
        return mBulkPaymentData;
    }
}

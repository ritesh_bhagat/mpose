package com.mpose.com.mpose.eventbus;

/**
 * Created by lp-ritesh on 27/7/17.
 */

public class CustomerNameEvent {
    private String customerName;

    public CustomerNameEvent(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
}

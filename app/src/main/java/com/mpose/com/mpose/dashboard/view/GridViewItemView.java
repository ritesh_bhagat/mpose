package com.mpose.com.mpose.dashboard.view;

import com.mpose.com.mpose.common.BaseView;
import com.mpose.com.mpose.pos.models.AllProducts;

/**
 * Created by lp-ritesh on 5/7/17.
 */

public interface GridViewItemView extends BaseView {

    void onProductSucessResponse(AllProducts body);
}

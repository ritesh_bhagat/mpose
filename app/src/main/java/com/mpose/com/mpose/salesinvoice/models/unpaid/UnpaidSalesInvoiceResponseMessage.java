package com.mpose.com.mpose.salesinvoice.models.unpaid;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Anjali on 27-09-2017.
 */

public class UnpaidSalesInvoiceResponseMessage {

    @SerializedName("message")
    private UnpaidSalesInvoiceApiResponse response;

    public UnpaidSalesInvoiceApiResponse getResponse() {
        return response;
    }

    public void setResponse(UnpaidSalesInvoiceApiResponse response) {
        this.response = response;
    }
}

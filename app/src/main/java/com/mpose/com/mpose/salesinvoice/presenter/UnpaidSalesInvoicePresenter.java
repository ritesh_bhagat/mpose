package com.mpose.com.mpose.salesinvoice.presenter;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.mpose.com.mpose.R;
import com.mpose.com.mpose.common.Presenter;
import com.mpose.com.mpose.manager.SharedPreferenceManager;
import com.mpose.com.mpose.retrofit.ApiClient;
import com.mpose.com.mpose.retrofit.ApiInterface;
import com.mpose.com.mpose.salesinvoice.actionlistener.IUnpaidSalesInvoiceActionListener;
import com.mpose.com.mpose.salesinvoice.models.unpaid.UnpaidSalesInvoiceResponseMessage;
import com.mpose.com.mpose.salesinvoice.view.UnpaidSalesInvoiceView;
import com.mpose.com.mpose.utils.AppConstants;
import com.mpose.com.mpose.utils.NetworkConncetionUtility;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Anjali on 27-09-2017.
 */

public class UnpaidSalesInvoicePresenter extends Presenter implements IUnpaidSalesInvoiceActionListener<UnpaidSalesInvoiceView> {

    private static final String TAG = UnpaidSalesInvoicePresenter.class.getSimpleName();
    private UnpaidSalesInvoiceView mUnpaidSalesInvoiceView;
    private Context mContext;

    @Inject
    UnpaidSalesInvoicePresenter(Context context) {
        mContext = context;
    }

    @Override
    public void setView(UnpaidSalesInvoiceView unpaidSalesInvoiceView) {
        mUnpaidSalesInvoiceView = unpaidSalesInvoiceView;
    }

    @Override
    public void getUnPaidSalesInvoiceList() {
        if (!NetworkConncetionUtility.getInstance(mContext).isConnected()) {
            mUnpaidSalesInvoiceView.showNetworkConnectionError();
            return;
        }

        mUnpaidSalesInvoiceView.showLoading();
        String sid = SharedPreferenceManager.getSharedPreferenceManager(mContext).getSID();
        ApiInterface apiInterface = ApiClient.getClient(mContext).create(ApiInterface.class);
        Call<UnpaidSalesInvoiceResponseMessage> call = apiInterface.getUnpaidSalesInvoice(sid);
        call.enqueue(new Callback<UnpaidSalesInvoiceResponseMessage>() {
            @Override
            public void onResponse(Call<UnpaidSalesInvoiceResponseMessage> call,
                                   Response<UnpaidSalesInvoiceResponseMessage> response) {
                if (response.isSuccessful() && response.body() != null) {
                    manageUnpaidSalesInvoiceList(response.body());
                } else {
                    if (response.code() == AppConstants.RESPONSE_FORBIDDEN) {
                        Toast.makeText(mContext, mContext.getString(R.string.session_expire_message),
                                Toast.LENGTH_SHORT).show();
                    }
                }
                mUnpaidSalesInvoiceView.hideLoading();
            }

            @Override
            public void onFailure(Call<UnpaidSalesInvoiceResponseMessage> call, Throwable t) {
                mUnpaidSalesInvoiceView.hideLoading();
//                Log.e(TAG, t.getMessage());
            }
        });
    }

    private void manageUnpaidSalesInvoiceList(UnpaidSalesInvoiceResponseMessage responseMessage) {
        if (responseMessage != null && responseMessage.getResponse() != null &&
                responseMessage.getResponse().getData() != null) {
            mUnpaidSalesInvoiceView.showUnpaidSalesInvoiceList(responseMessage.getResponse().getData());
        }
    }


}

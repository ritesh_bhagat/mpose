package com.mpose.com.mpose.dashboard.model.companydetails;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by lp-ritesh on 22/11/17.
 */

public class CompanyDetailsModel extends RealmObject {

    @SerializedName("message")
    private Message message;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

}



package com.mpose.com.mpose.di.component;

import com.mpose.com.mpose.activity.HomeActivity;
import com.mpose.com.mpose.di.PerActivity;
import com.mpose.com.mpose.di.module.ActivityModule;
import com.mpose.com.mpose.di.module.DashboardModule;
import com.mpose.com.mpose.fragments.AddExpenseFragment;
import com.mpose.com.mpose.fragments.CustomerSalesInvoiceFragment;
import com.mpose.com.mpose.fragments.DashboardFragment;
import com.mpose.com.mpose.fragments.ExpenseClaimFragment;
import com.mpose.com.mpose.fragments.GridViewItemFragment;
import com.mpose.com.mpose.fragments.ListViewItemFragment;
import com.mpose.com.mpose.fragments.POSFragment;
import com.mpose.com.mpose.fragments.SalesInvoiceFragment;
import com.mpose.com.mpose.fragments.UnpaidSalesInvoiceFragment;

import dagger.Component;

/**
 * Created by lp-ritesh on 5/7/17.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = {ActivityModule.class, DashboardModule.class})
public interface DashboardComponent {

    void inject(HomeActivity homeActivity);

    void inject(GridViewItemFragment gridViewItemFragment);

    void inject(ListViewItemFragment listViewItemFragment);

    void inject(POSFragment posFragment);

    void inject(CustomerSalesInvoiceFragment customerSalesInvoiceFragment);

    void inject(SalesInvoiceFragment salesInvoiceFragement);

    void inject(DashboardFragment dashboardFragment);

    void inject(AddExpenseFragment addExpenseFragment);

    void inject(ExpenseClaimFragment expenseClaimFragment);

    void inject(UnpaidSalesInvoiceFragment unpaidSalesInvoiceFragment);

}

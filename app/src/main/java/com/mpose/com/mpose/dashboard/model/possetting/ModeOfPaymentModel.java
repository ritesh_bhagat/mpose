package com.mpose.com.mpose.dashboard.model.possetting;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by lp-ritesh on 25/7/17.
 */

public class ModeOfPaymentModel extends RealmObject {

    @SerializedName("Cash")
    private String cash;
    @SerializedName("Bank Draft")
    private String bankDraft;
    @SerializedName("Credit Card")
    private String creditCard;

//    @SerializedName("modes_of_payment")
//    String modesOfPayment;

//    public String getModesOfPayment() {
//        return modesOfPayment;
//    }
//
//    public void setModesOfPayment(String modesOfPayment) {
//        this.modesOfPayment = modesOfPayment;
//    }


    public String getCash() {
        return cash;
    }

    public void setCash(String cash) {
        this.cash = cash;
    }

    public String getBankDraft() {
        return bankDraft;
    }

    public void setBankDraft(String bankDraft) {
        this.bankDraft = bankDraft;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }
}

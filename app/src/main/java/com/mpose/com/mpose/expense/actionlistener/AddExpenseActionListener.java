package com.mpose.com.mpose.expense.actionlistener;

/**
 * Created by lp-ritesh on 19/8/17.
 */

public interface AddExpenseActionListener<T> {
    void setView(T t);

    String getLoggedInUserName();

    void getAllExpenseTypes();

    void saveExpenseDetialsToDB();
}

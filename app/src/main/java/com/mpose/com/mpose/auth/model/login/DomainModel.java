package com.mpose.com.mpose.auth.model.login;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lp-ritesh on 21/7/17.
 */

public class DomainModel extends RealmObject {

    @PrimaryKey
    @SerializedName("message")
    private String message;

    @SerializedName("session_expired")
    private Integer sessionExpired;

    @SerializedName("domain_name")
    private String domainName;

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getSessionExpired() {
        return sessionExpired;
    }

    public void setSessionExpired(Integer sessionExpired) {
        this.sessionExpired = sessionExpired;
    }
}

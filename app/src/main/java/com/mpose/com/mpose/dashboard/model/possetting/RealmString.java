package com.mpose.com.mpose.dashboard.model.possetting;

import io.realm.RealmObject;

/**
 * Created by lp-ritesh on 25/7/17.
 */

public class RealmString extends RealmObject {


    private String mStringObject;

    public RealmString() {
    }

    public RealmString(String str) {
        mStringObject = str;
    }

    public String getStringObject() {
        return mStringObject;
    }

    public void setStringObject(String stringObject) {
        this.mStringObject = stringObject;
    }

    @Override
    public String toString() {
        return mStringObject;
    }
}

package com.mpose.com.mpose.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.mpose.com.mpose.home.presenter.HomePresenter;
import com.mpose.com.mpose.utils.NetworkConncetionUtility;

/**
 * Created by Anjali on 25-09-2017.
 */

public class AutoSyncIntentService extends IntentService {

    private static final String TAG = AutoSyncIntentService.class.getSimpleName();

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public AutoSyncIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.d(TAG, "onHandleIntent: ");
        if (NetworkConncetionUtility.getInstance(getApplicationContext()).isConnected()) {
            new HomePresenter(getApplicationContext()).populateSalesInvoiceCreationData();
        }
    }
}

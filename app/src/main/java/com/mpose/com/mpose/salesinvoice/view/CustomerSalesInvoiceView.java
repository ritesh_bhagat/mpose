package com.mpose.com.mpose.salesinvoice.view;

import com.mpose.com.mpose.common.BaseView;

import java.util.List;

/**
 * Created by lp-ritesh on 3/8/17.
 */

public interface CustomerSalesInvoiceView extends BaseView {

    void setCustomerNameToView(String name, String stringCustomerID);

    void setInvoiceListToSpinner(List<String> invoiceList);

    void setTotalCalculation(double grandTotalWithGst, Double additionalDiscountPercentage, Double outStandingAmount);

    void setCustomerIDToView(String s);

    void setSalesInvoiceDate(String invoiceDate);

    void setReturnAgainstInvoice(String returnInvoice);

    void showTaxDetailsToView(int vatTaxAmount, String desc);

    void setOutstandingAmount(Double outStandingAmount);
}

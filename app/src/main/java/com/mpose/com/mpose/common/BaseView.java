package com.mpose.com.mpose.common;

/**
 * Created by lp-ritesh on 5/7/17.
 */

public interface BaseView {
    void showLoading();

    void hideLoading();

    void showError(String message);

    void redirectToLogin();
}

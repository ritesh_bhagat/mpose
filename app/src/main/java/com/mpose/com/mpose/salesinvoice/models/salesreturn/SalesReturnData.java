package com.mpose.com.mpose.salesinvoice.models.salesreturn;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SalesReturnData {

    @SerializedName("invoice_id")
    private String invoiceId;
    @SerializedName("is_return")
    private Integer isReturn;
    @SerializedName("posting_date")
    private String postingDate;
    @SerializedName("return_against")
    private String returnAgainst;
    @SerializedName("items")
    private List<SalesReturnItem> items;

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Integer getIsReturn() {
        return isReturn;
    }

    public void setIsReturn(Integer isReturn) {
        this.isReturn = isReturn;
    }

    public String getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(String postingDate) {
        this.postingDate = postingDate;
    }

    public String getReturnAgainst() {
        return returnAgainst;
    }

    public void setReturnAgainst(String returnAgainst) {
        this.returnAgainst = returnAgainst;
    }

    public List<SalesReturnItem> getItems() {
        return items;
    }

    public void setItems(List<SalesReturnItem> items) {
        this.items = items;
    }

}
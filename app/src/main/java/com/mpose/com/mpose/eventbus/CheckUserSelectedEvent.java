package com.mpose.com.mpose.eventbus;

/**
 * Created by lp-ritesh on 20/7/17.
 */

public class CheckUserSelectedEvent {
    boolean isSelected;

    public CheckUserSelectedEvent(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}

package com.mpose.com.mpose.dashboard.model.possetting;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by lp-ritesh on 25/7/17.
 */

public class ExpenseTypeModel extends RealmObject {

    @SerializedName("Travel")
    private String Travel;

    @SerializedName("Others")
    private String Other;

    @SerializedName("Medical")
    private String Medical;

    @SerializedName("Food")
    private String Food;

    @SerializedName("Calls")
    private String Calls;

    public String getTravel() {
        return Travel;
    }

    public void setTravel(String travel) {
        Travel = travel;
    }

    public String getOther() {
        return Other;
    }

    public void setOther(String other) {
        Other = other;
    }

    public String getMedical() {
        return Medical;
    }

    public void setMedical(String medical) {
        Medical = medical;
    }

    public String getFood() {
        return Food;
    }

    public void setFood(String food) {
        Food = food;
    }

    public String getCalls() {
        return Calls;
    }

    public void setCalls(String calls) {
        Calls = calls;
    }

    @Override
    public String toString() {

        if (!TextUtils.isEmpty(getTravel())) {
            return getTravel();
        } else if (!TextUtils.isEmpty(getFood())) {
            return getFood();
        }

        return super.toString();
    }
}

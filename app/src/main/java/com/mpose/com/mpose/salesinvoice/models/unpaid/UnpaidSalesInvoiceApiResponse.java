package com.mpose.com.mpose.salesinvoice.models.unpaid;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Anjali on 27-09-2017.
 */

public class UnpaidSalesInvoiceApiResponse {

    @SerializedName("status")
    private String status;

    @SerializedName("data")
    private List<UnpaidSalesInvoice> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<UnpaidSalesInvoice> getData() {
        return data;
    }

    public void setData(List<UnpaidSalesInvoice> data) {
        this.data = data;
    }
}

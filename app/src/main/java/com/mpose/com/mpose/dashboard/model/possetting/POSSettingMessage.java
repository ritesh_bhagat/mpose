package com.mpose.com.mpose.dashboard.model.possetting;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class POSSettingMessage extends RealmObject{

    @SerializedName("status")
    private String status;

    @SerializedName("data")
    private POSSettingData data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public POSSettingData getData() {
        return data;
    }

    public void setData(POSSettingData data) {
        this.data = data;
    }


}
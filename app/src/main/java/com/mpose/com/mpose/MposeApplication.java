package com.mpose.com.mpose;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.mpose.com.mpose.di.component.ApplicationComponent;
import com.mpose.com.mpose.di.component.DaggerApplicationComponent;
import com.mpose.com.mpose.di.module.ApplicationModule;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by lp-ritesh on 23/6/17.
 */

public class MposeApplication extends MultiDexApplication {

    ApplicationComponent applicationComponent;

    private static MposeApplication mposeApplication;

    private boolean isCustomerSelected;
    private String customerName;

    public MposeApplication() {
        mposeApplication = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());
        initInjector();


        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(2)
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.getInstance(config);
        Realm.setDefaultConfiguration(config);
    }

    private void initInjector() {
        this.applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static MposeApplication getMposeApplication() {
        return mposeApplication;
    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }


    public boolean isCustomerSelected() {
        return isCustomerSelected;
    }

    public void setCustomerSelected(boolean customerSelected) {
        isCustomerSelected = customerSelected;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
}

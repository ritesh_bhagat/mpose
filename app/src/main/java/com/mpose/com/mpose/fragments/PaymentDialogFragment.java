package com.mpose.com.mpose.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.mpose.com.mpose.R;
import com.mpose.com.mpose.eventbus.GenerateSalesInvoice;
import com.mpose.com.mpose.utils.AppConstants;
import com.mpose.com.mpose.utils.CommonAlertDialog;
import com.mpose.com.mpose.utils.DateTimeUtil;

import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by lp-ritesh on 31/7/17.
 */

public class PaymentDialogFragment extends DialogFragment {

    private static final String TAG = PaymentDialogFragment.class.getSimpleName();

    @Bind(R.id.pay_dialog_cust_name)
    TextView customerName;

    @Bind(R.id.txt_pay_amout)
    TextView txtGrandTotal;

    @Bind(R.id.txt_payment_submit_button)
    Button submitButton;

    @Bind(R.id.paid_value)
    TextView txtCashValue;

    @Bind(R.id.outstanding_value)
    TextView outstandingValue;

    @Bind(R.id.change_value)
    TextView edChangeValue;

    @Bind(R.id.write_off_value)
    EditText writeOffAmount;

    @Bind(R.id.ed_cash_value)
    EditText edCashValue;

    @Bind(R.id.ed_discount_value)
    EditText edDiscountValue;

    @Bind(R.id.dialog_date)
    TextView dialogDate;

    @Bind(R.id.dialog_time)
    TextView dialogTime;

    @Bind(R.id.radio_cash_payment)
    RadioButton radioCashPayment;

    @Bind(R.id.dialog_close)
    ImageView closeDialog;


    private double mAmountPaidByUser = 0;

    private double mOutStandingValue = 0;

    private double mGrandTotal;

    private double mTempValue = 0;

    private double mTempValueChange = 0;

    private double mDiscountValue = 0;

    private boolean isChangeFromDiscount = false;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mGrandTotal = Double.parseDouble(getArguments().getString(AppConstants.GRAND_TOTAL_FIELD_NAME));
        mTempValue = mGrandTotal;
        String name = getArguments().getString(AppConstants.CUSTOMER_NAME_FIELD_NAME);

        setGrandTotalvalue(mGrandTotal);
        setTxtCashValue(mGrandTotal);
        setEdCashValue(mGrandTotal);
        setCustomerName(name);

        radioCashPayment.setChecked(true);

        String date = getCurrentDateAndTime();
        String time = getCurrentTime();

        dialogDate.setText(date);
        dialogTime.setText(time);

        edCashValue.addTextChangedListener(foredCashValueTextWatcher);

        edChangeValue.addTextChangedListener(forChangeValueTextWatcher);

        writeOffAmount.addTextChangedListener(forWriteTextWatcher);

        edDiscountValue.addTextChangedListener(forDiscountTextWatcher);
    }

    TextWatcher foredCashValueTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                if (!s.toString().trim().equals("")) {
                    double cashValue = Double.parseDouble(s.toString());
                    setTxtCashValue(cashValue);
                    if (cashValue > mGrandTotal) {
                        double changeValue = cashValue - mGrandTotal;
                        edChangeValue.setEnabled(true);
                        edChangeValue.removeTextChangedListener(forChangeValueTextWatcher);
                        mTempValueChange = -changeValue;
                        setEdChangeValue(changeValue);
                        edChangeValue.addTextChangedListener(forChangeValueTextWatcher);
                        setOutstandingValue(0);
                    } else {
                        edChangeValue.setEnabled(false);
                        edChangeValue.removeTextChangedListener(forChangeValueTextWatcher);
                        setEdChangeValue(0);
                        edChangeValue.addTextChangedListener(forChangeValueTextWatcher);
                        if (isChangeFromDiscount) {
                            isChangeFromDiscount = false;
                            return;
                        }
                        double newOutValue = mGrandTotal - cashValue;
                        setOutstandingValue(newOutValue);
                    }
                } else {
                    setTxtCashValue(0);
                    edChangeValue.removeTextChangedListener(forChangeValueTextWatcher);
                    setEdChangeValue(0);
                    edChangeValue.addTextChangedListener(forChangeValueTextWatcher);
                    double newOutValue = mGrandTotal - 0;
                    setOutstandingValue(newOutValue);

                    writeOffAmount.removeTextChangedListener(forWriteTextWatcher);
                    setWriteOffAmount(0);
                    writeOffAmount.addTextChangedListener(forWriteTextWatcher);
                }
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
            }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    TextWatcher forDiscountTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            isChangeFromDiscount = true;
            try {
                if (count > 0) {
                    if (!s.toString().trim().equals("")) {
                        double discountVal = Double.parseDouble(s.toString());
                        double finalTotal = discountVal * mTempValue / 100;
                        double cashValue = mTempValue - finalTotal;
//                        setGrandTotalvalue(cashValue);
                        setTxtCashValue(cashValue);
                        edCashValue.removeTextChangedListener(foredCashValueTextWatcher);
                        setEdCashValue(cashValue);
                        edCashValue.addTextChangedListener(foredCashValueTextWatcher);
                        if (cashValue > mGrandTotal) {
                            double changeValue = cashValue - mGrandTotal;
                            edChangeValue.removeTextChangedListener(forChangeValueTextWatcher);
                            setEdChangeValue(changeValue);
                            edChangeValue.addTextChangedListener(forChangeValueTextWatcher);
                            setOutstandingValue(0);
                        } else {
                            edChangeValue.removeTextChangedListener(forChangeValueTextWatcher);
                            setEdChangeValue(0);
                            edChangeValue.addTextChangedListener(forChangeValueTextWatcher);
                            setOutstandingValue(0);
                        }

                        mDiscountValue = discountVal;
                        mGrandTotal = mTempValue - finalTotal;
                    }
                } else {
                    mDiscountValue = 0;
//                    setGrandTotalvalue(mTempValue);
                    setTxtCashValue(mTempValue);
                    setEdChangeValue(0);
                    setOutstandingValue(0);
                    edCashValue.removeTextChangedListener(foredCashValueTextWatcher);
                    setEdCashValue(mTempValue);
                    edCashValue.addTextChangedListener(foredCashValueTextWatcher);
                    mGrandTotal = mTempValue;
                }
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
            }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    TextWatcher forChangeValueTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            double cashValue = 0;
            double chaneValueInt = 0;
            try {
                if (count > 0) {
                    String change = s.toString();
                    if (!s.toString().trim().equals("")) {
                        cashValue = Double.parseDouble(edCashValue.getText().toString());
                        if (!change.equalsIgnoreCase("")) {
                            chaneValueInt = Double.parseDouble(change);
                        }
                        double newValue = cashValue - mGrandTotal;
                        writeOffAmount.removeTextChangedListener(forWriteTextWatcher);
                        setWriteOffAmount(chaneValueInt - newValue);
                        writeOffAmount.addTextChangedListener(forWriteTextWatcher);
                    }
                } else {
                    writeOffAmount.removeTextChangedListener(forWriteTextWatcher);
                    setWriteOffAmount(mTempValueChange);
                    writeOffAmount.addTextChangedListener(forWriteTextWatcher);
                }

            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
            }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    TextWatcher forWriteTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String writeOff = s.toString();
            double cashValue = Double.parseDouble(edCashValue.getText().toString());
            double newOutValue = 0;
            double changeValue = 0;
            if (cashValue > mGrandTotal) {
                changeValue = cashValue - mGrandTotal;
                edChangeValue.removeTextChangedListener(forChangeValueTextWatcher);
                edChangeValue.addTextChangedListener(forChangeValueTextWatcher);
                setOutstandingValue(0);

                double writevalueInt = 0;
                try {
                    if (!writeOff.equalsIgnoreCase("")) {
                        writevalueInt = Double.parseDouble(writeOff);
                    }
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }

                double writeValue = newOutValue - writevalueInt;
                edChangeValue.removeTextChangedListener(forChangeValueTextWatcher);
                setEdChangeValue(changeValue - (-writevalueInt));
                edChangeValue.addTextChangedListener(forChangeValueTextWatcher);

            } else {
                edChangeValue.removeTextChangedListener(forChangeValueTextWatcher);
                setEdChangeValue(0);
                edChangeValue.addTextChangedListener(forChangeValueTextWatcher);
                newOutValue = mGrandTotal - cashValue;
                double writevalueInt = 0;
                double outValueInt = 0;
                try {
                    if (!writeOff.equalsIgnoreCase("")) {
                        writevalueInt = Double.parseDouble(writeOff);
                    }
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }
                double writeValue = newOutValue - writevalueInt;
                edChangeValue.removeTextChangedListener(forChangeValueTextWatcher);
                setOutstandingValue(writeValue);
                edChangeValue.addTextChangedListener(forChangeValueTextWatcher);
            }


        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void setCustomerName(String custName) {
        customerName.setText(custName);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.payment_layout_dialog, container, false);
        ButterKnife.bind(this, rootView);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().getAttributes().windowAnimations = R.style.dialog_animation_fade;
        return dialog;
    }

    public void setGrandTotalvalue(double grandTotal) {
        txtGrandTotal.setText(new DecimalFormat("#.##").format(grandTotal));
        mGrandTotal = Double.parseDouble(txtGrandTotal.getText().toString());
    }

    public void setTxtCashValue(double total) {
        txtCashValue.setText(new DecimalFormat("#.##").format(total));
        mAmountPaidByUser = Double.parseDouble(txtCashValue.getText().toString());
    }

    public void setEdCashValue(double total) {
        edCashValue.setText(new DecimalFormat("#.##").format(total));
    }

    public void setWriteOffAmount(double write) {
        writeOffAmount.setText(new DecimalFormat("#.##").format(write));
    }

    public void setOutstandingValue(double outvalue) {
        double roundOff = (double) Math.round(outvalue * 100) / 100;
        this.mOutStandingValue = roundOff;
        outstandingValue.setText(new DecimalFormat("#.##").format(mOutStandingValue));
    }

    public void setEdChangeValue(double value) {
        edChangeValue.setText(new DecimalFormat("#.##").format(value));

    }


    public double splitRupeesValue(String value) {
        String[] str = value.split(getResources().getString(R.string.rs) + " ");
        return Double.parseDouble(str[1]);
    }

    public String getCurrentDateAndTime() {
        return DateTimeUtil.currentDate(AppConstants.DATE_FORMAT);
    }

    public String getCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm a");
        String time = localDateFormat.format(calendar.getTime());
        System.out.println(time);
        return time;
    }

    @OnClick(R.id.dialog_close)
    public void closePaymentDialog() {
        if (getDialog().isShowing()) {
            getDialog().dismiss();
        }
    }

    @OnClick(R.id.txt_payment_submit_button)
    public void submitButtonClick() {
        if (TextUtils.isEmpty(edCashValue.getText().toString().trim())) {
            CommonAlertDialog.ShowAlertDialog(getActivity(), getResources().getString(R.string.si_error_title),
                    getResources().getString(R.string.si_error_cash_message), 0);
            return;
        }

        if (!TextUtils.isEmpty(edDiscountValue.getText().toString().trim())) {
            if (Double.parseDouble(edDiscountValue.getText().toString()) >= 100) {
                CommonAlertDialog.ShowAlertDialog(getActivity(), getResources().getString(R.string.discount_error_title),
                        getResources().getString(R.string.discount_error_message), 0);
                return;
            }
        }

        if (mDiscountValue == 0) {
            EventBus.getDefault().post(new GenerateSalesInvoice(mTempValue, mDiscountValue, mOutStandingValue, mAmountPaidByUser));
        } else {
            EventBus.getDefault().post(new GenerateSalesInvoice(mGrandTotal, mDiscountValue, mOutStandingValue, mAmountPaidByUser));
        }

        getDialog().dismiss();
        InputMethodManager inputMethodManager = (InputMethodManager) getDialog().getContext().getSystemService(
                Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(txtGrandTotal.getWindowToken(), 0);

    }


}

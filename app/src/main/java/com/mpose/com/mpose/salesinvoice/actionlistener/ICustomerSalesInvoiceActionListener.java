package com.mpose.com.mpose.salesinvoice.actionlistener;

import android.view.MenuItem;

import com.mpose.com.mpose.pos.models.ProductLite;

import java.util.List;

/**
 * Created by lp-ritesh on 3/8/17.
 */

public interface ICustomerSalesInvoiceActionListener<T> {
    void setView(T t);

    List<ProductLite> getCustomerInvoiceListFromDB(int invoiceNumber);

//    void fetchCustomerIdFromDB(int invoiceNumber);

    void fetchInvoicesForSpinner(String customerName);

    void fetchTotalCalculation(List<ProductLite> list);

    void createCustomerSalesInvoiceForReturn(List<ProductLite> mCurrentCreditList, int mInvoiceNumber);

    void fetchCustomerIdFromDB(int mInvoiceNumber);

    void getCustomerSaleInvoiceCreationDateFromDB(int mInvoiceNumber);

    void fetchReturnInvoice(int mInvoiceNumber);

    void fetchVatPercentFromDB();

    void changeReturnAgainstStatus();

    void fetchTotal(int invoiceNumber);

    String getPaymentStatus(int invoiceNumber);

    void fetchOutstandingAmount(int mInvoiceNumber);

    void handleMenuItemClick(MenuItem item);

    String getStringInvoiceNumberFromDB(int mInvoiceNumber);
}

package com.mpose.com.mpose.home.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.mpose.com.mpose.auth.model.login.Login;
import com.mpose.com.mpose.auth.model.logout.LogOut;
import com.mpose.com.mpose.common.Presenter;
import com.mpose.com.mpose.dashboard.model.possetting.POSSettingModel;
import com.mpose.com.mpose.dashboard.model.possetting.POSSettingTaxis;
import com.mpose.com.mpose.di.PerActivity;
import com.mpose.com.mpose.eventbus.UpdateSIPaymentEntryStatus;
import com.mpose.com.mpose.expense.models.addexpense.CustomerExpenseModel;
import com.mpose.com.mpose.expense.models.expenserequest.ExpenseData;
import com.mpose.com.mpose.expense.models.expenserequest.ExpenseRequestDetails;
import com.mpose.com.mpose.expense.models.expenserequest.ExpenseRequestModel;
import com.mpose.com.mpose.expense.models.expenseresponse.ExpenseResponse;
import com.mpose.com.mpose.home.actionlistener.IHomeActionListener;
import com.mpose.com.mpose.home.view.HomeView;
import com.mpose.com.mpose.manager.SharedPreferenceManager;
import com.mpose.com.mpose.pos.models.Customer;
import com.mpose.com.mpose.pos.models.CustomerSalesInvoice;
import com.mpose.com.mpose.pos.models.ProductLite;
import com.mpose.com.mpose.retrofit.ApiClient;
import com.mpose.com.mpose.retrofit.ApiInterface;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkapi.SalesInvoiceBulkApiModel;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkapi.SalesInvoiceItem;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkapi.SalesInvoicePayment;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkapi.SalesInvoiceTaxis;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkapi.SalesRequestData;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkresponse.SalesInvoiceBulkApiResponse;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkresponse.SalesInvoiceResponseData;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkresponse.SalesInvoiceResponseMessage;
import com.mpose.com.mpose.sync.model.bulkpaymentrequest.BulkPaymentApiModel;
import com.mpose.com.mpose.sync.model.bulkpaymentrequest.BulkPaymentData;
import com.mpose.com.mpose.sync.model.bulkpaymentresponse.BulkPaymentMessage;
import com.mpose.com.mpose.sync.model.bulkpaymentresponse.BulkPaymentResponse;
import com.mpose.com.mpose.utils.AppConstants;
import com.mpose.com.mpose.utils.CustomerIdUtils;
import com.mpose.com.mpose.utils.DateTimeUtil;
import com.mpose.com.mpose.utils.NetworkConncetionUtility;
import com.mpose.com.mpose.utils.PaymentUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lp-ritesh on 3/8/17.
 */

@PerActivity
public class HomePresenter extends Presenter implements IHomeActionListener<HomeView> {

    private static final String TAG = HomePresenter.class.getSimpleName();
    private Context mContext;
    private HomeView mHomeView;
    private Realm mRealm;
    private RealmList<Customer> customerList = new RealmList<>();
    private List<Customer> list = new ArrayList<>();
    private int CUST_ID = 0;

    @Inject
    public HomePresenter(Context mContext) {
        this.mContext = mContext;
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    public void setView(HomeView homeView) {
        this.mHomeView = homeView;

    }


    @Override
    public RealmList<Customer> readCustomerFromDB() {

        if (mRealm.isClosed()) {
            mRealm = Realm.getDefaultInstance();
        }
        RealmResults<Customer> realmResults = mRealm.where(Customer.class).findAll();
        Log.d(TAG, "realmResult- " + realmResults.size());

        for (Customer cust : realmResults) {
            Log.d(TAG, "realmResult- " + cust.getCustomerName() + " " + cust.getCustomerId());
        }
        customerList.clear();
        customerList.addAll(realmResults);
        return customerList;
    }

    private void bulkPayment(List<BulkPaymentData> bulkPaymentDataList) {

        String sid = SharedPreferenceManager.getSharedPreferenceManager(mContext).getSID();
        ApiInterface apiInterface = ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<BulkPaymentResponse> modelCall = apiInterface.postBulkPayment(sid, bulkPaymentDataList);
        modelCall.enqueue(new Callback<BulkPaymentResponse>() {
            @Override
            public void onResponse(Call<BulkPaymentResponse> call, Response<BulkPaymentResponse> response) {
                if (mHomeView != null) {
                    mHomeView.hideSyncDialog();
                }
                if (response.isSuccessful() && response.body() != null) {
                    if (mHomeView != null) {
                        mHomeView.showSucessMessage();
                    }
                    EventBus.getDefault().post(new UpdateSIPaymentEntryStatus());
                    saveSyncPaymentEntriesToDB(response.body());
                } else if (response.code() == AppConstants.RESPONSE_FORBIDDEN) {
                    mHomeView.hideLoading();
                    mHomeView.showSessionExpiryDialog();
                }
            }

            @Override
            public void onFailure(Call<BulkPaymentResponse> call, Throwable t) {
                Log.d(TAG, "bulkPayment failure" + t.getMessage());
                mHomeView.showOnFailureAlert();
            }
        });
    }

    private void saveSyncPaymentEntriesToDB(final BulkPaymentResponse bulkPaymentResponseBody) {
        Realm realmInstance = Realm.getDefaultInstance();
        realmInstance.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(bulkPaymentResponseBody);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                updatePaymentEntriesStatus();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.e(TAG, error.getMessage());
            }
        });


    }

    private void updatePaymentEntriesStatus() {
        final Realm realmInstance = Realm.getDefaultInstance();
        realmInstance.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<BulkPaymentMessage> results = realm.where(BulkPaymentMessage.class).findAll();
                for (BulkPaymentMessage response : results) {
                    RealmResults<BulkPaymentData> bulkPaymentResult = realm.where(BulkPaymentData.class).findAll();
                    for (BulkPaymentData bulkPaymentData : bulkPaymentResult) {
                        if (response.getPosPaymentId().equals(bulkPaymentData.getPaymentId())) {
                            bulkPaymentData.setPaymentStatus("Success");
                        }
                    }
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {

//                RealmResults<BulkPaymentData> results = realmInstance.where(BulkPaymentData.class)
//                        .equalTo("paymentStatus", "Success").findAll();
//                for (BulkPaymentData data : results) {
//                    /// update payment entry sync status
//                    Log.d(TAG, "onSuccess: data " + data.getInvoiceId());
//
//                    RealmResults<CustomerSalesInvoice> customerSalesInvoicesResult =
//                            realmInstance.where(CustomerSalesInvoice.class).findAll();
//                    for (CustomerSalesInvoice customerSalesInvoice : customerSalesInvoicesResult) {
//                        if (customerSalesInvoice.getStringInvoiceID().equals(data.getInvoiceId())) {
//                            customerSalesInvoice.setPaymentStatus(true);
//                        }
//                    }
//                }
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.e(TAG, error.getMessage());
            }
        });
    }

    @Override
    public void logOut() {
        mHomeView.showLoading();
        String sid = SharedPreferenceManager.getSharedPreferenceManager(mContext).getSID();
        ApiInterface apiInterface = ApiClient.getClient(mContext).create(ApiInterface.class);
        Call<LogOut> model = apiInterface.logOutUser(sid);
        model.enqueue(new Callback<LogOut>() {
            @Override
            public void onResponse(Call<LogOut> call, Response<LogOut> response) {
                if (response.isSuccessful()) {
                    LogOut.setDomainName(SharedPreferenceManager.getSharedPreferenceManager(mContext).getDomainName());
                    mHomeView.onSuccessLogout();
//                    removeLoginFromDB();
                } else {
                    Log.d(TAG, "LogoutStatus error - ");
                }
                mHomeView.hideLoading();
            }

            @Override
            public void onFailure(Call<LogOut> call, Throwable t) {
                mHomeView.hideLoading();
                Log.d(TAG, "LogoutStatus Failure- " + t.getMessage());
                mHomeView.showOnFailureAlert();
            }
        });
    }

    private void removeLoginFromDB() {
        RealmQuery<Login> query = mRealm.where(Login.class);
        final Login login = query.findFirst();
        if (login != null) {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(@NonNull Realm realm) {
                    realm.deleteAll();
                }
            });
        } else {
            removeLoginFromDB();
        }
    }

    /*private void removeLoginFromDB() {
        RealmQuery<Login> query = mRealm.where(Login.class);
        Login login = query.findFirst();
        final String name = login.getFullName();

        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Login> results = realm.where(Login.class).equalTo(AppConstants.USER_FULL_NAME, name).findAll();
                results.deleteAllFromRealm();
            }
        });

    }*/

    @Override
    public void setSelectedCustomerIdToDB(final String name) {

        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmQuery<Customer> query = realm.where(Customer.class)
                        .equalTo(AppConstants.CUSTOMER_NAME_FIELD_NAME, name);
                RealmResults<Customer> results = query.findAll();
                for (Customer cust : results) {
                    Log.d(TAG, "Selected Customer id + " + cust.getCustomerId());
                    query.findFirst().setSelectedCustomerId(cust.getCustomerId());
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                mHomeView.onSuccessSaveCustomerIdToDB(name);

            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.e(TAG, error.getMessage());
            }
        });
    }

    @Override
    public void saveCustomerIntoDB(final String customerName) {

        if (mRealm.isClosed()) {
            mRealm = Realm.getDefaultInstance();
        }

        final String currentTimeStamp = DateTimeUtil.getCurrentTimeStamp();
        final int customerId = CustomerIdUtils.getNewCustomerId();

        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Customer customerObj = realm.createObject(Customer.class, customerId);
                customerObj.setCustomerName(customerName);
                customerObj.setCurrentTimeStamp(currentTimeStamp);
            }
        });
        readCustomerFromDB();
        mHomeView.notifyToAdapter();
    }

    @Override
    public String getUserName() {
        Login login = mRealm.where(Login.class).findFirst();
        if (login != null && login.getFullName() != null) {
            return login.getFullName();
        } else {
            return null;
        }
    }

    @Override
    public boolean checkIfUserAvailable(String name) {
        for (Customer custName : customerList) {
            if (name.equalsIgnoreCase(custName.getCustomerName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void populatePaymentBulkRequestData() {

        int paymentId = PaymentUtils.getNewPaymentId();
        Random rand = new Random();
        String currentDate = DateTimeUtil.currentDate(AppConstants.DATE_FORMAT_YYYY_MM_DD);
        List<BulkPaymentData> bulkPaymentDataList = new ArrayList<>();
        Realm realmInstance = Realm.getDefaultInstance();
        RealmQuery<CustomerSalesInvoice> salesInvoiceRealmQuery = realmInstance.where(CustomerSalesInvoice.class)
                .equalTo(AppConstants.IS_INVOICE_SYNCED, true).notEqualTo(AppConstants.IS_RETURN, 1);
        RealmResults<CustomerSalesInvoice> invoiceRealmResults = salesInvoiceRealmQuery.findAll();
        for (CustomerSalesInvoice invoice : invoiceRealmResults) {
            if (invoice.getStatus().equalsIgnoreCase(AppConstants.UNPAID)) {
                final int x = rand.nextInt(paymentId++);
                BulkPaymentData data = new BulkPaymentData();
                data.setInvoiceId(invoice.getStringInvoiceID());
                data.setModeOfPayment("Cash");
                data.setAllocatedAmount(invoice.getAmountPaidByUser());
                data.setPaymentId(x + "-" + paymentId);
                data.setPostingDate(currentDate);
                bulkPaymentDataList.add(data);
            }
        }
        BulkPaymentApiModel model = new BulkPaymentApiModel();
        model.setData(bulkPaymentDataList);
    }


    @Override
    public void populateSalesInvoiceCreationData() {
        if (mRealm.isClosed()) {
            mRealm = Realm.getDefaultInstance();
        }
        String wareHouse = null;
        String accountHead = null;
        String chargeType = null;
        String description = null;
        String sellingPriceList = null;
        int discount;
        int rate = 0;

        RealmQuery<POSSettingModel> posQuery = mRealm.where(POSSettingModel.class);
        POSSettingModel posModel = posQuery.findFirst();
        if (posModel != null) {
            wareHouse = posModel.getMessage().getData().getWarehouse();
            sellingPriceList = posModel.getMessage().getData().getSellingPriceList();
        }


        RealmQuery<POSSettingTaxis> taxQuery = mRealm.where(POSSettingTaxis.class);
        POSSettingTaxis taxModel = taxQuery.findFirst();
        if (taxModel != null) {
            accountHead = taxModel.getAccountHead();
            chargeType = taxModel.getChargeType();
            description = taxModel.getDescription();
            rate = taxModel.getRate();
        }


        RealmQuery<CustomerSalesInvoice> query = mRealm.where(CustomerSalesInvoice.class).equalTo(AppConstants.IS_INVOICE_SYNCED, false);
        RealmResults<CustomerSalesInvoice> invoiceListResult = query.findAll();

        List<SalesRequestData> salesRequestDataList = new ArrayList<SalesRequestData>();
        for (CustomerSalesInvoice salesInvoice : invoiceListResult) {

            int invoiceNo = salesInvoice.getInvoiceNumber();

            List<SalesInvoiceItem> salesInvoiceItemList = new ArrayList<SalesInvoiceItem>();
            List<SalesInvoicePayment> salesInvoicePaymentList = new ArrayList<SalesInvoicePayment>();
            List<SalesInvoiceTaxis> salesInvoiceTaxisList = new ArrayList<SalesInvoiceTaxis>();

            SalesInvoiceTaxis taxis = new SalesInvoiceTaxis();
            taxis.setAccountHead(accountHead);
            taxis.setChargeType(chargeType);
            taxis.setDescription(description);
            taxis.setRate(rate);
            salesInvoiceTaxisList.add(taxis);

//          Get all data for provided invoice number
            RealmQuery<CustomerSalesInvoice> invoiceRealmQuery = mRealm.where(CustomerSalesInvoice.class)
                    .equalTo(AppConstants.INVOICE_NUMBER_FIELD_NAME, invoiceNo);
            RealmResults<CustomerSalesInvoice> customerResult = invoiceRealmQuery.findAll();

            for (CustomerSalesInvoice in : customerResult) {

                SalesRequestData requestData = new SalesRequestData();
                String invoiceDate;
                invoiceDate = DateTimeUtil.currentDate(AppConstants.DATE_FORMAT_YYYY_MM_DD);
                requestData.setCustomerName(in.getCustomerName());
                requestData.setCustomerId(in.getStringCustomerID());
                requestData.setInvoiceId(in.getStringInvoiceID());
                requestData.setPostingDate(invoiceDate);

                if (in.getIsReturn() != null && in.getIsReturn() == 1) {
                    requestData.setReturn(1);
                    requestData.setReturnAgainstInvoiceNumber(in.getReturnAgainstInvoiceNumber());
                }

//                add product info
                RealmQuery<ProductLite> liteQuery = mRealm.where(ProductLite.class).equalTo(AppConstants.INVOICE_NUMBER_FIELD_NAME, invoiceNo);
                RealmResults<ProductLite> realmResults = liteQuery.findAll();

                for (ProductLite lite : realmResults) {
                    SalesInvoiceItem item = new SalesInvoiceItem();
                    item.setItemCode(lite.getName());
                    item.setItemName(lite.getItemName());

                    if (in.getIsReturn() != null && in.getIsReturn() == 1) {
                        item.setQty(-lite.getItemCount());
                    } else {
                        item.setQty(lite.getItemCount());
                    }

                    item.setRate(lite.getPrice());
                    item.setWarehouse(wareHouse);
                    salesInvoiceItemList.add(item);
                }

                requestData.setAdditionalDiscountPercentage(in.getAdditionalDiscountPercentage());
                requestData.setSellingPriceList(sellingPriceList);

//                 add payment for current item
                SalesInvoicePayment payment = new SalesInvoicePayment();
                if (in.getStatus().equalsIgnoreCase(AppConstants.PAID)) {
                    payment.setAmount(in.getGrandTotal());
                } else if (in.getStatus().equalsIgnoreCase(AppConstants.UNPAID)) {
                    payment.setAmount(in.getAmountPaidByUser());
                }

//                payment.setAmount(0);
                payment.setModeOfPayment(AppConstants.MODE_OF_PAYMENT_CASH);
                salesInvoicePaymentList.add(payment);

                // added mList items
                requestData.setTaxes(salesInvoiceTaxisList);
                requestData.setItems(salesInvoiceItemList);
                requestData.setPayments(salesInvoicePaymentList);

                salesRequestDataList.add(requestData);
            }
            SalesInvoiceBulkApiModel model = new SalesInvoiceBulkApiModel();
            model.setData(salesRequestDataList);

        }
        callSalesInvoiceBulkApi(salesRequestDataList);
    }


    @Override
    public void callCreateSalesInvoiceBulkApi(List<SalesRequestData> salesRequestDataList) {

        String sid = SharedPreferenceManager.getSharedPreferenceManager(mContext).getSID();
        ApiInterface apiInterface = ApiClient.getClient(mContext).create(ApiInterface.class);
        Call<SalesInvoiceBulkApiResponse> modelCall = apiInterface
                .callSalesInvoiceBulkApi(sid, salesRequestDataList);

        modelCall.enqueue(new Callback<SalesInvoiceBulkApiResponse>() {
            @Override
            public void onResponse(Call<SalesInvoiceBulkApiResponse> call,
                                   Response<SalesInvoiceBulkApiResponse> response) {
                mHomeView.hideSyncDialog();
                if (response.isSuccessful()) {
                    //// TODO: 12/8/17  add error body to SI creation
//                    saveSyncInvoicesToDB(response.body());
//                    updateInvoiceStatus();
                    Log.d(TAG, "SalesInvoiceBulkApiResponse:- " + response.body().getMessage().getStatus());
                } else if (response.code() == AppConstants.RESPONSE_FORBIDDEN) {
                    mHomeView.hideLoading();
                    mHomeView.showSessionExpiryDialog();
                }
            }

            @Override
            public void onFailure(Call<SalesInvoiceBulkApiResponse> call, Throwable t) {
                mHomeView.showOnFailureAlert();
                mHomeView.hideSyncDialog();
            }
        });
    }

    @Override
    public void callSalesInvoiceBulkApi(List<SalesRequestData> salesRequestDataList) {
        if (!NetworkConncetionUtility.getInstance(mContext).isConnected()) {
            return;
        }
        String sid = SharedPreferenceManager.getSharedPreferenceManager(mContext).getSID();
        ApiInterface apiInterface = ApiClient.getClient(mContext).create(ApiInterface.class);
        Call<SalesInvoiceBulkApiResponse> modelCall =
                apiInterface.callSalesInvoiceBulkApi(sid, salesRequestDataList);

        modelCall.enqueue(new Callback<SalesInvoiceBulkApiResponse>() {
            @Override
            public void onResponse(Call<SalesInvoiceBulkApiResponse> call,
                                   Response<SalesInvoiceBulkApiResponse> response) {
                if (mHomeView != null) {
                    mHomeView.hideSyncDialog();
                    if (response.isSuccessful() && response.body().getMessage().getData() != null) {
                        for (SalesInvoiceResponseData data : response.body().getMessage().getData()) {
                            if (data.getStatus().equals(AppConstants.SI_SYNC_SUCESS)) {
                                Log.d(TAG, "response:- " + data.getInvoiceId());
                            } else {
                                Log.d(TAG, "onResponse: fail to sync- " + data.getInvoiceId());
                            }
                        }
                        saveSyncInvoicesToDB(response.body().getMessage());
                    } else if (response.code() == AppConstants.RESPONSE_FORBIDDEN) {
                        mHomeView.showSessionExpiryDialog();
                    }
                    populateExpenseData();
                }
            }

            @Override
            public void onFailure(Call<SalesInvoiceBulkApiResponse> call, Throwable t) {
                Log.e(TAG, "SalesInvoiceBulkApiResponse Failure " + t.getMessage());

            }
        });

    }

    private void updateInvoiceStatus() {
        Realm realmInstance = Realm.getDefaultInstance();
        realmInstance.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<SalesInvoiceResponseData> result =
                        realm.where(SalesInvoiceResponseData.class).findAll();
                for (SalesInvoiceResponseData response : result) {
                    RealmResults<CustomerSalesInvoice> realmResults =
                            realm.where(CustomerSalesInvoice.class).findAll();
                    for (CustomerSalesInvoice salesInvoice : realmResults) {
                        salesInvoice.setPaymentInitialte(true);
                        if (response.getStatus().equals("Success")) {
                            if (response.getInvoiceId().equalsIgnoreCase(salesInvoice.getStringInvoiceID())) {
                                salesInvoice.setInvoiceSync(true);
                                salesInvoice.setErpInvoiceId(response.getErpInvoiceId());
                            }
                        } else {
                            if (response.getInvoiceId().equalsIgnoreCase(salesInvoice.getStringInvoiceID())) {
                                salesInvoice.setInvoiceSync(false);
                                salesInvoice.setErpInvoiceId(response.getErpInvoiceId());
                            }
                        }
                    }
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                checkResult();
//                populatePaymentBulkRequestData();
            }
        });
    }

    private void checkResult() {
        Realm realmInstance = Realm.getDefaultInstance();
        RealmQuery<CustomerSalesInvoice> realmQuery = realmInstance.where(CustomerSalesInvoice.class)
                .equalTo("isInvoiceSync", true);
        RealmResults<CustomerSalesInvoice> results = realmQuery.findAll();
        for (CustomerSalesInvoice invoice : results) {
            Log.d(TAG, "checkResult: invoices Synced " + invoice.getInvoiceNumber());
        }
    }

    @Override
    public void checkSalesInvoicesStatus() {
        RealmQuery<CustomerSalesInvoice> realmQuery = mRealm.where(CustomerSalesInvoice.class)
                .equalTo("isInvoiceSync", false);
        RealmResults<CustomerSalesInvoice> results = realmQuery.findAll();
        if (results.size() > 0) {
            mHomeView.showAlertDialogBeforeLogout(true);
        } else {
            mHomeView.showAlertDialogBeforeLogout(false);
        }
    }

    @Override
    public void populateUnpaidSalesInvoiceData(BulkPaymentData bulkPaymentData) {
        List<BulkPaymentData> bulkPaymentDataList = new ArrayList<>();
        bulkPaymentDataList.add(bulkPaymentData);
        bulkPayment(bulkPaymentDataList);
    }

    private void saveSyncInvoicesToDB(final SalesInvoiceResponseMessage data) {
        Realm realmInstance = Realm.getDefaultInstance();
        realmInstance.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(data);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                updateInvoiceStatus();
            }
        });
    }

    @Override
    public boolean checkIfCustomerExist(String name) {
        RealmQuery<Customer> customerRealmQuery = mRealm.where(Customer.class)
                .contains(AppConstants.CUSTOMER_NAME_FIELD_NAME, name, Case.INSENSITIVE);
        RealmResults<Customer> result = customerRealmQuery.findAll();
        for (Customer cust : result) {
            if (cust.getCustomerName().equalsIgnoreCase(name)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void handleCustomerCreate(String customerName) {
        if (checkIfCustomerExist(customerName)) {
            mHomeView.showCustomerAlreadyPresentError();
            return;
        }
        saveCustomerIntoDB(customerName);
    }


    @Override
    public void populateExpenseData() {
        Realm realmInstance = Realm.getDefaultInstance();
        RealmResults<CustomerExpenseModel> dataRealmResults = realmInstance.where(CustomerExpenseModel.class)
                .equalTo(AppConstants.IS_EXPENSE_SYNC_FIELD_NAME, false).findAll();
        ExpenseData data = null;
        List<ExpenseData> expenseDataList = null;
        ExpenseRequestModel requestModel;
        expenseDataList = new ArrayList<>();

        if (dataRealmResults.size() > 0) {
            for (CustomerExpenseModel model : dataRealmResults) {
                data = new ExpenseData();
                requestModel = new ExpenseRequestModel();
                ExpenseRequestDetails expenseModel = new ExpenseRequestDetails();

                List<ExpenseRequestDetails> expenseDetailsList = new ArrayList<>();

                expenseModel.setExpenseType(model.getExpenseType());
                expenseModel.setAmount(model.getClaimAmount());
                expenseModel.setDescription(model.getClaimDescription());
                expenseDetailsList.add(expenseModel);

                data.setPostingDate(model.getPostingDate());
                data.setExpenseId(model.getStringExpenseID());
                data.setExpenses(expenseDetailsList);

                expenseDataList.add(data);

                requestModel.setData(data);

                callSyncExpenseAPI(data);
            }
        }
    }

    private void callSyncExpenseAPI(ExpenseData modelList) {

        String sid = SharedPreferenceManager.getSharedPreferenceManager(mContext).getSID();

        ApiInterface apiInterface = ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<ExpenseResponse> requestModelCall = apiInterface.callExpenseSyncAPI(sid, modelList);

        requestModelCall.enqueue(new Callback<ExpenseResponse>() {
            @Override
            public void onResponse(Call<ExpenseResponse> call, Response<ExpenseResponse> response) {

                if (mHomeView != null) {
                    mHomeView.hideSyncDialog();
                }
                if (response.isSuccessful() && response.body() != null) {
                    saveExpenseSyncDetailsToDB(response.body());
                } else if (response.code() == AppConstants.RESPONSE_FORBIDDEN) {
                    mHomeView.hideLoading();
                    mHomeView.showSessionExpiryDialog();
                }
            }

            @Override
            public void onFailure(Call<ExpenseResponse> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });

    }

    private void saveExpenseSyncDetailsToDB(final ExpenseResponse body) {
        Realm realmInstance = Realm.getDefaultInstance();
        realmInstance.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(body);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                updateExpenseInvoicesStatus();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.e(TAG, error.getMessage());
            }
        });
    }

    private void updateExpenseInvoicesStatus() {
        final Realm realmInstance = Realm.getDefaultInstance();
        realmInstance.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<ExpenseResponse> addExpenseList = realm.where(ExpenseResponse.class).findAll();
                for (ExpenseResponse expenseResponse : addExpenseList) {
                    RealmResults<CustomerExpenseModel> expenseModelRealmResults =
                            realm.where(CustomerExpenseModel.class).findAll();
                    for (CustomerExpenseModel addExpenseModel : expenseModelRealmResults) {
                        addExpenseModel.setPaymentInitialte(true);
                        if (expenseResponse.getMessage().getPosExpenseId().equalsIgnoreCase(addExpenseModel.getStringExpenseID())) {
                            addExpenseModel.setExpenseIdSync(true);
                        }
                    }
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {

            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.e(TAG, error.getMessage());
            }
        });

    }
}
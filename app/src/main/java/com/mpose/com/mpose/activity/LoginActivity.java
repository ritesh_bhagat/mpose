package com.mpose.com.mpose.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.mpose.com.mpose.MposeApplication;
import com.mpose.com.mpose.R;
import com.mpose.com.mpose.auth.actionlistener.ILoginActionListener;
import com.mpose.com.mpose.auth.model.login.Login;
import com.mpose.com.mpose.auth.view.LoginView;
import com.mpose.com.mpose.dashboard.model.possetting.POSSettingModel;
import com.mpose.com.mpose.di.HasComponent;
import com.mpose.com.mpose.di.component.AuthComponent;
import com.mpose.com.mpose.di.component.DaggerAuthComponent;
import com.mpose.com.mpose.manager.SharedPrefConstants;
import com.mpose.com.mpose.manager.SharedPreferenceManager;
import com.mpose.com.mpose.retrofit.ApiClient;
import com.mpose.com.mpose.utils.AppConstants;
import com.mpose.com.mpose.utils.CommonAlertDialog;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

/**
 * A login screen that offers login via username/password.
 */
public class LoginActivity extends BaseActivity implements LoginView,
        HasComponent<AuthComponent> {

    private static final String TAG = LoginActivity.class.getSimpleName();

    @Bind(R.id.sign_in_button)
    Button mSignInButton;

    @Bind(R.id.ed_username)
    EditText mUserName;

    @Bind(R.id.ed_password)
    EditText mPassword;

    @Bind(R.id.txt_forget_password)
    TextView mForgotPassword;

    @Bind(R.id.ed_domain)
    EditText mDomain;

    @Inject
    ILoginActionListener mLoginActionListener;

    private AuthComponent mAuthComponent;

    private Realm mRealm;
    private Animation previousAnim;
    private Animation nextAnim;

    private MaterialDialog mMaterialDialog;

    private String mUserFullName;

    private AlertDialog mDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        initializeInjector();
        this.getComponent().inject(this);

        mLoginActionListener.setView(this);

        mRealm = Realm.getDefaultInstance();

        mForgotPassword.setText(Html.fromHtml(getString(R.string.forgot_password_link)));
        initMaterialDialog();
        initAnimUtils();

        mLoginActionListener.populateDomainFromDB();

        boolean flag = SharedPreferenceManager.getSharedPreferenceManager(this).getUserLoggedIn();
        if (flag) {
            redirect(false);
        }
    }

    private void setTempValuesToViews() {
        mUserName.setText("Administrator");
        mPassword.setText("admin");
        mDomain.setText("myecom.indictranstech.com");
    }


    private void initAnimUtils() {
        previousAnim = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.left_to_center);
        nextAnim = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.center_to_left);
    }

    private void initMaterialDialog() {
        mMaterialDialog = new MaterialDialog.Builder(LoginActivity.this)
                .title(R.string.loading_data)
                .content(R.string.please_wait)
                .progress(true, 0)
                .progressIndeterminateStyle(false)
                .build();
    }

    public void initializeInjector() {
        this.mAuthComponent = DaggerAuthComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .build();
    }

    @OnClick(R.id.sign_in_button)
    public void loginClick() {
        if (!isValidateLogin()) {
            return;
        }

        String strDomain = mDomain.getText().toString().trim();
//        String strDomain = "myecom.indictranstech.com";

        String domain = strDomain.replaceAll(" ", "");

        mLoginActionListener.domainGetCall(domain);

    }

    @Override
    public void showNetworkConnectionError() {
        CommonAlertDialog.ShowAlertDialog(LoginActivity.this, getResources().getString(R.string.internet_connection_title),
                getResources().getString(R.string.internet_connection_message), R.style.dialog_animation_fade);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }

    private boolean isValidateLogin() {
        boolean isVarified = false;
        if (TextUtils.isEmpty(mUserName.getText())) {
            mUserName.setError(getString(R.string.blank_user_name_error));
            return isVarified;
        }

        if (TextUtils.isEmpty(mPassword.getText())) {
            mPassword.setError(getString(R.string.blank_password_name_error));
            return isVarified;
        }
        if (TextUtils.isEmpty(mDomain.getText())) {
            mDomain.setError(getString(R.string.blank_domain_name_error));
            return isVarified;
        }

        return true;
    }

    @Override
    public void showLoading() {
        mMaterialDialog.show();
    }

    @Override
    public void hideLoading() {
        if (mMaterialDialog.isShowing()) {
            mMaterialDialog.dismiss();
        }
    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void redirectToLogin() {

    }

    @Override
    public void onLoginSucess(Login body) {

    }

    @Override
    public void onPosSettingSuccessResponse(POSSettingModel body) {

    }

    @Override
    public void domainSucessForgotPassword(String strDomain) {
        openForgotPasswordDialog(strDomain);
    }

    private void openForgotPasswordDialog(String strDomain) {

        mDialog = new AlertDialog.Builder(this)
                .setView(R.layout.forgot_password)
                .setCancelable(false)
                .show();

        Button forgotPasswordButton = (Button) mDialog.findViewById(R.id.forgot_password_button);
        final EditText edForgotPass = (EditText) mDialog.findViewById(R.id.ed_forgot_email);
        Button forgotBackButton = (Button) mDialog.findViewById(R.id.forgot_back_button);
        forgotBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        forgotPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = edForgotPass.getText().toString();
                if (isValidEmail(email)) {
                    mLoginActionListener.forgotPassword(email);
                } else {
                    edForgotPass.setError(getString(R.string.valid_email_error));
                }
            }
        });

    }

    @OnClick(R.id.txt_forget_password)
    public void forgotPasswordClick() {
        if (TextUtils.isEmpty(mDomain.getText().toString().trim())) {
            mDomain.setError(getResources().getString(R.string.blank_domain_name_error));
            return;
        }

        String strDomain = mDomain.getText().toString();
        mLoginActionListener.domainGetCallForForgotPassword(strDomain);

    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void onLoginFailed() {
        CommonAlertDialog.ShowAlertDialog(LoginActivity.this, getResources().getString(R.string.auth_error_title),
                getResources().getString(R.string.auth_error_message), R.style.dialog_animation_fade);
    }

    @Override
    public void somethingWentWrong() {
        CommonAlertDialog.ShowAlertDialog(LoginActivity.this, getResources().getString(R.string.something_went_wrong),
                getResources().getString(R.string.something_went_wrong_message), R.style.dialog_animation_fade);
    }

    @Override
    public void showToastMessage() {
        Toast.makeText(this, getResources().getString(R.string.something_went_wrong_message), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void sendUserDataToHome(String fullName) {
        mUserFullName = fullName;
    }

    @Override
    public void redirect(boolean flag) {
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.USER_FULL_NAME, mUserFullName);
        bundle.putBoolean(AppConstants.LOGIN_CALL, flag);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    @Override
    public void clearLocalPreferences() {
        ((MposeApplication) getApplication()).setCustomerSelected(false);
        SharedPreferenceManager.getSharedPreferenceManager(this).getmEditor().remove(SharedPrefConstants.USER_LOGGED_IN);
        SharedPreferenceManager.getSharedPreferenceManager(this).getmEditor().remove(SharedPrefConstants.SID);
        SharedPreferenceManager.getSharedPreferenceManager(this).getmEditor().remove(SharedPrefConstants.SYNC_DATE_TIME);
        SharedPreferenceManager.getSharedPreferenceManager(this).getmEditor().remove(SharedPrefConstants.IS_FIRST_TIME_CALLED);
        SharedPreferenceManager.getSharedPreferenceManager(this).getmEditor().remove(SharedPrefConstants.TOTAL_COUNT_FROM_API);
        SharedPreferenceManager.getSharedPreferenceManager(this).getmEditor().remove(SharedPrefConstants.SELECTED_CUSTOMER);
        SharedPreferenceManager.getSharedPreferenceManager(this).getmEditor().remove(SharedPrefConstants.IS_CUSTOMER_SELECTED);
        SharedPreferenceManager.getSharedPreferenceManager(this).getmEditor().remove(SharedPrefConstants.USER_FULL_NAME);
        SharedPreferenceManager.getSharedPreferenceManager(this).getmEditor().remove(SharedPrefConstants.USER_NAME);

//        SharedPreferences mySPrefs =PreferenceManager.getDefaultSharedPreferences(this);
//        SharedPreferences.Editor editor = mySPrefs.edit();
//        editor.remove(String key);
//        editor.apply();


//        SharedPreferenceManager.getSharedPreferenceManager(this).clearAllPreferences();
    }

    @Override
    public AuthComponent getComponent() {
        return mAuthComponent;
    }

    @Override
    public void onDomainPingCallFailure() {
        ApiClient.resetRetrofit();
        SharedPreferenceManager.getSharedPreferenceManager(LoginActivity.this).clearAllPreferences();
        CommonAlertDialog.ShowAlertDialog(LoginActivity.this, getResources().getString(R.string.domain_error_title),
                getResources().getString(R.string.domain_error_message), R.style.dialog_animation_fade);

    }

    @Override
    public void onDomainPingCallNetworkFailure() {
        ApiClient.resetRetrofit();
        SharedPreferenceManager.getSharedPreferenceManager(LoginActivity.this).clearAllPreferences();
        CommonAlertDialog.ShowAlertDialog(LoginActivity.this, getResources().getString(R.string.network_error_title),
                getResources().getString(R.string.network_error_message), R.style.dialog_animation_fade);
    }

    @Override
    public void onForgotPaswordSucess(String message) {
        CommonAlertDialog.ShowDialog(LoginActivity.this, AppConstants.FORGOT_PASSWORD_TITLE,
                message, R.style.dialog_animation_fade);
        if (mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    @Override
    public void onForgotPasswordFailure(String message) {
        CommonAlertDialog.ShowAlertDialog(LoginActivity.this, AppConstants.FORGOT_PASSWORD_TITLE,
                message, R.style.dialog_animation_fade);
    }

    @Override
    public void callLoginApiView() {
        String strUserName = mUserName.getText().toString().trim();
        String strPassword = mPassword.getText().toString().trim();
        String strDomain = mDomain.getText().toString().trim();
        mLoginActionListener.populateLoginObject(strUserName, strPassword, strDomain);
    }

    @Override
    public void setDomainToView(String domainName) {
        mDomain.setText(domainName);
    }
}


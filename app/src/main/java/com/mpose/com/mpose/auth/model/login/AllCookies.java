package com.mpose.com.mpose.auth.model.login;

/**
 * Created by lp-ritesh on 6/7/17.
 */

public class AllCookies {
    String userImage;
    String systemImage;
    String sid;
    String fullName;
    String userId;

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getSystemImage() {
        return systemImage;
    }

    public void setSystemImage(String systemImage) {
        this.systemImage = systemImage;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}

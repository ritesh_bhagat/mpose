package com.mpose.com.mpose.salesinvoice.models.unpaid;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Anjali on 27-09-2017.
 */

public class UnpaidSalesInvoice {

    @SerializedName("status")
    private String status;
    @SerializedName("pos_id")
    private String posId;
    @SerializedName("customer")
    private String customer;
    @SerializedName("outstanding_amount")
    private Double outstandingAmount;
    @SerializedName("erp_invoice_id")
    private String erpInvoiceId;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPosId() {
        return posId;
    }

    public void setPosId(String posId) {
        this.posId = posId;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public Double getOutstandingAmount() {
        return outstandingAmount;
    }

    public void setOutstandingAmount(Double outstandingAmount) {
        this.outstandingAmount = outstandingAmount;
    }

    public String getErpInvoiceId() {
        return erpInvoiceId;
    }

    public void setErpInvoiceId(String erpInvoiceId) {
        this.erpInvoiceId = erpInvoiceId;
    }
}

package com.mpose.com.mpose.retrofit;

import com.mpose.com.mpose.auth.model.login.DomainModel;
import com.mpose.com.mpose.auth.model.login.ForgotPassword;
import com.mpose.com.mpose.auth.model.login.Login;
import com.mpose.com.mpose.auth.model.logout.LogOut;
import com.mpose.com.mpose.dashboard.model.companydetails.CompanyDetailsModel;
import com.mpose.com.mpose.dashboard.model.possetting.POSSettingModel;
import com.mpose.com.mpose.dashboard.model.report.ReportDetailModel;
import com.mpose.com.mpose.expense.models.expenserequest.ExpenseData;
import com.mpose.com.mpose.expense.models.expenseresponse.ExpenseResponse;
import com.mpose.com.mpose.pos.models.AllProducts;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkapi.SalesRequestData;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkresponse.SalesInvoiceBulkApiResponse;
import com.mpose.com.mpose.salesinvoice.models.unpaid.UnpaidSalesInvoiceResponseMessage;
import com.mpose.com.mpose.sync.model.bulkpaymentrequest.BulkPaymentData;
import com.mpose.com.mpose.sync.model.bulkpaymentresponse.BulkPaymentResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by lp-ritesh on 24/5/17.
 */

public interface ApiInterface {

    @FormUrlEncoded
    @POST("method/login")
    Call<Login> authenticateUser(@Field("usr") String username, @Field("pwd") String password);


    @GET("method/erpnext_magento.pos_api.get_items")
    Call<AllProducts> getAllProducts(@Header("Cookie") String sid,
                                     @Query("modified") String modifled,
                                     @Query("limit_start") String limitStart,
                                     @Query("limit_page_length") String limitPageLength);


    @GET("method/erpnext_magento.pos_api.forgot_password")
    Call<ForgotPassword> forgotPassword(@Query("user_id") String email);

    @GET("method/erpnext_magento.pos_api.get_pos_settings")
    Call<POSSettingModel> posSetting(@Header("Cookie") String sid);

    @GET
    Call<DomainModel> pingServer(@Url String url);

    @GET("method/erpnext_magento.pos_api.get_daily_total_sales")
    Call<ReportDetailModel> getPOSReport(@Header("Cookie") String sid,
                                         @Query("start_date") String startDate,
                                         @Query("end_date") String endDate);

    @POST("method/erpnext_magento.pos_api.create_pos_invoices")
    Call<SalesInvoiceBulkApiResponse> callSalesInvoiceBulkApi(@Header("Cookie") String sid,
                                                              @Body List<SalesRequestData> data);

    @POST("method/erpnext_magento.pos_api.log_out")
    Call<LogOut> logOutUser(@Header("Cookie") String sid);

    @POST("method/erpnext_magento.pos_api.make_pos_payment_entry")
    Call<BulkPaymentResponse> postBulkPayment(@Header("Cookie") String sid,
                                              @Body List<BulkPaymentData> model);

    @POST("method/erpnext_magento.pos_api.create_expense_entry")
    Call<ExpenseResponse> callExpenseSyncAPI(@Header("Cookie") String sid, @Body ExpenseData modelList);

    @GET("method/erpnext_magento.pos_api.get_si")
    Call<UnpaidSalesInvoiceResponseMessage> getUnpaidSalesInvoice(@Header("Cookie") String sid);

    @GET("method/erpnext_magento.pos_api.get_pf_details")
    Call<CompanyDetailsModel> getCompanyDetailsForPrint(@Header("Cookie") String sid);
}



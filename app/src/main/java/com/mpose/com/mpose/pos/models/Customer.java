package com.mpose.com.mpose.pos.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lp-ritesh on 17/7/17.
 */

public class Customer extends RealmObject {

    @SerializedName("customerName")
    private String customerName;

    @PrimaryKey
    @SerializedName("customerId")
    private Integer customerId;

    @SerializedName("selectedCustomerId")
    private Integer selectedCustomerId;

    @SerializedName("CustomerSalesInvoice")
    RealmList<CustomerSalesInvoice> allCartItemsRealmList;

    @SerializedName("current_timestamp")
    private String currentTimeStamp;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public RealmList<CustomerSalesInvoice> getAllCartItemsRealmList() {
        return allCartItemsRealmList;
    }

    public void setAllCartItemsRealmList(RealmList<CustomerSalesInvoice> allCartItemsRealmList) {
        this.allCartItemsRealmList = allCartItemsRealmList;
    }

    public Integer getSelectedCustomerId() {
        return selectedCustomerId;
    }

    public void setSelectedCustomerId(Integer selectedCustomerId) {
        this.selectedCustomerId = selectedCustomerId;
    }

    public String getStringCustomerID() {
        return  currentTimeStamp + "-" + customerId;
    }

    public String getCurrentTimeStamp() {
        return currentTimeStamp;
    }

    public void setCurrentTimeStamp(String currentTimeStamp) {
        this.currentTimeStamp = currentTimeStamp;
    }

    @Override
    public String toString() {
        return customerName;
    }
}

package com.mpose.com.mpose.auth.model.logout;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lp-ritesh on 26/7/17.
 */

public class LogOut {

    @SerializedName("message")
    private LogOutMessage message;

    @SerializedName("mDomainName")
    private static String mDomainName;

    public LogOutMessage getMessage() {
        return message;
    }

    public void setMessage(LogOutMessage message) {
        this.message = message;
    }

    public static String getDomainName() {
        return mDomainName;
    }

    public static void setDomainName(String domainName) {
        mDomainName = domainName;
    }
}

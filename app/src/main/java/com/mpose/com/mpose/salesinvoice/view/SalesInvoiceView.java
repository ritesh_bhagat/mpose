package com.mpose.com.mpose.salesinvoice.view;

import com.mpose.com.mpose.common.BaseView;
import com.mpose.com.mpose.pos.models.CustomerSalesInvoice;

import java.util.List;

/**
 * Created by lp-ritesh on 18/7/17.
 */

public interface SalesInvoiceView extends BaseView {

    void callAdapter(List<CustomerSalesInvoice> mCustomerStatusInvoicesList);

    void showEmptyInvoiceImage();
}

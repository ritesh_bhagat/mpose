package com.mpose.com.mpose.dashboard.model.possetting;

/**
 * Created by lp-ritesh on 25/7/17.
 */

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class POSSettingModel extends RealmObject{

    @PrimaryKey
    @SerializedName("id")
    private int id;

    @SerializedName("message")
    private POSSettingMessage message;

    public POSSettingMessage getMessage() {
        return message;
    }

    public void setMessage(POSSettingMessage message) {
        this.message = message;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}


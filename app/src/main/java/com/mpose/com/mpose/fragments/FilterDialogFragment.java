package com.mpose.com.mpose.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mpose.com.mpose.R;
import com.mpose.com.mpose.adapters.FilterProductsAdapter;
import com.mpose.com.mpose.eventbus.RefreshListEvent;
import com.mpose.com.mpose.pos.models.FilterProducts;
import com.mpose.com.mpose.pos.models.Product;
import com.mpose.com.mpose.pos.presenter.FilterProductPresenter;
import com.mpose.com.mpose.pos.view.FilterProductsView;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.morrox.fontinator.FontButton;
import io.realm.RealmList;

/**
 * Created by lp-ritesh on 28/12/17.
 */

public class FilterDialogFragment extends DialogFragment implements FilterProductsView {


    @Bind(R.id.filter_list_view)
    RecyclerView mRecyclerView;

    @Bind(R.id.apply_filter)
    FontButton mApplyFilter;

    @Bind(R.id.clear_filter)
    FontButton mClearFilter;

//    @Bind(R.id.cancel_action)
//    FontButton mCancelAction;

    private RealmList<FilterProducts> mProductRealmList;
    private FilterProductPresenter mFilterProductPresenter;
    private FilterProductsAdapter mFilterProductsAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFilterProductPresenter = new FilterProductPresenter();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_filter_dialog, container, false);
        ButterKnife.bind(this, rootView);
        getDialog().setCanceledOnTouchOutside(false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFilterProductPresenter.setView(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initializeRecyclerView();

        mFilterProductPresenter.populateGroupListFromDB();
    }

    private void setFilterAdapter() {

    }

    private void initializeRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void showListToView(RealmList<FilterProducts> list) {
        if (list != null) {
            mProductRealmList = list;
            mFilterProductsAdapter = new FilterProductsAdapter(getActivity(), mProductRealmList);
            mRecyclerView.setAdapter(mFilterProductsAdapter);
        }
    }

    @OnClick(R.id.apply_filter)
    public void applyFilterAction() {
        RealmList<FilterProducts> list = mFilterProductsAdapter.getFilterList();
        if (list != null)
            mFilterProductPresenter.updateFilterModel(list);
    }

    @OnClick(R.id.clear_filter)
    public void clearFilterAction() {
        mFilterProductPresenter.removeAllFilter();

    }

//    @OnClick(R.id.cancel_action)
//    public void cancelAction() {
//        if (getDialog().isShowing()) {
//            getDialog().dismiss();
//        }
//    }

    @Override
    public void navigateToPreviousScreen() {
        if (getDialog().isShowing()) {
            getDialog().dismiss();
        }
        EventBus.getDefault().post(new RefreshListEvent());
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void redirectToLogin() {

    }
}

package com.mpose.com.mpose.pos.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by lp-ritesh on 22/7/17.
 */

public class ProductLite extends RealmObject {

    @SerializedName("name")
    private String name;
    @SerializedName("item_name")
    private String itemName;
    @SerializedName("qty")
    private Integer qty;
    @SerializedName("price")
    private double price;
    @SerializedName("itemCount")
    private Integer itemCount;
    @SerializedName("invoiceNumber")
    private Integer invoiceNumber;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getItemCount() {
        return itemCount;
    }

    public void setItemCount(Integer itemCount) {
        this.itemCount = itemCount;
    }

    public Integer getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(Integer invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    @Override
    public String toString() {
        return "ProductLite{" +
                "name='" + name + '\'' +
                ", itemName='" + itemName + '\'' +
                ", qty=" + qty +
                ", price=" + price +
                ", itemCount=" + itemCount +
                ", invoiceNumber='" + invoiceNumber + '\'' +
                '}';
    }
}

package com.mpose.com.mpose.dashboard.view;

import com.github.mikephil.charting.data.BarEntry;
import com.mpose.com.mpose.common.BaseView;
import com.mpose.com.mpose.dashboard.model.possetting.POSSettingModel;
import com.mpose.com.mpose.dashboard.model.report.ReportDetailModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lp-ritesh on 5/7/17.
 */

public interface DashboardView extends BaseView {

    void onPosSettingSuccessResponse(POSSettingModel body);

//    void onPosChartSucessResponse(ReportDetailModel body);

    void createCustomerDB();

//    void showLocalData(ReportDetailModel mReportDetailModel);

    void showOnFailureAlert();

    void showSessionExpiryDialog();

    void showNetworkConnectionError();

    void showChart(ArrayList<BarEntry> mChartEntries, List<String> mPostingDatesList);

    String getStartDate();

    String getEndDate();
}

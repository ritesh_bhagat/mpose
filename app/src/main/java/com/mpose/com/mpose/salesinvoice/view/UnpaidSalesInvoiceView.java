package com.mpose.com.mpose.salesinvoice.view;

import com.mpose.com.mpose.common.BaseView;
import com.mpose.com.mpose.salesinvoice.models.unpaid.UnpaidSalesInvoice;

import java.util.List;

/**
 * Created by Anjali on 27-09-2017.
 */

public interface UnpaidSalesInvoiceView extends BaseView {

    void showNetworkConnectionError();

    void showUnpaidSalesInvoiceList(List<UnpaidSalesInvoice> salesInvoices);
}

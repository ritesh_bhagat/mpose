package com.mpose.com.mpose.pos.actionlistener;

import android.view.MenuItem;

import com.mpose.com.mpose.eventbus.AddItemCountEvent;
import com.mpose.com.mpose.pos.models.Customer;
import com.mpose.com.mpose.pos.models.Product;
import com.mpose.com.mpose.pos.models.ProductMessage;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkapi.SalesRequestData;

import java.util.List;

import io.realm.RealmResults;

/**
 * Created by lp-ritesh on 7/7/17.
 */

public interface IPOSActionListener<T> {

    void setView(T t);

    void getAllProductsFromAPI(String date, String limitStart, String pageLength);

    void callSalesInvoiceBulkApi(List<SalesRequestData> model);

    List<Product> getAllProductsFromDB();

    void processPayButtonAction();


    void saveProductDataToCartDB(double grandTotal, double discountValue, double outStandingValue, double amountPaidByuser);

    void resetProductCount();

    void saveProductsToDB(ProductMessage message);

    void manageItemForCart(AddItemCountEvent addItemCountEvent);

    RealmResults<Product> getAllCartProductListFromDB();

    void checkTotalAndUpdateView();

    List<Product> transformRealmResultToList(RealmResults<Product> realmResults);

    void fetchVatTaxPercentFromDB();

    void calculateGrandTotalWithDiscount(CharSequence discountVal, int count);

    void resume(String s, String s1, String s2);

    void handleCustomerCreate(String name);

    List<Customer> fetchCustomerFromDB();

    void setSelectedCustomerIdToDB(String name);

    boolean handleOptionSelectedEvent(MenuItem item);

}

package com.mpose.com.mpose.adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mpose.com.mpose.MposeApplication;
import com.mpose.com.mpose.R;
import com.mpose.com.mpose.eventbus.AddItemCountEvent;
import com.mpose.com.mpose.pos.models.CustomerSalesInvoice;
import com.mpose.com.mpose.pos.models.Product;
import com.mpose.com.mpose.utils.CommonAlertDialog;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by lp-ritesh on 3/7/17.
 */

public class PayLayoutRecyclerViewAdapter extends RecyclerView.Adapter<PayLayoutRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = PayLayoutRecyclerViewAdapter.class.getSimpleName();
    private Context mContext;

    private Map<String, CustomerSalesInvoice> allCartItemsList;
    private List<String> allItemNames;
    List<Product> mAllCartItemsList;
    private Realm realmInstance;
    private int lastPosition = -1;
    private Product mAllCartItems;
    private ViewHolder mViewHolder;

    public PayLayoutRecyclerViewAdapter(Context context, List<Product> allCartItemsList) {
        this.mContext = context;
        this.mAllCartItemsList = allCartItemsList;
        realmInstance = Realm.getDefaultInstance();
        getCartList();
    }

    private void getCartList() {
        RealmQuery<Product> result = realmInstance.where(Product.class).greaterThan("itemCount", 0);
        final RealmResults<Product> data = result.findAll();
        mAllCartItemsList.clear();
        mAllCartItemsList.addAll(data);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_pay_layout_item_row, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        mViewHolder = holder;
        final Product allCartItems = mAllCartItemsList.get(position);
        mAllCartItems = allCartItems;
        holder.productName.setText(allCartItems.getItemName());
        holder.productPrice.setText(mContext.getResources().getString(R.string.rs) + " " + allCartItems.getPrice());

        holder.productCount.setText(String.valueOf(allCartItems.getItemCount()));

        holder.plusButton.setTag(allCartItems);
        holder.plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelected = ((MposeApplication) mContext.getApplicationContext()).isCustomerSelected();
                if (!isSelected) {
                    showAlerDialog();
                    return;
                }
                realmInstance.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        Log.d(TAG, "execute: ");
                        final Product product = (Product) holder.minusButton.getTag();
                        product.setItemCount(product.getItemCount() + 1);
                        EventBus.getDefault().post(new AddItemCountEvent(mAllCartItems.getName(), true, 0, true));
                    }
                });
            }
        });

        holder.minusButton.setTag(allCartItems);
        holder.minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelected = ((MposeApplication) mContext.getApplicationContext()).isCustomerSelected();
                if (!isSelected) {
                    showAlerDialog();
                    return;
                }
                realmInstance.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        final Product product = (Product) holder.minusButton.getTag();
                        product.setItemCount(product.getItemCount() - 1);
                        EventBus.getDefault().post(new AddItemCountEvent(mAllCartItems.getName(), true, 0, true));
                    }
                });
            }
        });

        holder.productCount.setTag(allCartItems);
        holder.productCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                boolean isSelected = ((MposeApplication) mContext.getApplicationContext()).isCustomerSelected();
                if (!isSelected) {
                    showAlerDialog();
                    return;
                }

                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.dialog_edit_product_count);
                dialog.getWindow().getAttributes().windowAnimations = R.style.dialog_animation_slide_down_up;
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
                final EditText edCount = (EditText) dialog.findViewById(R.id.ed_product_count);
                final Button done = (Button) dialog.findViewById(R.id.txt_product_count_button);
                edCount.setText(holder.productCount.getText().toString());
                done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Product product = (Product) view.getTag();
                        if (!edCount.getText().toString().trim().equals("")) {
                            int count = Integer.parseInt(edCount.getText().toString().trim());

                            int itemCount = -1;
                            if (count > 0) {
                                itemCount = Integer.parseInt(String.valueOf(count));
                            } else {
                                itemCount = Integer.parseInt(String.valueOf(count));
                            }
                            final int finalItemCount = itemCount;
                            realmInstance.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    product.setItemCount(finalItemCount);
                                    EventBus.getDefault().post(new AddItemCountEvent(mAllCartItems.getName(), true, finalItemCount, true));
                                }
                            });

                        }else {
                            realmInstance.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    product.setItemCount(0);
                                    EventBus.getDefault().post(new AddItemCountEvent(mAllCartItems.getName(), true, 0, true));
                                }
                            });
                        }
                        dialog.dismiss();
                    }
                });
            }
        });
    }

    private void showAlerDialog() {
        CommonAlertDialog.ShowAlertDialog(mContext, mContext.getString(R.string.select_user),
                mContext.getString(R.string.select_user_message),
                R.style.dialog_animation_slide_down_up);
    }

    public void setList(List<Product> allCartItemsList) {
        this.mAllCartItemsList = allCartItemsList;
    }

    @Override
    public int getItemCount() {
        return mAllCartItemsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.txt_pay_item_name)
        TextView productName;

        @Bind(R.id.txt_pay_product_price)
        TextView productPrice;

        @Bind(R.id.txt_pay_product_count)
        TextView productCount;

        @Bind(R.id.img_pay_plus)
        ImageView plusButton;

        @Bind(R.id.img_pay_minus)
        ImageView minusButton;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}

package com.mpose.com.mpose.sync.model.bulkpaymentrequest;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class BulkPaymentData extends RealmObject {

    @SerializedName("invoice_id")
    private String invoiceId;
    @SerializedName("posting_date")
    private String postingDate;
    @SerializedName("mode_of_payment")
    private String modeOfPayment;
    @SerializedName("payment_id")
    private String paymentId;
    @SerializedName("allocated_amount")
    private Double allocatedAmount;
    @SerializedName("paymentStatus")
    private String paymentStatus;

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(String postingDate) {
        this.postingDate = postingDate;
    }

    public String getModeOfPayment() {
        return modeOfPayment;
    }

    public void setModeOfPayment(String modeOfPayment) {
        this.modeOfPayment = modeOfPayment;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public Double getAllocatedAmount() {
        return allocatedAmount;
    }

    public void setAllocatedAmount(Double allocatedAmount) {
        this.allocatedAmount = allocatedAmount;
    }

    public String getStringPaymentID() {
        return "POS-" + paymentId;
    }

    @Override
    public String toString() {
        return "BulkPaymentData{" +
                "invoiceId='" + invoiceId + '\'' +
                ", postingDate='" + postingDate + '\'' +
                ", modeOfPayment='" + modeOfPayment + '\'' +
                ", paymentId=" + paymentId +
                ", allocatedAmount=" + allocatedAmount +
                '}';
    }
}
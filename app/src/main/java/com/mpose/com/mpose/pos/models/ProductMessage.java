package com.mpose.com.mpose.pos.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class ProductMessage extends RealmObject {

    @SerializedName("status")
    private String status;

    @SerializedName("total_count")
    private Integer totalCount;

    @SerializedName("data")
    private RealmList<Product> data = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public RealmList<Product> getData() {
        return data;
    }

    public void setData(RealmList<Product> data) {
        this.data = data;
    }


}
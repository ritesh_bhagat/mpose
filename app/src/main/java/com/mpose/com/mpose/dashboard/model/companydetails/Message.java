package com.mpose.com.mpose.dashboard.model.companydetails;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Message extends RealmObject{

    @SerializedName("status")
    private String status;
    @SerializedName("data")
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
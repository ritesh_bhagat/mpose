package com.mpose.com.mpose.expense.models.expenseresponse;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by lp-ritesh on 19/8/17.
 */

public class ExpenseResponse extends RealmObject{

    @SerializedName("message")
    private ExpenseResponseMessage message;

    public ExpenseResponseMessage getMessage() {
        return message;
    }

    public void setMessage(ExpenseResponseMessage message) {
        this.message = message;
    }

}




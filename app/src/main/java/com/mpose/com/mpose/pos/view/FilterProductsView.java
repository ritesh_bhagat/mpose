package com.mpose.com.mpose.pos.view;

import com.mpose.com.mpose.common.BaseView;
import com.mpose.com.mpose.pos.models.FilterProducts;
import com.mpose.com.mpose.pos.models.Product;

import io.realm.RealmList;

/**
 * Created by lp-ritesh on 28/12/17.
 */

public interface FilterProductsView extends BaseView{

    void showListToView(RealmList<FilterProducts> list);

    void navigateToPreviousScreen();

}

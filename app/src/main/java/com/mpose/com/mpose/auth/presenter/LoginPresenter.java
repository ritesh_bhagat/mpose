package com.mpose.com.mpose.auth.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.mpose.com.mpose.auth.actionlistener.ILoginActionListener;
import com.mpose.com.mpose.auth.model.login.DomainModel;
import com.mpose.com.mpose.auth.model.login.ForgotPassword;
import com.mpose.com.mpose.auth.model.login.Login;
import com.mpose.com.mpose.auth.model.logout.LogOut;
import com.mpose.com.mpose.auth.view.LoginView;
import com.mpose.com.mpose.common.Presenter;
import com.mpose.com.mpose.dashboard.model.companydetails.CompanyDetailsModel;
import com.mpose.com.mpose.dashboard.model.possetting.POSSettingData;
import com.mpose.com.mpose.dashboard.model.possetting.POSSettingModel;
import com.mpose.com.mpose.dashboard.model.possetting.RealmString;
import com.mpose.com.mpose.di.PerActivity;
import com.mpose.com.mpose.manager.SharedPreferenceManager;
import com.mpose.com.mpose.retrofit.ApiClient;
import com.mpose.com.mpose.retrofit.ApiInterface;
import com.mpose.com.mpose.utils.AppConstants;
import com.mpose.com.mpose.utils.CommonAlertDialog;
import com.mpose.com.mpose.utils.NetworkConncetionUtility;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lp-ritesh on 5/7/17.
 */

@PerActivity
public class LoginPresenter extends Presenter implements ILoginActionListener<LoginView> {

    private static final String TAG = LoginPresenter.class.getSimpleName();
    private LoginView mLoginView;
    private Context mContext;
    private Realm mRealm;
    private CommonAlertDialog commonAlertDialog;

    @Inject
    LoginPresenter(Context mContext) {
        this.mContext = mContext;
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    public void setView(LoginView loginView) {
        this.mLoginView = loginView;
    }

    @Override
    public void populateLoginObject(String strUserName, String strPassword, String strDomain) {
        if (!NetworkConncetionUtility.getInstance(mContext).isConnected()) {
            loginUserViaOffline(strUserName, strDomain, strPassword);
        } else {
            Login login = new Login();
            login.setUserName(strUserName);
            login.setPassword(strPassword);
            callLoginApi(login, strDomain);
            saveUserNameToDb(strDomain);
        }

    }


    private void callLoginApi(Login login, final String strDomain) {
        final String userName = login.getUserName();
        final String password = login.getPassword();
        Log.d(TAG, "callLoginApi:strDomain " + strDomain);

        mLoginView.showLoading();

        ApiInterface apiInterface = ApiClient.getClient(mContext).create(ApiInterface.class);
        Call<Login> loginCall = apiInterface.authenticateUser(login.getUserName(), login.getPassword());

        loginCall.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {

                if (response.isSuccessful()) {
                    if (AppConstants.LOGIN_SUCESS.equals(response.body().getMessage())) {
                        checkUsersCredentials(response.body());
                        List<String> cookieList = new ArrayList<String>();
                        okhttp3.Headers headers = response.headers();
                        String cookie = headers.get("Set-Cookie");
                        SharedPreferenceManager.getSharedPreferenceManager(mContext).setSID(cookie);
                        onLoginSuccess(response.body(), strDomain, userName, password);
                        mLoginView.sendUserDataToHome(response.body().getFullName());
                        callPOSSettingAPI();
                        callPrintInfoAPI();
                    }
                } else {
                    Log.d(TAG, response.errorBody().toString());
                    mLoginView.onLoginFailed();
                }
                mLoginView.hideLoading();
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                mLoginView.hideLoading();
                Toast.makeText(mContext, "Something went wrong. Please try again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void callPrintInfoAPI() {
        String sid = SharedPreferenceManager.getSharedPreferenceManager(mContext).getSID();
        ApiInterface apiInterface = ApiClient.getClient(mContext).create(ApiInterface.class);
        Call<CompanyDetailsModel> call = apiInterface.getCompanyDetailsForPrint(sid);
        call.enqueue(new Callback<CompanyDetailsModel>() {
            @Override
            public void onResponse(Call<CompanyDetailsModel> call, Response<CompanyDetailsModel> response) {
                if (response.isSuccessful()) {
                    saveCompanyDetailsToDB(response.body());
                } else {

                }
            }

            @Override
            public void onFailure(Call<CompanyDetailsModel> call, Throwable t) {

            }
        });

    }

    private void saveCompanyDetailsToDB(final CompanyDetailsModel body) {
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(CompanyDetailsModel.class);
                realm.copyToRealm(body);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {

            }
        });

    }

    private void checkUsersCredentials(Login body) {
        String userName = SharedPreferenceManager.getSharedPreferenceManager(mContext).getUserName();
        if (userName != null) {
            if (!body.getFullName().equalsIgnoreCase(userName)) {
                removeLoginFromDB();
                mLoginView.clearLocalPreferences();
            }
        }
    }

    private void loginUserViaOffline(String userName, String strDomain, String strPassword) {
        RealmQuery<Login> realmQuery = mRealm.where(Login.class);
        Login loginModel = realmQuery.findFirst();
        if (loginModel != null) {
            mLoginView.hideLoading();
            if (!loginModel.getDomainName().equals(strDomain)) {
                mLoginView.onDomainPingCallFailure();
                return;
            }
            if (loginModel.getUserName().equalsIgnoreCase(userName)
                    && loginModel.getPassword().equals(strPassword)) {
                mLoginView.redirect(true);
            } else {
                mLoginView.onLoginFailed();
            }
        } else {
            mLoginView.hideLoading();
            mLoginView.showToastMessage();
        }
    }

    private void removeLoginFromDB() {
        RealmQuery<Login> query = mRealm.where(Login.class);
        final Login login = query.findFirst();
        if (login != null) {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(@NonNull Realm realm) {
                    realm.deleteAll();
                }
            });
        } else {
            removeLoginFromDB();
        }
    }

    private void callPOSSettingAPI() {
        String sid = SharedPreferenceManager.getSharedPreferenceManager(mContext).getSID();
        ApiInterface apiInterface = ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<POSSettingModel> posSettingCall = apiInterface.posSetting(sid);
        posSettingCall.enqueue(new Callback<POSSettingModel>() {
            @Override
            public void onResponse(Call<POSSettingModel> call, Response<POSSettingModel> response) {
                if (response.isSuccessful()) {
                    savePOSSettingDataToDB(response.body());
                    mLoginView.redirect(true);
                } else {
                    mLoginView.somethingWentWrong();
                }
            }

            @Override
            public void onFailure(Call<POSSettingModel> call, Throwable t) {
                Log.e(TAG, "POSSettingError- " + t.getMessage());
                mLoginView.somethingWentWrong();
            }
        });


    }

    private void savePOSSettingDataToDB(final POSSettingModel body) {
        final Realm realm = Realm.getDefaultInstance();
        final int primaryKey = 1;
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgrealm) {
                body.getMessage().getData().setId(primaryKey);
                bgrealm.insertOrUpdate(body.getMessage().getData());
                bgrealm.insertOrUpdate(body);

            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                RealmResults<POSSettingData> results = realm.where(POSSettingData.class).findAll();
                for (POSSettingData model : results) {
                    for (RealmString str : model.getExpenseType()) {
                        Log.d(TAG, "onSuccess: str- " + str);
                    }
                }
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.e(TAG, "POS mRealm Db- " + error.getMessage());
            }
        });

    }

    private void onLoginSuccess(Login body, String strDomain, String userName, String password) {
        SharedPreferenceManager.getSharedPreferenceManager(mContext).setUserLoggedIn(true);
        SharedPreferenceManager.getSharedPreferenceManager(mContext).setUserFullName(body.getFullName());
        SharedPreferenceManager.getSharedPreferenceManager(mContext).setUserName(userName);
        saveCredentialToDB(body, strDomain, userName, password);
    }

    @Override
    public void forgotPassword(String email) {
        if (!NetworkConncetionUtility.getInstance(mContext).isConnected()) {
            mLoginView.showNetworkConnectionError();
            return;
        }

        mLoginView.showLoading();
        ApiInterface apiInterface = ApiClient.getClient(mContext).create(ApiInterface.class);

        Call<ForgotPassword> call = apiInterface.forgotPassword(email);
        call.enqueue(new Callback<ForgotPassword>() {
            @Override
            public void onResponse(Call<ForgotPassword> call, Response<ForgotPassword> response) {
                if (response.isSuccessful()) {
                    if (AppConstants.FORGOT_ERROR.equals(response.body().getMessage().getStatus())) {
                        mLoginView.onForgotPasswordFailure(response.body().getMessage().getMessage());
                    } else {
                        mLoginView.onForgotPaswordSucess(response.body().getMessage().getMessage());
                    }

                    //commonAlertDialog.ShowAlertDialog(mContext, AppConstants.INTERNET_CONNECTION_TITLE, AppConstants.INTERNET_CONNECTION_MESSAGE, R.style.dialog_animation_fade);
                    Toast.makeText(mContext, "Status sucess- " + response.body().getMessage().getMessage(), Toast.LENGTH_SHORT).show();

                } else {

                    Toast.makeText(mContext, "Status- " + response.message(), Toast.LENGTH_SHORT).show();
                    //commonAlertDialog.ShowAlertDialog(mContext, AppConstants.INTERNET_CONNECTION_TITLE, AppConstants.INTERNET_CONNECTION_MESSAGE, R.style.dialog_animation_fade);
                }
                mLoginView.hideLoading();
            }

            @Override
            public void onFailure(Call<ForgotPassword> call, Throwable t) {
                mLoginView.hideLoading();
                Toast.makeText(mContext, "Status- " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void domainGetCall(final String strDomain) {
        saveUserDomainToDB(strDomain);
        mLoginView.showLoading();
        if (!NetworkConncetionUtility.getInstance(mContext).isConnected()) {
            mLoginView.callLoginApiView();
//            mLoginView.showNetworkConnectionError();
        } else {
            String url = "http://" + strDomain + "/api/method/erpnext_magento.pos_api.ping";
            Log.d(TAG, "domainUrl " + url);
            ApiInterface apiInterface = ApiClient.getClient(mContext).create(ApiInterface.class);
            Call<DomainModel> call = apiInterface.pingServer(url);
            call.enqueue(new Callback<DomainModel>() {
                @Override
                public void onResponse(Call<DomainModel> call, Response<DomainModel> response) {
                    Log.d(TAG, "response code " + response.code());
                    if (response.isSuccessful()) {
                        mLoginView.callLoginApiView();
                    } else {
                        mLoginView.onDomainPingCallFailure();
                    }
                    mLoginView.hideLoading();
                }

                @Override
                public void onFailure(Call<DomainModel> call, Throwable t) {
                    Log.e(TAG, "response " + t.getMessage());
                    mLoginView.hideLoading();
                    mLoginView.onDomainPingCallFailure();
                }
            });
        }
    }


    private void saveCredentialToDB(final Login body, final String strDomain, final String userName, final String password) {
        if (mRealm.isClosed()) {
            mRealm = Realm.getDefaultInstance();
        }
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                Login login = bgRealm.createObject(Login.class);
                login.setFullName(body.getFullName());
                login.setHomePage(body.getHomePage());
                login.setMessage(body.getMessage());
                login.setUserName(userName);
                login.setPassword(password);
                login.setDomainName(strDomain);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                RealmQuery<Login> results = mRealm.where(Login.class);
                RealmResults<Login> loginRealmResults = results.findAll();
                for (Login fullname : loginRealmResults) {
                    Log.d(TAG, "onSuccess: fullname " + fullname.getFullName());
                    Log.d(TAG, "onSuccess: username " + fullname.getUserName());
                }
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                // Transaction failed and was automatically canceled.
                Log.e(TAG, "RealmDB  POSSettingData stored error" + error.getMessage());
            }
        });
    }

    private void saveUserDomainToDB(String domainName) {
        SharedPreferenceManager.getSharedPreferenceManager(mContext).setDomainName(domainName);
    }

    @Override
    public void domainGetCallForForgotPassword(final String strDomain) {
        SharedPreferenceManager.getSharedPreferenceManager(mContext).setDomainName(strDomain);
        mLoginView.showLoading();
        if (!NetworkConncetionUtility.getInstance(mContext).isConnected()) {
            mLoginView.showNetworkConnectionError();
            return;
        }

        String url = "http://" + strDomain + "/api/method/erpnext_magento.pos_api.ping";
        Log.d(TAG, "domainUrl " + url);
        ApiInterface apiInterface = ApiClient.getClient(mContext).create(ApiInterface.class);
        Call<DomainModel> call = apiInterface.pingServer(url);
        call.enqueue(new Callback<DomainModel>() {
            @Override
            public void onResponse(Call<DomainModel> call, Response<DomainModel> response) {
                Log.d(TAG, "response " + response.code());
                if (response.isSuccessful()) {
                    mLoginView.domainSucessForgotPassword(strDomain);
                } else {
                    mLoginView.onDomainPingCallFailure();
                }
                mLoginView.hideLoading();
            }

            @Override
            public void onFailure(Call<DomainModel> call, Throwable t) {
                Log.e(TAG, "response " + t.getMessage());
                mLoginView.hideLoading();
                mLoginView.onDomainPingCallFailure();
            }
        });

    }

    private void saveUserNameToDb(final String strDomain) {
        final RealmQuery<DomainModel> query = mRealm.where(DomainModel.class).equalTo("message", "Success");
        RealmResults<DomainModel> result = query.findAll();
        Log.d(TAG, "saveUserNameToDb: result " + result.size());
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                DomainModel domainModel = query.findFirst();
                if (domainModel != null) {
                    domainModel.setDomainName(strDomain);
                }
            }
        });

    }


    @Override
    public void populateDomainFromDB() {
        mLoginView.setDomainToView(LogOut.getDomainName());
    }
}
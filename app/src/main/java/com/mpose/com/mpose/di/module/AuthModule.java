package com.mpose.com.mpose.di.module;

import com.mpose.com.mpose.auth.actionlistener.ILoginActionListener;
import com.mpose.com.mpose.auth.presenter.LoginPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lp-ritesh on 5/7/17.
 */

@Module
public class AuthModule {

    @Provides
    ILoginActionListener iLoginActionListener(LoginPresenter loginPresenter) {
        return loginPresenter;
    }


}

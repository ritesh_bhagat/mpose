package com.mpose.com.mpose.pos.presenter;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.mpose.com.mpose.MposeApplication;
import com.mpose.com.mpose.R;
import com.mpose.com.mpose.common.Presenter;
import com.mpose.com.mpose.dashboard.model.possetting.POSSettingTaxis;
import com.mpose.com.mpose.di.PerActivity;
import com.mpose.com.mpose.eventbus.AddItemCountEvent;
import com.mpose.com.mpose.manager.SharedPreferenceManager;
import com.mpose.com.mpose.pos.actionlistener.IPOSActionListener;
import com.mpose.com.mpose.pos.models.AllProducts;
import com.mpose.com.mpose.pos.models.Customer;
import com.mpose.com.mpose.pos.models.CustomerSalesInvoice;
import com.mpose.com.mpose.pos.models.FilterProducts;
import com.mpose.com.mpose.pos.models.Product;
import com.mpose.com.mpose.pos.models.ProductLite;
import com.mpose.com.mpose.pos.models.ProductMessage;
import com.mpose.com.mpose.pos.view.POSView;
import com.mpose.com.mpose.retrofit.ApiClient;
import com.mpose.com.mpose.retrofit.ApiInterface;
import com.mpose.com.mpose.salesinvoice.models.salesinvoice.InvoiceNumberModel;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkapi.SalesRequestData;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkresponse.SalesInvoiceBulkApiResponse;
import com.mpose.com.mpose.utils.AppConstants;
import com.mpose.com.mpose.utils.CustomerIdUtils;
import com.mpose.com.mpose.utils.DateTimeUtil;
import com.mpose.com.mpose.utils.InvoiceUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lp-ritesh on 7/7/17.
 */

@PerActivity
public class POSPresenter extends Presenter implements IPOSActionListener<POSView> {

    private static final String TAG = POSPresenter.class.getSimpleName();

    private Context mContext;
    private POSView mPosView;

    private List<Product> mTempAllProductList = new ArrayList<>();
    private List<Product> mList = new ArrayList<>();
    private RealmList<CustomerSalesInvoice> mAllCartItemsList = new RealmList<>();
    private RealmList<InvoiceNumberModel> mInvoiceList = new RealmList<InvoiceNumberModel>();
    private List<InvoiceNumberModel> mList1 = new ArrayList<InvoiceNumberModel>();
    private RealmList<Customer> customerList = new RealmList<>();


    private int ID = 1;

    private int mDataSize = 0;
    private double mGrandTotalWithGst;

    private double mTempValue = 0;
    private int mDiscountValue = 0;

    private int mInvoiceNo = 0;
    private int vatTaxAmount;

    private Realm mRealm;

    private AllProducts allProductsModel;

    @Inject
    public POSPresenter(Context mContext) {
        this.mContext = mContext;
        this.mRealm = Realm.getDefaultInstance();
    }

    @Override
    public void setView(POSView posView) {
        this.mPosView = posView;
    }

    @Override
    public void resume(String date, String limitStart, String pageLength) {
        super.resume();
        if (allProductsModel == null) {
            getAllProductsFromAPI(date, limitStart, pageLength);
        } else {
            mPosView.showLocalDBData();
            getAllProductsFromAPI(date, limitStart, pageLength);
        }
    }

    @Override
    public void getAllProductsFromAPI(String date, String limitStart, String pageLength) {

        mPosView.showLoading();
        String sid = SharedPreferenceManager.getSharedPreferenceManager(mContext).getSID();
        boolean isFirstTimeCall = SharedPreferenceManager.getSharedPreferenceManager(mContext).getIsFirstTimeCalled();

        if (isFirstTimeCall) {
            mPosView.showLocalDBData();
            date = SharedPreferenceManager.getSharedPreferenceManager(mContext).getSyncDateTime();
        } else {
            date = "";
        }

        ApiInterface apiInterface = ApiClient.getClient(mContext).create(ApiInterface.class);
        Call<AllProducts> allProductsCall = apiInterface.getAllProducts(sid, date, limitStart, pageLength);
        allProductsCall.enqueue(new Callback<AllProducts>() {
            @Override
            public void onResponse(Call<AllProducts> call, Response<AllProducts> response) {
                if (response.isSuccessful()) {
                    allProductsModel = response.body();
                    String status = response.body().getMessage().getStatus();
                    ProductMessage productMessage = response.body().getMessage();

                    saveProductsToDB(productMessage);
                    if (AppConstants.PRODUCT_SUCESS.equals(status)) {
                        if (productMessage.getData().size() > 0 && productMessage.getData() != null) {
                            mDataSize = mDataSize + productMessage.getData().size();
                            if (productMessage.getTotalCount() == mDataSize) {
                                String currentDate = DateTimeUtil.currentDate(AppConstants.DATE_TIME_FORMAT);
                                SharedPreferenceManager.getSharedPreferenceManager(mContext).setSyncDateTime(currentDate);
                                SharedPreferenceManager.getSharedPreferenceManager(mContext).setIsFirstTimeCalled(true);
                                mPosView.hideLoading();
                                saveLastSyncDateTimeToDB();
                                mDataSize = 0;
                            } else {
                                getAllProductsFromAPI("", String.valueOf(mDataSize), "30");
                            }
                            SharedPreferenceManager.getSharedPreferenceManager(mContext)
                                    .setTotalCountFromApi(productMessage.getTotalCount());

                        } else {
                            mPosView.hideLoading();
                            Toast.makeText(mContext, "No product update", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else if (response.code() == AppConstants.RESPONSE_FORBIDDEN) {
                    mPosView.hideLoading();
                    mPosView.showSessionExpiryDialog();
                }
            }

            @Override
            public void onFailure(Call<AllProducts> call, Throwable t) {
                mPosView.hideLoading();
//                mPosView.showLocalDBData();
                mPosView.showOnFailureAlert();
            }
        });
    }

    private void saveLastSyncDateTimeToDB() {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                String syncDateTime = DateTimeUtil.currentDate(AppConstants.DATE_TIME_FORMAT);
                RealmQuery<Product> query = realm.where(Product.class);
                Product product = query.findFirst();
                product.setSyncDateTime(syncDateTime);
            }
        });
    }

    @Override
    public void callSalesInvoiceBulkApi(final List<SalesRequestData> model) {

        String sid = SharedPreferenceManager.getSharedPreferenceManager(mContext).getSID();

        ApiInterface apiInterface = ApiClient.getClient(mContext).create(ApiInterface.class);
        Call<SalesInvoiceBulkApiResponse> modelCall = apiInterface.callSalesInvoiceBulkApi(sid, model);

        modelCall.enqueue(new Callback<SalesInvoiceBulkApiResponse>() {
            @Override
            public void onResponse(Call<SalesInvoiceBulkApiResponse> call, Response<SalesInvoiceBulkApiResponse> response) {
                if (response.isSuccessful()) {
//                    Log.d(TAG, "SalesInvoiceBulkApiResponse:- " + response.body().getMessage().getStatus());
                } else {
                    Log.d(TAG, "SalesInvoiceBulkApiResponse error");
                }
            }

            @Override
            public void onFailure(Call<SalesInvoiceBulkApiResponse> call, Throwable t) {
                Log.e(TAG, "SalesInvoiceBulkApiResponse Failure " + t.getMessage());

            }
        });

    }

    public List<Product> getProductList() {
        RealmResults<Product> realmResults = mRealm.where(Product.class).findAll();
        List<Product> list = new ArrayList<Product>();
        for (Product product : realmResults) {
            Product model = new Product();
            model.setName(product.getName());
            model.setId(product.getId());
            model.setQty(product.getQty());
            model.setDescription(product.getDescription());
            model.setGrandTotal(product.getGrandTotal());
            model.setImage(product.getImage());
            model.setItemAvailableCount(product.getItemAvailableCount());
            model.setItemCode(product.getItemCode());
            model.setItemCount(product.getItemCount());
            model.setItemGroup(product.getItemGroup());
            model.setItemName(product.getItemName());
            model.setPrice(product.getPrice());
            list.add(model);
        }

        return list;
    }

    @Override
    public RealmList<Product> getAllProductsFromDB() {

        RealmQuery<FilterProducts> realmQuery1 = mRealm.where(FilterProducts.class)
                .equalTo("isSelected", true);
        RealmResults<FilterProducts> realmResults1 = realmQuery1.findAll();

        RealmList<Product> list = new RealmList<>();
        if (realmResults1.size() > 0) {
            for (FilterProducts filterProducts : realmResults1) {
                RealmQuery<Product> realmQuery = mRealm.where(Product.class)
                        .equalTo("itemGroup", filterProducts.getGroupName());
                RealmResults realmResults = realmQuery.findAll();
                if (realmResults != null) {
                    list.addAll(realmResults);
                }
            }
            return list;
        }


        RealmList<Product> realmList = new RealmList<>();
        RealmResults<Product> realmResults = mRealm.where(Product.class).findAll();
        realmList.addAll(realmResults);
        return realmList;
    }

    @Override
    public List<Product> transformRealmResultToList(RealmResults<Product> realmResults) {
        List<Product> list = new ArrayList<Product>();
        for (Product product : realmResults) {
            Product model = new Product();
            model.setName(product.getName());
            model.setId(product.getId());
            model.setQty(product.getQty());
            model.setDescription(product.getDescription());
            model.setGrandTotal(product.getGrandTotal());
            model.setImage(product.getImage());
            model.setItemAvailableCount(product.getItemAvailableCount());
            model.setItemCode(product.getItemCode());
            model.setItemCount(product.getItemCount());
            model.setItemGroup(product.getItemGroup());
            model.setItemName(product.getItemName());
            model.setPrice(product.getPrice());
            list.add(model);
        }
        return list;
    }

    @Override
    public void processPayButtonAction() {
        boolean isCustomerSelected = ((MposeApplication) mContext.getApplicationContext()).isCustomerSelected();
        if (!isCustomerSelected) {
            mPosView.showSelectCustomerErroDialog();
            return;
        }
        RealmQuery<Product> query = mRealm.where(Product.class)
                .greaterThan(AppConstants.ITEM_COUNT_FIELD_NAME, 0);
        RealmResults<Product> result = query.findAll();
        if (result.size() <= 0) {
            Toast.makeText(mContext, "No Items in Cart..", Toast.LENGTH_SHORT).show();
            return;
        }

        if (mGrandTotalWithGst <= 0) {
            Toast.makeText(mContext, "No Items in Cart..", Toast.LENGTH_SHORT).show();
            return;
        }
        mPosView.openPaymentDialog();
    }

    @Override
    public void saveProductDataToCartDB(final double grandTotal, final double discountValue, final double outStandingValue, final double amountPaidByUser) {
        if (mRealm.isClosed()) {
            mRealm = Realm.getDefaultInstance();
        }

        final String androidId = Settings.Secure.getString(mContext.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        mInvoiceNo = InvoiceUtils.getNewInvoiceId();


        final String currentTimeStamp = DateTimeUtil.getCurrentTimeStamp();

        final String name = SharedPreferenceManager.getSharedPreferenceManager(mContext).getSelectedCustomer();
        RealmQuery<Product> productRealmQuery = mRealm.where(Product.class).greaterThan(AppConstants.ITEM_COUNT_FIELD_NAME, 0);
        RealmResults<Product> productRealmList = productRealmQuery.findAll();

        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {

                CustomerSalesInvoice customerSalesInvoice = bgRealm.createObject(CustomerSalesInvoice.class,
                        String.valueOf(mInvoiceNo));
                customerSalesInvoice.setCustomerName(name);
                customerSalesInvoice.setGrandTotal(grandTotal);
                customerSalesInvoice.setAdditionalDiscountPercentage(discountValue);
                customerSalesInvoice.setInvoiceDate(DateTimeUtil.currentDate(AppConstants.DATE_TIME_FORMAT));
                customerSalesInvoice.setInvoiceSync(false);
                customerSalesInvoice.setDeviceID(androidId);
                customerSalesInvoice.setCurrentTimeStamp(currentTimeStamp);
                if (outStandingValue > 0) {
                    customerSalesInvoice.setAmountPaidByUser(amountPaidByUser);
                    customerSalesInvoice.setStatus(AppConstants.UNPAID);
                    customerSalesInvoice.setOutStandingAmount(outStandingValue);
                } else {
                    customerSalesInvoice.setOutStandingAmount(0.0);
                    customerSalesInvoice.setStatus(AppConstants.PAID);
                }

                RealmQuery<Product> productRealmQuery = bgRealm.where(Product.class).greaterThan(AppConstants.ITEM_COUNT_FIELD_NAME, 0);
                RealmResults<Product> productRealmList = productRealmQuery.findAll();

                for (Product product : productRealmList) {
                    ProductLite productObj = bgRealm.createObject(ProductLite.class);
                    productObj.setName(product.getName());
                    productObj.setItemName(product.getItemName());
                    productObj.setPrice(product.getPrice());
                    productObj.setQty(product.getQty());
                    productObj.setItemCount(product.getItemCount());
                    productObj.setInvoiceNumber(mInvoiceNo);
                    customerSalesInvoice.addInvoiceProduct(productObj);
                }

                // get customer id from customer table and add it to CustomerSalesInvoice table
                RealmQuery<Customer> invoiceQuery = bgRealm.where(Customer.class).contains(AppConstants.CUSTOMER_NAME_FIELD_NAME, name);
                RealmResults<Customer> results = invoiceQuery.findAll();

                for (Customer customer : results) {
                    customerSalesInvoice.setCustomerId(customer.getCustomerId());
                }
                Intent intent = new Intent("AUTO_SYNC_ACTION");
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
            }
        });
    }

    @Override
    public void resetProductCount() {

        if (mRealm.isClosed()) {
            mRealm = Realm.getDefaultInstance();
        }

        RealmResults<CustomerSalesInvoice> result = mRealm.where(CustomerSalesInvoice.class).findAll();

        RealmQuery<Product> results = mRealm.where(Product.class)
                .greaterThan(AppConstants.ITEM_COUNT_FIELD_NAME, 0);
        final RealmResults<Product> data = results.findAll();
        for (Product product : data) {
            final RealmQuery<Product> realmQuery = mRealm.where(Product.class)
                    .contains(AppConstants.PRODUCT_NAME_FIELD_NAME, product.getName());
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Product allProductsData = realmQuery.findFirst();
                    realmQuery.findFirst().setItemCount(0);
                    // todo check out what this copyToRealmOrUpdate Do here
//                    realm.copyToRealmOrUpdate(realmQuery.findFirst());
                }
            });
        }

        mPosView.clearListAndResetViews();
    }


    public void saveProductsToDB(final ProductMessage message) {
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                bgRealm.insertOrUpdate(message);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                RealmList<Product> list = new RealmList<>();
                RealmResults<Product> realmResults = mRealm.where(Product.class).findAll();
                list.addAll(realmResults);
                RealmQuery<FilterProducts> realmQuery = mRealm.where(FilterProducts.class).equalTo("isSelected", true);
                RealmResults<FilterProducts> result = realmQuery.findAll();
                if (result.size() > 0) {
                    RealmList<Product> realmResults1 = getAllProductsFromDB();
                    mPosView.notifyPosAdapterAgain(realmResults1);
                } else {
                    mPosView.notifyPosAdapterAgain(list);
                }
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
            }
        });
    }


    @Override
    public void manageItemForCart(final AddItemCountEvent addItemCountEvent) {
//        final RealmQuery<Product> realmQuery = mRealm.where(Product.class)
//                .contains(AppConstants.PRODUCT_NAME_FIELD_NAME, addItemCountEvent.getName());
//        if (addItemCountEvent.isIsAdd()) {
//            mRealm.executeTransaction(new Realm.Transaction() {
//                @Override
//                public void execute(Realm realm) {
//                    Product allProductsData = realmQuery.findFirst();
//                    allProductsData.setItemCount(allProductsData.getItemCount() + 1);
//                    if (allProductsData.getItemCount() > 0) {
//                        mPosView.manageTotalLayoutVisibility(true);
//                    }
//                }
//            });
//        } else {
//            mRealm.executeTransaction(new Realm.Transaction() {
//                @Override
//                public void execute(Realm realm) {
//                    Product allProductsData = realmQuery.findFirst();
//                    realmQuery.findFirst().setItemCount(allProductsData.getItemCount() - 1);
//                }
//            });
//        }

        double total;
        total = calculateTotal();
        mPosView.setValueToNetTotalView(total);

        mGrandTotalWithGst = calculateGrandTotal(total);

        mTempValue = mGrandTotalWithGst;
        mPosView.setValueToGrandTotalView(mGrandTotalWithGst);
    }

    @Override
    public RealmResults<Product> getAllCartProductListFromDB() {
        RealmQuery<Product> result = mRealm.where(Product.class)
                .greaterThan(AppConstants.ITEM_COUNT_FIELD_NAME, 0);
        final RealmResults<Product> data = result.findAll();
        return data;
    }


    @Override
    public void checkTotalAndUpdateView() {
        double total = calculateTotal();
        mPosView.setValueToNetTotalView(total);
        mGrandTotalWithGst = calculateGrandTotal(total);
        mPosView.setValueToGrandTotalView(mGrandTotalWithGst);
    }

    @Override
    public void fetchVatTaxPercentFromDB() {
        RealmQuery<POSSettingTaxis> query = mRealm.where(POSSettingTaxis.class);
        RealmResults<POSSettingTaxis> realmResults = query.findAll();
        for (POSSettingTaxis tax : realmResults) {
            if (AppConstants.NET_TOTAL_VAT_PERCENT.equalsIgnoreCase(tax.getChargeType())) {
                vatTaxAmount = tax.getRate();
                String desc = tax.getDescription();
                mPosView.showTaxDetailsToView(vatTaxAmount, desc);
            }
        }
    }

    @Override
    public void calculateGrandTotalWithDiscount(CharSequence s, int count) {
        if (count > 0 && count < 100) {
            if (!s.toString().trim().equals("")) {
                int discountVal = Integer.parseInt(s.toString());
                double finalTotal = discountVal * mTempValue / 100;
                mPosView.updateGrandTotalToView(mTempValue - finalTotal);
                this.mDiscountValue = discountVal;
            }
        } else {
            this.mDiscountValue = 0;
            mPosView.updateGrandTotalToView(mTempValue);
        }

    }

    private double calculateTotal() {
        double total = 0;
        RealmQuery<Product> result = mRealm.where(Product.class).greaterThan(AppConstants.ITEM_COUNT_FIELD_NAME, 0);
        RealmResults<Product> data = result.findAll();
        for (Product productsData : data) {
            total = total + (productsData.getItemCount() * productsData.getPrice());
        }
        return total;
    }

    private double calculateGrandTotal(double total) {
        double finalTotal = vatTaxAmount * total / 100;
        mGrandTotalWithGst = finalTotal + total;
        return mGrandTotalWithGst;
    }

    @Override
    public void handleCustomerCreate(String customerName) {
        if (checkIfCustomerExist(customerName)) {
            mPosView.showCustomerAlreadyPresentError();
            return;
        }
        saveCustomerIntoDB(customerName);
    }

    private void saveCustomerIntoDB(final String customerName) {

        if (mRealm.isClosed()) {
            mRealm = Realm.getDefaultInstance();
        }
        final int customerId = CustomerIdUtils.getNewCustomerId();

        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Customer customerObj = realm.createObject(Customer.class, customerId);
                customerObj.setCustomerName(customerName);
            }
        });
        fetchCustomerFromDB();
        mPosView.setCustomerDataToView(customerName);
        mPosView.notifyToAdapter();
    }

    @Override
    public RealmList<Customer> fetchCustomerFromDB() {

        if (mRealm.isClosed()) {
            mRealm = Realm.getDefaultInstance();
        }
        RealmResults<Customer> realmResults = mRealm.where(Customer.class).findAll();
        customerList.clear();
        customerList.addAll(realmResults);
        return customerList;
    }

    private boolean checkIfCustomerExist(String name) {
        RealmQuery<Customer> customerRealmQuery = mRealm.where(Customer.class)
                .contains(AppConstants.CUSTOMER_NAME_FIELD_NAME, name, Case.INSENSITIVE);
        RealmResults<Customer> result = customerRealmQuery.findAll();
        for (Customer cust : result) {
            if (cust.getCustomerName().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void setSelectedCustomerIdToDB(final String name) {

        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmQuery<Customer> query = realm.where(Customer.class)
                        .equalTo(AppConstants.CUSTOMER_NAME_FIELD_NAME, name);
                RealmResults<Customer> results = query.findAll();
                for (Customer cust : results) {
                    query.findFirst().setSelectedCustomerId(cust.getCustomerId());
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                mPosView.onSucessSaveCustomerIdToDB(name);

            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.e(TAG, error.getMessage());
            }
        });
    }

    @Override
    public boolean handleOptionSelectedEvent(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.img_create_customer:
                mPosView.createCustomer();
                break;
            case R.id.img_filter_product:
                mPosView.openFilterDialog();
                break;
            default:
        }
        return true;
    }
}

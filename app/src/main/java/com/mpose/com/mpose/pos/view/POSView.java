package com.mpose.com.mpose.pos.view;

import com.mpose.com.mpose.common.BaseView;
import com.mpose.com.mpose.pos.models.AllProducts;
import com.mpose.com.mpose.pos.models.Product;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkapi.SalesRequestData;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by lp-ritesh on 7/7/17.
 */

public interface POSView extends BaseView {
    void onProductSuccessResponse(AllProducts body);

    void showLocalDBData();

    void setList();

    void onSalesInvoiceSuccess();

    void openPaymentDialog();

    void showSelectCustomerErroDialog();

    void callSalesInvoiceApi(List<SalesRequestData> salesRequestDataList);

    void clearListAndResetViews();

    void notifyPosAdapterAgain(RealmList<Product> realmResults);

    void manageTotalLayoutVisibility(boolean b);

    void setValueToNetTotalView(double total);

    void setValueToGrandTotalView(double mGrandTotalWithGst);

    void showTaxDetailsToView(int taxAmount, String desc);

    void updateGrandTotalToView(double v);

    void showOnFailureAlert();

    void showSessionExpiryDialog();

    void showCustomerAlreadyPresentError();

    void setCustomerDataToView(String customerName);

    void notifyToAdapter();

    void onSucessSaveCustomerIdToDB(String name);

    void createCustomer();

    void openFilterDialog();
}

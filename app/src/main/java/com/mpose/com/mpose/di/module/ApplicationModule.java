package com.mpose.com.mpose.di.module;

import android.content.Context;

import com.mpose.com.mpose.MposeApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lp-ritesh on 23/6/17.
 */

@Module
public class ApplicationModule {

    private final MposeApplication mposeApplication;

    public ApplicationModule(MposeApplication mposeApplication) {
        this.mposeApplication = mposeApplication;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return this.mposeApplication;
    }
}

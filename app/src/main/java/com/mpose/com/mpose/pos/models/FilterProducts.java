package com.mpose.com.mpose.pos.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lp-ritesh on 28/12/17.
 */

public class FilterProducts extends RealmObject {

    @PrimaryKey
    @SerializedName("group_name")
    String groupName;

    @SerializedName("isSelected")
    private boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public String toString() {
        return "FilterProducts{" +
                "groupName='" + groupName + '\'' +
                ", isSelected=" + isSelected +
                '}';
    }
}

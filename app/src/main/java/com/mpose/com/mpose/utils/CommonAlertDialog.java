package com.mpose.com.mpose.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.mpose.com.mpose.R;

/**
 * Created by lp-ritesh on 3/8/17.
 */

public class CommonAlertDialog {

    private Context mContext;

    public CommonAlertDialog(Context mcContext) {
        this.mContext = mcContext;
    }

    public static void ShowAlertDialog(Context context, String title, String message, int animationSource) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // continue with delete
                dialog.dismiss();
            }
        });
        builder.setIcon(R.mipmap.ic_alert);
        AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = animationSource;
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    public static void ShowDialog(Context context, String title, String message, int animationSource) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        //builder.setNegativeButton("OK", null);
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // continue with delete
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = animationSource;
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }
}

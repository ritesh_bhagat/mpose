package com.mpose.com.mpose.dashboard.presenter;

import android.content.Context;

import com.mpose.com.mpose.common.Presenter;
import com.mpose.com.mpose.dashboard.actionlistener.IListViewItemActionListner;
import com.mpose.com.mpose.dashboard.view.ListViewItemView;

import javax.inject.Inject;

/**
 * Created by lp-ritesh on 5/7/17.
 */

public class ListViewItemPresenter extends Presenter implements IListViewItemActionListner<ListViewItemView> {
    private Context mContext;

    @Inject
    public ListViewItemPresenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void setView(ListViewItemView listViewItemView) {

    }
}

package com.mpose.com.mpose.retrofit;

import android.content.Context;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.mpose.com.mpose.dashboard.model.possetting.RealmString;
import com.mpose.com.mpose.manager.SharedPreferenceManager;
import com.mpose.com.mpose.utils.AppConstants;

import java.io.IOException;
import java.lang.reflect.Type;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by lp-ritesh on 24/5/17.
 */

public class ApiClient {


    private static final String TAG = ApiClient.class.getSimpleName();

    private static Retrofit retrofit = null;
    private static Realm realm;

    public static Retrofit getClient(Context context) {

        String domainName = SharedPreferenceManager.getSharedPreferenceManager(context).getDomainName();

        if (retrofit == null) {

            realm = Realm.getDefaultInstance();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();

            Type tokenString = new TypeToken<RealmList<RealmString>>() {
            }.getType();

            final Gson gson = new GsonBuilder()
                    .setLenient()
                    .setExclusionStrategies(new ExclusionStrategy() {
                        @Override
                        public boolean shouldSkipField(FieldAttributes f) {
                            return f.getDeclaredClass().equals(RealmObject.class);
                        }

                        @Override
                        public boolean shouldSkipClass(Class<?> clazz) {
                            return false;
                        }
                    })
                    .registerTypeAdapter(tokenString, new TypeAdapter<RealmList<RealmString>>() {
                        @Override
                        public void write(JsonWriter out, RealmList<RealmString> value) throws IOException {

                        }

                        @Override
                        public RealmList<RealmString> read(JsonReader in) throws IOException {

                            RealmList<RealmString> list = new RealmList<RealmString>();
                            in.beginArray();
                            while (in.hasNext()) {
                                list.add(new RealmString(in.nextString()));
                            }
                            in.endArray();

                            return list;
                        }
                    })
                    .create();

            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();

            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            builder.addInterceptor(loggingInterceptor);

            /*StethoInterceptor stethoInterceptor = new StethoInterceptor();
            builder.addNetworkInterceptor(stethoInterceptor);*/

            builder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
//                    String cookie = "user_id=Administrator; system_user=yes; full_name=Administrator; sid=a49c44520866d2328d98f32f1368c7f8b9f95a39a674d617d6490ccc";
                    Request request = original.newBuilder()
//                            .header(AppConstants.HEADER_KEY_CONTENT_TYPE, AppConstants.CONTENT_TYPE_APPLICATION_JSON)
//                            .header("Cookie", cookie)
                            .build();
                    return chain.proceed(request);
                }
            });

            retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstants.BASE_URL + domainName + AppConstants.BASE_URL_AFTER)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(builder.build())
                    .build();
        }
        return retrofit;
    }

    public static void resetRetrofit() {
        retrofit = null;
    }
}

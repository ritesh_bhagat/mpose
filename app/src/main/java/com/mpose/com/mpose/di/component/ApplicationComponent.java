package com.mpose.com.mpose.di.component;

import android.content.Context;

import com.mpose.com.mpose.activity.BaseActivity;
import com.mpose.com.mpose.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by lp-ritesh on 23/6/17.
 */

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    void inject(BaseActivity baseActivity);
    Context context();
}

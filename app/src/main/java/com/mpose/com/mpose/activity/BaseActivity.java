package com.mpose.com.mpose.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.mpose.com.mpose.MposeApplication;
import com.mpose.com.mpose.di.component.ApplicationComponent;
import com.mpose.com.mpose.di.module.ActivityModule;
import com.mpose.com.mpose.manager.SharedPreferenceManager;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by lp-ritesh on 23/6/17.
 */

public class BaseActivity extends AppCompatActivity {

    private static final String TAG = BaseActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    ActivityModule getActivityModule() {
        return new ActivityModule(this);
    }


    ApplicationComponent getApplicationComponent() {
        return ((MposeApplication) getApplication()).getApplicationComponent();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    public void navigateToLoginScreen(Context context) {
        Log.d(TAG, "navigateToLoginScreen: ");
        Intent i = new Intent(context, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        SharedPreferenceManager.getSharedPreferenceManager(context).clearAllPreferences();
        context.startActivity(i);
    }

}

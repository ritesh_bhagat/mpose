package com.mpose.com.mpose.di.component;

import android.app.Activity;

import com.mpose.com.mpose.di.PerActivity;
import com.mpose.com.mpose.di.module.ActivityModule;

import dagger.Component;

/**
 * Created by lp-ritesh on 23/6/17.
 */

@PerActivity
@Component(dependencies = {ApplicationComponent.class},
        modules = {ActivityModule.class})
public interface ActivityComponent {

    Activity activity();

}

package com.mpose.com.mpose.eventbus;

/**
 * Created by lp-ritesh on 12/7/17.
 */

public class ListReachedEndEvent {
    private int mListSize;
    private boolean mIsDataAvailable;

    public ListReachedEndEvent(int size, boolean isDataAvailable) {
        this.mIsDataAvailable = isDataAvailable;
        this.mListSize = size;
    }

    public int getmListSize() {
        return mListSize;
    }

    public void setmListSize(int mListSize) {
        this.mListSize = mListSize;
    }

    public boolean isDataAvailable() {
        return mIsDataAvailable;
    }

    public void setDataAvailable(boolean dataAvailable) {
        mIsDataAvailable = dataAvailable;
    }
}

package com.mpose.com.mpose.utils;

import com.mpose.com.mpose.pos.models.Customer;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by lp-ritesh on 4/8/17.
 */

public class CustomerIdUtils {


    private static final int CUSTOMER_ID_START = 10001;


    public static int getNewCustomerId() {
        Realm mRealm = Realm.getDefaultInstance();
        RealmResults<Customer> resultList = mRealm.where(Customer.class).findAllSorted(AppConstants.CUSTOMER_ID_FIELD_NAME, Sort.DESCENDING);
        if (resultList.size() <= 0) {
            return CUSTOMER_ID_START;
        }

        Customer customer = resultList.first();
        if (customer == null) {
            return CUSTOMER_ID_START;
        } else {
            int customerId = -1;
            try {
                customerId = customer.getCustomerId();
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
            }
            return customerId + 1;
        }
    }
}

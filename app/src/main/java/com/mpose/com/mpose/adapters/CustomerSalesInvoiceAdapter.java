package com.mpose.com.mpose.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mpose.com.mpose.R;
import com.mpose.com.mpose.pos.models.ProductLite;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by abc on 7/15/2017.
 */

public class CustomerSalesInvoiceAdapter extends RecyclerView.Adapter<CustomerSalesInvoiceAdapter.ViewHolder> {

    private static final String TAG = CustomerSalesInvoiceAdapter.class.getSimpleName();
    private Context mContext;

    private boolean mFlag = false;
    private boolean mCheckState = false;
    private ItemCountCallback mCallback;
    private int mInvoiceNumber;

    private List<ProductLite> mCurrentSelectedItems;
    private List<ProductLite> mSalesInvoiceList = new ArrayList<>();

    public void setCallBack(ItemCountCallback customerSalesInvoiceFragment) {
        this.mCallback = customerSalesInvoiceFragment;
    }

    public List<ProductLite> getUpdatedList() {
        return mCurrentSelectedItems;
    }

    public interface ItemCountCallback {
        void isItemCountGreater(boolean flag);
    }

    public CustomerSalesInvoiceAdapter(Context context, int invoiceNumber,
                                       ItemCountCallback callback, List<ProductLite> salesInvoiceList) {
        this.mContext = context;
        this.mCallback = callback;
        this.mInvoiceNumber = invoiceNumber;
        mCurrentSelectedItems = new ArrayList<>();
        this.mSalesInvoiceList = salesInvoiceList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.customer_sales_invoice_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final ProductLite salesInvoice = mSalesInvoiceList.get(position);
        holder.productItemName.setText(salesInvoice.getItemName());
        holder.quantityValue.setText(String.valueOf(salesInvoice.getItemCount()));
        holder.rateValue.setText(String.valueOf(salesInvoice.getPrice()));

        holder.totalValue.setText(mContext.getResources().getString(R.string.rs) + " "
                + String.valueOf(salesInvoice.getPrice() * salesInvoice.getItemCount()));

        holder.checkProductValue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    holder.quantityValue.setEnabled(true);
                    mCurrentSelectedItems.add(salesInvoice);
                } else {
                    mCurrentSelectedItems.remove(salesInvoice);
                }
            }
        });

        if (mFlag) {
            holder.checkProductValue.setEnabled(true);
        }
        if (mCheckState) {
            holder.checkProductValue.setChecked(true);
        } else {
            holder.checkProductValue.setChecked(false);
        }

        holder.quantityValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (!s.toString().trim().equals("")) {
                        int itemCount = Integer.parseInt(s.toString().trim());
                        if (itemCount > salesInvoice.getItemCount()) {
                            mCallback.isItemCountGreater(false);
                            return;
                        } else {
                            mCallback.isItemCountGreater(true);
                            salesInvoice.setItemCount(itemCount);
                        }
                    }

                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }

    public void changeCheckboxState() {
        mFlag = true;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mSalesInvoiceList.size();
    }

    public void checkAll(boolean flag) {
        this.mCheckState = flag;
        notifyDataSetChanged();
    }

    public interface CheckItemCount {
        void checkItemCount(boolean flag);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.chck_box_product_value_row_item)
        CheckBox checkProductValue;

        @Bind(R.id.txt_product_row_item)
        TextView productItemName;

        @Bind(R.id.txt_qnty_value_row_item)
        EditText quantityValue;

        @Bind(R.id.txt_rate_value_row_item)
        TextView rateValue;

        @Bind(R.id.txt_total_value_row_item)
        TextView totalValue;

        @Bind(R.id.image_delete)
        ImageView imageViewDelete;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

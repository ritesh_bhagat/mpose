package com.mpose.com.mpose.di.component;

import com.mpose.com.mpose.activity.LoginActivity;
import com.mpose.com.mpose.di.PerActivity;
import com.mpose.com.mpose.di.module.ActivityModule;
import com.mpose.com.mpose.di.module.AuthModule;

import dagger.Component;

/**
 * Created by lp-ritesh on 5/7/17.
 */

@PerActivity
@Component(dependencies = {ApplicationComponent.class},
        modules = {ActivityModule.class, AuthModule.class})
public interface AuthComponent extends ActivityComponent {

    void inject(LoginActivity loginActivity);
}

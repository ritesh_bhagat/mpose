package com.mpose.com.mpose.pos.presenter;

import android.util.Log;

import com.mpose.com.mpose.common.Presenter;
import com.mpose.com.mpose.pos.models.FilterProducts;
import com.mpose.com.mpose.pos.models.Product;
import com.mpose.com.mpose.pos.view.FilterProductsView;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by lp-ritesh on 28/12/17.
 */

public class FilterProductPresenter extends Presenter {
    private static final String TAG = FilterProductPresenter.class.getSimpleName();

    private Realm mRealm;
    private FilterProductsView mFilterProductsView;

    public void setView(FilterProductsView filterProductsView) {
        this.mFilterProductsView = filterProductsView;
        mRealm = Realm.getDefaultInstance();
    }

    private RealmList<FilterProducts> getFilteredList() {
        RealmResults<FilterProducts> filterProductList = mRealm.where(FilterProducts.class).findAll();

        if (filterProductList == null || filterProductList.size() <= 0) {
            return null;
        }

        RealmList<FilterProducts> list = new RealmList<>();
        list.addAll(filterProductList);
        return list;
    }

    public void populateGroupListFromDB() {

        RealmList<FilterProducts> list = getFilteredList();

        if (list == null || list.size() <= 0) {
            RealmResults<Product> realmResults = mRealm.where(Product.class).distinct("itemGroup");
            list = new RealmList<>();
            for (Product product : realmResults) {
                FilterProducts filterProducts = new FilterProducts();
                filterProducts.setGroupName(product.getItemGroup());
                list.add(filterProducts);
            }
            final RealmList<FilterProducts> listUpdated = list;
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.delete(FilterProducts.class);
                    realm.insertOrUpdate(listUpdated);
                }
            });
        }

        mFilterProductsView.showListToView(list);
    }

    public void updateFilterModel(final RealmList<FilterProducts> list) {
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
//                realm.delete(FilterProducts.class);
//                realm.insertOrUpdate(list);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                RealmQuery<FilterProducts> realmQuery = mRealm.where(FilterProducts.class)
                        .equalTo("isSelected", true);
                RealmResults<FilterProducts> realmResults = realmQuery.findAll();
                Log.d(TAG, "onSuccess: size- " + realmResults.size());
                mFilterProductsView.navigateToPreviousScreen();
            }
        });
    }

    public void removeAllFilter() {
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(FilterProducts.class);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                mFilterProductsView.navigateToPreviousScreen();
            }
        });
    }
}

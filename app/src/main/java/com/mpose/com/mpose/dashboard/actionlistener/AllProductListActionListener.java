package com.mpose.com.mpose.dashboard.actionlistener;

import com.mpose.com.mpose.pos.models.Product;

import java.util.List;

/**
 * Created by lp-ritesh on 7/7/17.
 */

public interface AllProductListActionListener {

    void allProductList(List<Product> allProductsDatas);

}

package com.mpose.com.mpose.utils;

/**
 * Created by lp-ritesh on 4/7/17.
 */

public class AppConstants {

    public static final String BASE_URL = "http://";

    public static final String BASE_URL_AFTER = "/api/";

    public static final String NAV_HOME = "Home (Reports)";

    public static final String NAV_SALE_INVOICE = "Sales Invoice";

    public static final String NAV_SYNC_DATA = "SYNC POSSettingData";

    public static final String NAV_POS = "POS";

    public static final String HEADER_KEY_CONTENT_TYPE = "Content-Type";

    public static final String CONTENT_TYPE_APPLICATION_JSON = "application/json";

    public static final String HEADER_KEY_ACCEPT_ENCODING = "Accept-Encoding";

    public static final String ENCODING_GZIP = "gzip, deflate, sdch";

    public static final String LOGIN_SUCESS = "Logged In";

    public static final String PRODUCT_SUCESS = "Success";

    public static final String CUST = "CUST";

    public static final String PAID = "PAID";

    public static final String UNPAID = "UNPAID";

    public static final String INVOICE_RETURN = "RETURN";

    public static final String OVERDUE = "Overdue";

    public static final String FORGOT_PASSWORD_TITLE = "Forgot Password";

    public static final String IS_RETURN = "isReturn";

    public static final String IS_INVOICE_SYNCED = "isInvoiceSync";

    public static final String SYNC_DIALOG_FRAGMENT = "SyncFragment";

    public static final String PAYMENT_ID = "paymentId";

    public static final String SALE_INVOICE_TITLE = "Sales Invoice";

    public static final String SALE_INVOICE_TITLE_MESSAGE = "Sales Invoice Created Sucessfully";

    public static final String PAYMENT_STATUS = "payment_status";

    public static final String INVOICE_SYNC_ERROR = "Error";

    public static final String USER_FULL_NAME = "fullName";

    public static final String SYSTEM_UPDATE = "System Update";

    public static final String SYSTEM_UPDATE_MESSAGE = "All Invoices are up to date";

    public static final String INVOICE_SYNC_ERROR_MESSAGE = "Error occured while SI creation." +
            " Please contact to Administrator";

    public static final String PRODUCT_FALURE_TITLE = "Connection Timeout";

    public static final String PRODUCT_FAILURE_MESSAGE = "Please check the internet connection and try again";

    public static final String LOGIN_CALL = "login_call";

    public static final String PAYMENT_STATUS_FIELD_NAME = "status";

    public static final String SALES_RETURN_ERROR = "Sales Return Error";

    public static final String SALES_RETURN_ERROR_MESSAGE = "Can not return item more than selected items";

    public static final String EXPENSE_ID_FIELD_NAME = "expenseID";

    public static final String CREDIT_NOTE_ISSUED = "Credit Note Issued";

    public static final String RETURN_AGAINST_INVOICE_NUMBER = "returnAgainstInvoiceNumber";

    public static final String IS_EXPENSE_SYNC_FIELD_NAME = "isExpenseIdSync";

    public static final String SI_SYNC_SUCESS = "Success";

    public static final String MODE_OF_PAYMENT_CASH = "Cash";

    public static final String DISCOUNT_ERROR_TITLE = "Discount Error";

    public static final String DISCOUNT_ERROR_MESSAGE = "Discount must be less than 100";

    public static final String SALE_INVOICE_ERROR_TITLE = "SI Error";

    public static final String SALES_INVOICE_ERROR_MESSAGE = "Please Enter Cash Amount";

    public static final String INVOICE_FORMAT = "INVOICE_FORMAT";

    public static final String FILTER_DIALOG_FRAGMENT = "FilterDialogFragment";


    public static String COOKIEE = "Cookie";

    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String DATE_FORMAT = "dd-MM-yyyy";

    public static final String DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd";

    public static final int RESPONSE_FORBIDDEN = 403;

    public static final String FORGOT_ERROR = "Error";

    public static final String INVOICE_NUMBER_FIELD_NAME = "invoiceNumber";

    public static final String CUSTOMER_NAME_FIELD_NAME = "customerName";

    public static final String GRAND_TOTAL_FIELD_NAME = "grandTotal";

    public static final String CUSTOMER_ID_FIELD_NAME = "customerId";

    public static final String ITEM_COUNT_FIELD_NAME = "itemCount";

    public static final String PRODUCT_NAME_FIELD_NAME = "name";

    public static final String INTERNET_CONNECTION_TITLE = "Internet Connection";

    public static final String INTERNET_CONNECTION_MESSAGE = "Please Check your Internet Connection!";

    public static final String AUTH_ERROR_TITLE = "Authentication Error";

    public static final String AUTH_ERROR_MESSAGE = "Incorrect username or password. Please try again";

    public static final String DOMAIN_ERROR_TITLE = "Domain Error";

    public static final String DOMAIN_ERROR_MESSAGE = "This domain is not available. Please try again ";

    public static final String NET_TOTAL_VAT_PERCENT = "On Net Total";


}

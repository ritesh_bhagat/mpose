package com.mpose.com.mpose.pos.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Product extends RealmObject {


    @PrimaryKey
    @SerializedName("name")
    private String name;
    @SerializedName("item_name")
    private String itemName;
    @SerializedName("image")
    private String image;
    @SerializedName("item_group")
    private String itemGroup;
    @SerializedName("item_code")
    private String itemCode;
    @SerializedName("qty")
    private Integer qty;
    @SerializedName("price")
    private Double price;
    @SerializedName("description")
    private String description;

    @SerializedName("id")
    private int id;

    private int itemAvailableCount;

    @SerializedName("itemCount")
    private int itemCount;

    @SerializedName("syncDateTime")
    private String syncDateTime;

    @SerializedName("isSynced")
    private boolean isSynced;

    @SerializedName("grandTotal")
    private double grandTotal;

    @SerializedName("isInvoiceProduct")
    private boolean isInvoiceProduct = false;

    @SerializedName("isSelected")
    private boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public int getItemAvailableCount() {
        return itemAvailableCount;
    }

    public void setItemAvailableCount(int itemAvailableCount) {
        this.itemAvailableCount = itemAvailableCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getItemGroup() {
        return itemGroup;
    }

    public void setItemGroup(String itemGroup) {
        this.itemGroup = itemGroup;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
        this.itemAvailableCount = qty;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSyncDateTime() {
        return syncDateTime;
    }

    public void setSyncDateTime(String syncDateTime) {
        this.syncDateTime = syncDateTime;
    }

    public boolean isSynced() {
        return isSynced;
    }

    public void setSynced(boolean synced) {
        isSynced = synced;
    }

    public boolean isInvoiceProduct() {
        return isInvoiceProduct;
    }

    public void setInvoiceProduct(boolean invoiceProduct) {
        isInvoiceProduct = invoiceProduct;
    }
}
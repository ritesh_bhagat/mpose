package com.mpose.com.mpose.di.module;

import com.mpose.com.mpose.dashboard.actionlistener.IDashboardActionListener;
import com.mpose.com.mpose.dashboard.actionlistener.IGridViewItemActionListener;
import com.mpose.com.mpose.dashboard.actionlistener.IListViewItemActionListner;
import com.mpose.com.mpose.dashboard.presenter.DashboardPresenter;
import com.mpose.com.mpose.dashboard.presenter.GridViewItemPresenter;
import com.mpose.com.mpose.dashboard.presenter.ListViewItemPresenter;
import com.mpose.com.mpose.expense.actionlistener.AddExpenseActionListener;
import com.mpose.com.mpose.expense.actionlistener.ExpenseClaimActionListener;
import com.mpose.com.mpose.expense.presenter.AddExpensePresenter;
import com.mpose.com.mpose.expense.presenter.ExpenseClaimPresenter;
import com.mpose.com.mpose.home.actionlistener.IHomeActionListener;
import com.mpose.com.mpose.home.presenter.HomePresenter;
import com.mpose.com.mpose.pos.actionlistener.IPOSActionListener;
import com.mpose.com.mpose.pos.presenter.POSPresenter;
import com.mpose.com.mpose.salesinvoice.actionlistener.ICustomerSalesInvoiceActionListener;
import com.mpose.com.mpose.salesinvoice.actionlistener.ISalesInvoiceActionListener;
import com.mpose.com.mpose.salesinvoice.actionlistener.IUnpaidSalesInvoiceActionListener;
import com.mpose.com.mpose.salesinvoice.presenter.CustomerSalesInvoicePresenter;
import com.mpose.com.mpose.salesinvoice.presenter.SalesInvoicePresenter;
import com.mpose.com.mpose.salesinvoice.presenter.UnpaidSalesInvoicePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lp-ritesh on 5/7/17.
 */

@Module
public class DashboardModule {

    @Provides
    IDashboardActionListener dashboardActionListener(DashboardPresenter dashboardPresenter) {
        return dashboardPresenter;
    }


    @Provides
    IGridViewItemActionListener gridViewItemActionListener(GridViewItemPresenter gridViewItemPresenter) {
        return gridViewItemPresenter;
    }

    @Provides
    IListViewItemActionListner listViewItemActionListner(ListViewItemPresenter listViewItemPresenter) {
        return listViewItemPresenter;
    }

    @Provides
    IPOSActionListener posActionListener(POSPresenter posPresenter) {
        return posPresenter;
    }


    @Provides
    ISalesInvoiceActionListener salesInvoiceActionListener(SalesInvoicePresenter salesInvoicePresenter) {
        return salesInvoicePresenter;
    }

    @Provides
    ICustomerSalesInvoiceActionListener customerSalesInvoiceActionListener(CustomerSalesInvoicePresenter customerSalesInvoicePresenter) {
        return customerSalesInvoicePresenter;
    }

    @Provides
    IHomeActionListener homeActionListener(HomePresenter homePresenter) {
        return homePresenter;
    }

    @Provides
    AddExpenseActionListener addExpenseActionListener(AddExpensePresenter addExpensePresenter) {
        return addExpensePresenter;
    }

    @Provides
    ExpenseClaimActionListener expenseClaimActionListener(ExpenseClaimPresenter expenseClaimPresenter) {
        return expenseClaimPresenter;
    }

    @Provides
    IUnpaidSalesInvoiceActionListener unpaidSalesInvoiceActionListener(UnpaidSalesInvoicePresenter unpaidSalesInvoicePresenter) {
        return unpaidSalesInvoicePresenter;
    }

}

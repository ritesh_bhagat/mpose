package com.mpose.com.mpose.salesinvoice.presenter;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.mpose.com.mpose.common.Presenter;
import com.mpose.com.mpose.dashboard.model.possetting.POSSettingTaxis;
import com.mpose.com.mpose.di.PerActivity;
import com.mpose.com.mpose.eventbus.SalesInvoiceCallEvent;
import com.mpose.com.mpose.pos.models.CustomerSalesInvoice;
import com.mpose.com.mpose.pos.models.ProductLite;
import com.mpose.com.mpose.salesinvoice.actionlistener.ICustomerSalesInvoiceActionListener;
import com.mpose.com.mpose.salesinvoice.view.CustomerSalesInvoiceView;
import com.mpose.com.mpose.utils.AppConstants;
import com.mpose.com.mpose.utils.DateTimeUtil;
import com.mpose.com.mpose.utils.InvoiceUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by lp-ritesh on 3/8/17.
 */

@PerActivity
public class CustomerSalesInvoicePresenter extends Presenter
        implements ICustomerSalesInvoiceActionListener<CustomerSalesInvoiceView> {
    private static final String TAG = CustomerSalesInvoicePresenter.class.getSimpleName();
    private Context mContext;

    private Realm mRealm;
    private List<ProductLite> mSalesInvoiceList;
    private String mInvoiceNumber;
    private CustomerSalesInvoiceView mCustomerSalesInvoiceView;
    private int vatTaxAmount;
    private int mInvoiceNo = 0;


    @Inject
    public CustomerSalesInvoicePresenter(Context mContext) {
        this.mContext = mContext;
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    public void setView(CustomerSalesInvoiceView customerSalesInvoiceView) {
        this.mCustomerSalesInvoiceView = customerSalesInvoiceView;

    }

    @Override
    public List<ProductLite> getCustomerInvoiceListFromDB(int invoiceNumber) {
        RealmQuery<CustomerSalesInvoice> query = mRealm.where(CustomerSalesInvoice.class)
                .equalTo(AppConstants.INVOICE_NUMBER_FIELD_NAME, invoiceNumber);
        RealmResults<CustomerSalesInvoice> realmResults = query.findAll();

        mSalesInvoiceList = new ArrayList<>();
        for (CustomerSalesInvoice customer : realmResults) {
            for (ProductLite lite : customer.getInvoiceProductsList()) {
                ProductLite productLite = new ProductLite();
                productLite.setName(lite.getName());
                productLite.setItemCount(lite.getItemCount());
                productLite.setInvoiceNumber(lite.getInvoiceNumber());
                productLite.setItemName(lite.getItemName());
                productLite.setPrice(lite.getPrice());
                productLite.setQty(lite.getQty());
                mSalesInvoiceList.add(productLite);
            }
        }
        return mSalesInvoiceList;
    }


    @Override
    public String getStringInvoiceNumberFromDB(int invoiceNumber) {
        RealmQuery<CustomerSalesInvoice> query = mRealm.where(CustomerSalesInvoice.class)
                .equalTo(AppConstants.INVOICE_NUMBER_FIELD_NAME, invoiceNumber);
        CustomerSalesInvoice customerSalesInvoice = query.findFirst();
        if (customerSalesInvoice != null) {
            return customerSalesInvoice.getStringInvoiceID();
        }
        return null;
    }

    @Override
    public void fetchInvoicesForSpinner(String customerName) {
        RealmResults<CustomerSalesInvoice> results = mRealm.where(CustomerSalesInvoice.class)
                .contains(AppConstants.CUSTOMER_NAME_FIELD_NAME, customerName).findAll();
        List<String> invoiceList = new ArrayList<>();
        for (CustomerSalesInvoice invoice : results) {
            invoiceList.add(String.valueOf(invoice.getInvoiceNumber()));
        }
        mCustomerSalesInvoiceView.setInvoiceListToSpinner(invoiceList);
    }

    @Override
    public void fetchTotal(int invoiceNumber) {
        RealmQuery<CustomerSalesInvoice> query = mRealm.where(CustomerSalesInvoice.class)
                .equalTo(AppConstants.INVOICE_NUMBER_FIELD_NAME, invoiceNumber);

        CustomerSalesInvoice salesInvoice = query.findFirst();
        salesInvoice.getGrandTotal();
        mCustomerSalesInvoiceView.setTotalCalculation(salesInvoice.getGrandTotal(),
                salesInvoice.getAdditionalDiscountPercentage(), salesInvoice.getOutStandingAmount());

    }

    @Override
    public String getPaymentStatus(int invoiceNumber) {
        RealmQuery<CustomerSalesInvoice> query = mRealm.where(CustomerSalesInvoice.class)
                .equalTo(AppConstants.INVOICE_NUMBER_FIELD_NAME, invoiceNumber);
        CustomerSalesInvoice salesInvoice = query.findFirst();
        return salesInvoice.getStatus();

    }

    @Override
    public void fetchTotalCalculation(List<ProductLite> mSalesInvoiceList) {

        double total = 0;
        double rateTotal = 0;
        for (ProductLite product : mSalesInvoiceList) {
            total = total + product.getPrice() * product.getItemCount();
            rateTotal = rateTotal + product.getPrice();
        }
        double finalTotal = vatTaxAmount * total / 100;
        double grandTotalWithGst = finalTotal + total;
//        mCustomerSalesInvoiceView.setTotalCalculation(grandTotalWithGst);
    }

    @Override
    public void fetchVatPercentFromDB() {
        RealmQuery<POSSettingTaxis> query = mRealm.where(POSSettingTaxis.class);
        RealmResults<POSSettingTaxis> realmResults = query.findAll();
        for (POSSettingTaxis tax : realmResults) {
            if (AppConstants.NET_TOTAL_VAT_PERCENT.equalsIgnoreCase(tax.getChargeType())) {
                vatTaxAmount = tax.getRate();
                String desc = tax.getDescription();
                mCustomerSalesInvoiceView.showTaxDetailsToView(vatTaxAmount, desc);
            }
        }
    }

    @Override
    public void createCustomerSalesInvoiceForReturn(final List<ProductLite> mCurrentCreditList,
                                                    final int invoiceNumber) {

        final String mCurrentDate = DateTimeUtil.currentDate(AppConstants.DATE_TIME_FORMAT);
        if (mCurrentCreditList.size() <= 0) {
            Toast.makeText(mContext, " Can not generate invoice ", Toast.LENGTH_SHORT).show();
            return;
        }

        final double total = calculateGrandTotalWithGST(mCurrentCreditList);

        final String androidId = Settings.Secure.getString(mContext.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        mInvoiceNo = InvoiceUtils.getNewInvoiceId();
        final String currentTimeStamp = DateTimeUtil.getCurrentTimeStamp();

        String name = null;
        int id = 0;

        // find customer name and id from DB using invoice number
        RealmQuery<CustomerSalesInvoice> realmQuery = mRealm.where(CustomerSalesInvoice.class)
                .equalTo(AppConstants.INVOICE_NUMBER_FIELD_NAME, invoiceNumber);
        CustomerSalesInvoice customerRealmResults = realmQuery.findFirst();
        name = customerRealmResults.getCustomerName();
        id = customerRealmResults.getCustomerId();

        CustomerSalesInvoice customerSalesInvoice = realmQuery.findFirst();
        final String strInvoiceNumber = customerSalesInvoice.getReturnAgainstStringInvoiceID();
        final int returnAgainstInvoiceInt = customerSalesInvoice.getInvoiceNumber();


        final int finalInvoiceNo = mInvoiceNo;
        final String finalName = name;
        final int finalId = id;

        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(final Realm bgRealm) {
                final CustomerSalesInvoice salesInvoice = bgRealm.createObject(CustomerSalesInvoice.class,
                        String.valueOf(finalInvoiceNo));
                salesInvoice.setCustomerName(finalName);
                salesInvoice.setGrandTotal(total);
                salesInvoice.setCustomerId(finalId);
                salesInvoice.setInvoiceDate(mCurrentDate);
                salesInvoice.setStatus(AppConstants.INVOICE_RETURN);
                salesInvoice.setReturnAgainstInvoiceNumber(strInvoiceNumber);
                salesInvoice.setReturnAgainstInvoiceNumberInt(returnAgainstInvoiceInt);
                salesInvoice.setReturn(1);
                salesInvoice.setDeviceID(androidId);
                salesInvoice.setCurrentTimeStamp(currentTimeStamp);


                for (int i = 0; i < mCurrentCreditList.size(); i++) {
                    ProductLite product = mCurrentCreditList.get(i);
                    ProductLite productObj = bgRealm.createObject(ProductLite.class);
                    productObj.setName(product.getName());
                    productObj.setItemName(product.getItemName());
                    productObj.setPrice(product.getPrice());
                    productObj.setQty(product.getQty());
                    productObj.setItemCount(product.getItemCount());
                    productObj.setInvoiceNumber(finalInvoiceNo);
                    salesInvoice.addInvoiceProduct(productObj);
                }

            }

        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                changeReturnAgainstStatus();
                EventBus.getDefault().post(new SalesInvoiceCallEvent());
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
            }
        });
    }


    public void changeReturnAgainstStatus() {

        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                RealmQuery<CustomerSalesInvoice> realmQuery = realm.where(CustomerSalesInvoice.class)
                        .equalTo(AppConstants.IS_RETURN, 1);
                RealmResults<CustomerSalesInvoice> invoiceRealmResults = realmQuery.findAll();

                for (CustomerSalesInvoice invoice : invoiceRealmResults) {

                    RealmQuery<CustomerSalesInvoice> query = realm.where(CustomerSalesInvoice.class)
                            .equalTo(AppConstants.INVOICE_NUMBER_FIELD_NAME, invoice.getReturnAgainstInvoiceNumberInt());

                    CustomerSalesInvoice customerSalesInvoice = query.findFirst();
                    customerSalesInvoice.setStatus(AppConstants.CREDIT_NOTE_ISSUED);
                    Log.d(TAG, "execute: " + invoice.getReturnAgainstInvoiceNumberInt());
//                    customerSalesInvoice.setOutStandingAmount(invoice.getOutStandingAmount());

                }
            }
        });
    }

    @Override
    public void fetchReturnInvoice(int invoiceNumber) {
        RealmQuery<CustomerSalesInvoice> realmQuery = mRealm.where(CustomerSalesInvoice.class)
                .equalTo(AppConstants.INVOICE_NUMBER_FIELD_NAME, invoiceNumber);
        CustomerSalesInvoice invoice = realmQuery.findFirst();
        if (invoice.getReturnAgainstInvoiceNumber() == null) {
            mCustomerSalesInvoiceView.setReturnAgainstInvoice(null);
            return;
        }
        String returnInvoice = invoice.getReturnAgainstInvoiceNumber();
        mCustomerSalesInvoiceView.setReturnAgainstInvoice(returnInvoice);

    }

    @Override
    public void fetchOutstandingAmount(int mInvoiceNumber) {
        RealmQuery<CustomerSalesInvoice> realmQuery = mRealm.where(CustomerSalesInvoice.class)
                .equalTo(AppConstants.INVOICE_NUMBER_FIELD_NAME, mInvoiceNumber);
        CustomerSalesInvoice invoice = realmQuery.findFirst();
        if (invoice.getOutStandingAmount() != null) {
            mCustomerSalesInvoiceView.setOutstandingAmount(invoice.getOutStandingAmount());
        }

    }

    @Override
    public void fetchCustomerIdFromDB(int mInvoiceNumber) {
        RealmQuery<CustomerSalesInvoice> query = mRealm.where(CustomerSalesInvoice.class)
                .equalTo(AppConstants.INVOICE_NUMBER_FIELD_NAME, mInvoiceNumber);
        CustomerSalesInvoice result = query.findFirst();
        int custId = result.getCustomerId();
        String name1 = result.getCustomerName();
        mCustomerSalesInvoiceView.setCustomerIDToView(result.getStringCustomerID());
        mCustomerSalesInvoiceView.setCustomerNameToView(name1, result.getStringCustomerID());
    }

    @Override
    public void getCustomerSaleInvoiceCreationDateFromDB(int invoiceNumber) {
        RealmQuery<CustomerSalesInvoice> invoiceRealmQuery = mRealm.where(CustomerSalesInvoice.class)
                .equalTo(AppConstants.INVOICE_NUMBER_FIELD_NAME, invoiceNumber);
        CustomerSalesInvoice invoiceDate = invoiceRealmQuery.findFirst();
        invoiceDate.getInvoiceDate();
        mCustomerSalesInvoiceView.setSalesInvoiceDate(invoiceDate.getInvoiceDate());
    }

    private double calculateGrandTotalWithGST(List<ProductLite> list) {
        double total = 0;
        int rateTotal = 0;
        double finalTotal;
        for (ProductLite product : list) {
            total = total + product.getPrice() * product.getItemCount();
        }
        finalTotal = vatTaxAmount * total / 100;
        return total;
    }

    @Override
    public void handleMenuItemClick(MenuItem item) {

    }
}

package com.mpose.com.mpose.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.mpose.com.mpose.utils.NetworkConncetionUtility;

import java.util.Calendar;

/**
 * Created by Anjali on 11-08-2017.
 */

public class DbUpdateReceiver extends BroadcastReceiver {

    private static final String TAG = DbUpdateReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        if (NetworkConncetionUtility.getInstance(context).isConnected()) {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent i = new Intent(context, AutoSyncBroadcastReceiver.class); // explicit intent
            PendingIntent intentExecuted = PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, 1);
//            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
//                    calendar.getTimeInMillis(), 2 * 60 * 1000, intentExecuted);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
                    calendar.getTimeInMillis(), AlarmManager.INTERVAL_HOUR, intentExecuted);


        }
    }
}

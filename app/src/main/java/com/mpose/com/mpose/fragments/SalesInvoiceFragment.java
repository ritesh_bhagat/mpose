package com.mpose.com.mpose.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mpose.com.mpose.R;
import com.mpose.com.mpose.adapters.SalesInvoiceAdapter;
import com.mpose.com.mpose.di.HasComponent;
import com.mpose.com.mpose.di.component.DashboardComponent;
import com.mpose.com.mpose.eventbus.ShowCustomeToolbarEvent;
import com.mpose.com.mpose.pos.models.CustomerSalesInvoice;
import com.mpose.com.mpose.salesinvoice.actionlistener.ISalesInvoiceActionListener;
import com.mpose.com.mpose.salesinvoice.view.SalesInvoiceView;
import com.mpose.com.mpose.utils.AppConstants;
import com.mpose.com.mpose.utils.DateTimeUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by lp-ritesh on 20/7/17.
 */

public class SalesInvoiceFragment extends BaseFragment implements SalesInvoiceView,
        HasComponent<DashboardComponent> {

    private static final String TAG = SalesInvoiceFragment.class.getSimpleName();
    @Bind(R.id.sales_invoice_recycler_view)
    RecyclerView mSalesInvoiceRecyclerView;

    @Bind(R.id.txt_date_view)
    TextView mCurrentDateView;

    @Bind(R.id.empty_invoices)
    ImageView mEmptyInvoice;

    @Bind(R.id.filter_sales_invoice)
    EditText edFilterInvoice;

    @Bind(R.id.img_filter)
    ImageView filterImage;

    @Bind(R.id.filter_invoices_layout)
    RelativeLayout filterLayout;

    @Bind(R.id.filter_close_icon)
    ImageView closeIcon;

    @Inject
    ISalesInvoiceActionListener mSalesInvoiceActionListener;

    private RealmList<CustomerSalesInvoice> mSearchResultList;
    private List<CustomerSalesInvoice> mAllSalesInvoices;

    private DashboardComponent mDashboardComponent;
    private Realm mRealmInstance;
    private RealmList<CustomerSalesInvoice> mCustomerStatusInvoicesList = new RealmList<>();
    private SalesInvoiceAdapter mSalesInvoiceAdapter;
    private String mCurrentDate;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRealmInstance = Realm.getDefaultInstance();
        mCurrentDate = DateTimeUtil.currentDate(AppConstants.DATE_FORMAT);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_customer_invoice_list, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.getComponent(DashboardComponent.class).inject(this);

        mSalesInvoiceActionListener.setView(this);

        setToolBarTitle();
        initLayoutComponent();
        setCurrentDateToView();
        fetchCustomerListFromDB();
        mAllSalesInvoices = mSalesInvoiceActionListener.getAllInvoicesList();
        mSalesInvoiceActionListener.checkSalesInvoicesSyncStatus();

        edFilterInvoice.addTextChangedListener(forFilterInvoices);

        if (isAdded()) {
            EventBus.getDefault().post(new ShowCustomeToolbarEvent(false));
        }
    }

    TextWatcher forFilterInvoices = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                String input = s.toString();
                mSearchResultList = new RealmList<CustomerSalesInvoice>();
                for (int i = 0; i < mAllSalesInvoices.size(); i++) {
                    if (mAllSalesInvoices.get(i).getStatus().toLowerCase().trim()
                            .contains(input.toLowerCase().trim())
                            || mAllSalesInvoices.get(i).getCustomerName().toLowerCase().trim()
                            .contains(input.toLowerCase().trim())
                            || mAllSalesInvoices.get(i).getErpInvoiceId().toLowerCase().trim()
                            .contains(input.toLowerCase().trim())) {
                        mSearchResultList.add(mAllSalesInvoices.get(i));
                    }
                }
                setNewList(mSearchResultList);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void setNewList(RealmList<CustomerSalesInvoice> searchResultList) {
        Log.d(TAG, "setNewList: mSearchResultList " + searchResultList.size());
        mSalesInvoiceAdapter.setFilteredList(searchResultList);
        mSalesInvoiceAdapter.notifyDataSetChanged();
    }

    private void fetchCustomerListFromDB() {
        mSalesInvoiceActionListener.getCustomerInvoiceList();
        mSalesInvoiceActionListener.sortListByTimeStamp();
    }

    private void setSalesInvoiceAdapter(List<CustomerSalesInvoice> mCustomerStatusInvoicesList) {
        this.mCustomerStatusInvoicesList.addAll(mCustomerStatusInvoicesList);
        mSalesInvoiceAdapter = new SalesInvoiceAdapter(getActivity(), this.mCustomerStatusInvoicesList);
        mSalesInvoiceRecyclerView.setAdapter(mSalesInvoiceAdapter);
    }

    @Override
    public void showEmptyInvoiceImage() {
        mEmptyInvoice.setVisibility(View.VISIBLE);
    }

    @Override
    public void callAdapter(List<CustomerSalesInvoice> mCustomerStatusInvoicesList) {
        setSalesInvoiceAdapter(mCustomerStatusInvoicesList);
    }

    private void setCurrentDateToView() {
        mCurrentDateView.setText(mCurrentDate);
    }

    private void initLayoutComponent() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mSalesInvoiceRecyclerView.setLayoutManager(layoutManager);
        mSalesInvoiceRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void setToolBarTitle() {
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_sales_invoice);
        }
    }

    @OnClick(R.id.img_filter)
    public void onFilterIconClick() {
        filterLayout.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.filter_close_icon)
    public void closeFilterLayout() {
        filterLayout.setVisibility(View.GONE);
        edFilterInvoice.setText("");
        mSalesInvoiceAdapter.notifyDataSetChanged();
        hideKeyboard();
    }

    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(
                Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(filterLayout.getWindowToken(), 0);
    }

    private void getCustomerList() {
        if (mRealmInstance.isClosed()) {
            mRealmInstance = Realm.getDefaultInstance();
        }
        RealmResults<CustomerSalesInvoice> realmResults = mRealmInstance.where(CustomerSalesInvoice.class).findAll();
        mCustomerStatusInvoicesList.clear();
        mCustomerStatusInvoicesList.addAll(realmResults);
        mSalesInvoiceAdapter.notifyDataSetChanged();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void redirectToLogin() {

    }

    @Override
    public DashboardComponent getComponent() {
        return mDashboardComponent;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}

package com.mpose.com.mpose.dashboard.presenter;

import android.content.Context;
import android.widget.Toast;

import com.mpose.com.mpose.common.Presenter;
import com.mpose.com.mpose.dashboard.actionlistener.IGridViewItemActionListener;
import com.mpose.com.mpose.pos.models.AllProducts;
import com.mpose.com.mpose.dashboard.view.GridViewItemView;
import com.mpose.com.mpose.manager.SharedPreferenceManager;
import com.mpose.com.mpose.retrofit.ApiClient;
import com.mpose.com.mpose.retrofit.ApiInterface;
import com.mpose.com.mpose.utils.AppConstants;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lp-ritesh on 5/7/17.
 */

public class GridViewItemPresenter extends Presenter implements
        IGridViewItemActionListener<GridViewItemView> {


    private Context mContext;
    private GridViewItemView mGridViewItemView;

    @Inject
    public GridViewItemPresenter(Context mContext) {
        this.mContext = mContext;
    }


    @Override
    public void setView(GridViewItemView gridViewItemView) {
        this.mGridViewItemView = gridViewItemView;
    }

    @Override
    public void getAllProducts() {
        String sid = SharedPreferenceManager.getSharedPreferenceManager(mContext).getSID();

        ApiInterface apiInterface = ApiClient.getClient(mContext).create(ApiInterface.class);
        Call<AllProducts> allProductsCall = apiInterface.getAllProducts(sid, "2017-07-03 11:49:29", "0", "2");

        allProductsCall.enqueue(new Callback<AllProducts>() {
            @Override
            public void onResponse(Call<AllProducts> call, Response<AllProducts> response) {
                if (response.isSuccessful()) {

                    String status = response.body().getMessage().getStatus();
                    if (AppConstants.PRODUCT_SUCESS.equals(status)) {
                        mGridViewItemView.onProductSucessResponse(response.body());
                    }

                    Toast.makeText(mContext, "res- " + response.body().getMessage().getStatus(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AllProducts> call, Throwable t) {


            }
        });


    }
}

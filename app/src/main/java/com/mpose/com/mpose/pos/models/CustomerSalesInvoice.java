package com.mpose.com.mpose.pos.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lp-ritesh on 3/7/17.
 */

public class CustomerSalesInvoice extends RealmObject {

    @PrimaryKey
    @SerializedName("invoiceNumber")
    private Integer invoiceNumber;

    @SerializedName("customerName")
    private String customerName;

    private RealmList<ProductLite> invoiceProductsList = new RealmList<>();

    @SerializedName("invoiceDate")
    private String invoiceDate;

    @SerializedName("grandTotal")
    private double grandTotal;

    @SerializedName("customer_status")
    private String status;

    @SerializedName("customer_id")
    private Integer customerId;

    @SerializedName("additional_discount_percentage")
    private Double additionalDiscountPercentage;

    @SerializedName("return_against")
    private String returnAgainstInvoiceNumber;

    @SerializedName("return_against_invoice_int")
    private Integer returnAgainstInvoiceNumberInt;

    @SerializedName("is_return")
    private Integer isReturn;

    @SerializedName("isInvoiceSync")
    private Boolean isInvoiceSync = false;

    @SerializedName("isPaymentInitialte")
    private Boolean isPaymentInitialte = false;

    @SerializedName("device_id")
    private String deviceID;

    @SerializedName("current_timestamp")
    private String currentTimeStamp;

    @SerializedName("paymentStatus")
    private Boolean paymentStatus = false;

    @SerializedName("outStandingAmount")
    private Double outStandingAmount;

    @SerializedName("amount_paid_by_user")
    private Double amountPaidByUser;

    @SerializedName("erp_invoice_id")
    private String erpInvoiceId;

    public String getErpInvoiceId() {
        return erpInvoiceId;
    }

    public void setErpInvoiceId(String erpInvoiceId) {
        this.erpInvoiceId = erpInvoiceId;
    }

    public Double getAmountPaidByUser() {
        return amountPaidByUser;
    }

    public void setAmountPaidByUser(Double amountPaidByUser) {
        this.amountPaidByUser = amountPaidByUser;
    }

    public Double getOutStandingAmount() {
        return outStandingAmount;
    }

    public void setOutStandingAmount(Double outStandingAmount) {
        this.outStandingAmount = outStandingAmount;
    }

    public Boolean getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(Boolean paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Boolean getPaymentInitialte() {
        return isPaymentInitialte;
    }

    public void setPaymentInitialte(Boolean paymentInitialte) {
        isPaymentInitialte = paymentInitialte;
    }

    public String getCurrentTimeStamp() {
        return currentTimeStamp;
    }

    public void setCurrentTimeStamp(String currentTimeStamp) {
        this.currentTimeStamp = currentTimeStamp;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public Integer getIsReturn() {
        return isReturn;
    }

    public void setIsReturn(Integer isReturn) {
        this.isReturn = isReturn;
    }

    public Boolean getInvoiceSync() {
        return isInvoiceSync;
    }

    public void setInvoiceSync(Boolean invoiceSync) {
        isInvoiceSync = invoiceSync;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public void addInvoiceProduct(ProductLite product) {
        invoiceProductsList.add(product);
    }

    public Integer getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(Integer invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public RealmList<ProductLite> getInvoiceProductsList() {
        return invoiceProductsList;
    }

    public void setInvoiceProductsList(RealmList<ProductLite> invoiceProductsList) {
        this.invoiceProductsList = invoiceProductsList;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getReturnAgainstInvoiceNumber() {
        return returnAgainstInvoiceNumber;
    }

    public void setReturnAgainstInvoiceNumber(String returnAgainstInvoiceNumber) {
        this.returnAgainstInvoiceNumber = returnAgainstInvoiceNumber;
    }

    public Integer isReturn() {
        return isReturn;
    }

    public void setReturn(Integer aReturn) {
        this.isReturn = aReturn;
    }

    public String getStringInvoiceID() {
        return deviceID + "-" + currentTimeStamp + "-" + invoiceNumber;
    }

    public String getStringCustomerID() {
        return currentTimeStamp + "-" + customerId;
    }

    public String getReturnAgainstStringInvoiceID() {
        return deviceID + "-" + currentTimeStamp + "-" + invoiceNumber;
    }

    public Double getAdditionalDiscountPercentage() {
        return additionalDiscountPercentage;
    }

    public void setAdditionalDiscountPercentage(Double additionalDiscountPercentage) {
        this.additionalDiscountPercentage = additionalDiscountPercentage;
    }

    public Integer getReturnAgainstInvoiceNumberInt() {
        return returnAgainstInvoiceNumberInt;
    }

    public void setReturnAgainstInvoiceNumberInt(Integer returnAgainstInvoiceNumberInt) {
        this.returnAgainstInvoiceNumberInt = returnAgainstInvoiceNumberInt;
    }

    @Override
    public String toString() {
        return "CustomerSalesInvoice{" +
                "invoiceNumber=" + invoiceNumber +
                ", customerName='" + customerName + '\'' +
                ", invoiceProductsList=" + invoiceProductsList +
                ", invoiceDate='" + invoiceDate + '\'' +
                ", grandTotal=" + grandTotal +
                ", status='" + status + '\'' +
                ", customerId=" + customerId +
                ", additionalDiscountPercentage=" + additionalDiscountPercentage +
                ", returnAgainstInvoiceNumber=" + returnAgainstInvoiceNumber +
                ", isReturn=" + isReturn +
                ", isInvoiceSync=" + isInvoiceSync +
                '}';
    }
}

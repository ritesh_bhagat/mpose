package com.mpose.com.mpose.di;

import com.mpose.com.mpose.di.component.ActivityComponent;

/**
 * Created by lp-ritesh on 23/6/17.
 */

public interface HasComponent<C> {
    C getComponent();
}

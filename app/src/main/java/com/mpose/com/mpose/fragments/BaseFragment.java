package com.mpose.com.mpose.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.mpose.com.mpose.activity.LoginActivity;
import com.mpose.com.mpose.di.HasComponent;
import com.mpose.com.mpose.manager.SharedPreferenceManager;

/**
 * Created by lp-ritesh on 1/7/17.
 */

public class BaseFragment extends Fragment {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }

}

package com.mpose.com.mpose.expense.models.expenserequest;

import com.google.gson.annotations.SerializedName;

public class ExpenseRequestDetails {

    @SerializedName("expense_type")
    private String expenseType;
    @SerializedName("amount")
    private Double amount;
    @SerializedName("description")
    private String description;

    public String getExpenseType() {
        return expenseType;
    }

    public void setExpenseType(String expenseType) {
        this.expenseType = expenseType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
package com.mpose.com.mpose.eventbus;

/**
 * Created by lp-ritesh on 6/7/17.
 */

public class NavigationDrawerEvent {
    private String mNavMenu;

    public NavigationDrawerEvent(String menu) {
        this.mNavMenu = menu;
    }


    public String getMenu() {
        return mNavMenu;
    }

    public void setMenu(String menu) {
        this.mNavMenu = menu;
    }

}

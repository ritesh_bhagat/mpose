package com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkapi;

import com.google.gson.annotations.SerializedName;

public class SalesInvoicePayment {

    @SerializedName("mode_of_payment")
    private String modeOfPayment;
    @SerializedName("amount")
    private Double amount;

    public String getModeOfPayment() {
        return modeOfPayment;
    }

    public void setModeOfPayment(String modeOfPayment) {
        this.modeOfPayment = modeOfPayment;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

}
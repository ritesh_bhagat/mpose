package com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkapi;

import com.google.gson.annotations.SerializedName;

public class SalesInvoiceTaxis {

    @SerializedName("charge_type")
    private String chargeType;
    @SerializedName("rate")
    private Integer rate;
    @SerializedName("account_head")
    private String accountHead;
    @SerializedName("description")
    private String description;

    public String getChargeType() {
        return chargeType;
    }

    public void setChargeType(String chargeType) {
        this.chargeType = chargeType;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public String getAccountHead() {
        return accountHead;
    }

    public void setAccountHead(String accountHead) {
        this.accountHead = accountHead;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
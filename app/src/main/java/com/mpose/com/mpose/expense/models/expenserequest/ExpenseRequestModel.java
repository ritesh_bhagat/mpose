package com.mpose.com.mpose.expense.models.expenserequest;

import com.google.gson.annotations.SerializedName;


public class ExpenseRequestModel {

    @SerializedName("data")
    private ExpenseData data;

    public ExpenseData getData() {
        return data;
    }

    public void setData(ExpenseData data) {
        this.data = data;
    }

}
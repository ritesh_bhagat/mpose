package com.mpose.com.mpose.salesinvoice.models.salesreturn;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lp-ritesh on 26/7/17.
 */
public class SalesReturnModel {

    @SerializedName("data")
    private List<SalesReturnData> data = null;

    @SerializedName("data")
    public List<SalesReturnData> getData() {
        return data;
    }

    public void setData(List<SalesReturnData> data) {
        this.data = data;
    }

}
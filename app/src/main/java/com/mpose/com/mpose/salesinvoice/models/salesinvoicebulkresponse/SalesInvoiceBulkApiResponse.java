package com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkresponse;


import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;


public class SalesInvoiceBulkApiResponse extends RealmObject{

    @SerializedName("message")
    private SalesInvoiceResponseMessage message;

    public SalesInvoiceResponseMessage getMessage() {
        return message;
    }

    public void setMessage(SalesInvoiceResponseMessage message) {
        this.message = message;
    }

}

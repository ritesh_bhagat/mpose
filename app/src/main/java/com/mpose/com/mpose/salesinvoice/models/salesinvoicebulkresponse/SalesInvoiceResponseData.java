package com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkresponse;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class SalesInvoiceResponseData extends RealmObject{

    @SerializedName("status")
    private String status;
    @SerializedName("invoice_id")
    private String invoiceId;
    @SerializedName("erp_invoice_id")
    private String erpInvoiceId;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getErpInvoiceId() {
        return erpInvoiceId;
    }

    public void setErpInvoiceId(String erpInvoiceId) {
        this.erpInvoiceId = erpInvoiceId;
    }

}
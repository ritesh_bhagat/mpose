package com.mpose.com.mpose.auth.view;

import com.mpose.com.mpose.auth.model.login.DomainModel;
import com.mpose.com.mpose.auth.model.login.Login;
import com.mpose.com.mpose.common.BaseView;
import com.mpose.com.mpose.dashboard.model.possetting.POSSettingModel;

/**
 * Created by lp-ritesh on 5/7/17.
 */

public interface LoginView extends BaseView {

    void onLoginSucess(Login body);

    void onLoginFailed();

    void redirect(boolean b);

    void onDomainPingCallFailure();

    void onForgotPaswordSucess(String message);

    void onForgotPasswordFailure(String message);

    void showNetworkConnectionError();

    void sendUserDataToHome(String fullName);

    void callLoginApiView();

    void setDomainToView(String domainName);

    void domainSucessForgotPassword(String strDomain);

    void onDomainPingCallNetworkFailure();

    void onPosSettingSuccessResponse(POSSettingModel body);

    void somethingWentWrong();

    void clearLocalPreferences();

    void showToastMessage();
}

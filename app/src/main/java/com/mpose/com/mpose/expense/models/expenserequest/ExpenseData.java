package com.mpose.com.mpose.expense.models.expenserequest;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ExpenseData {

    @SerializedName("posting_date")
    private String postingDate;

    @SerializedName("expenses")
    private List<ExpenseRequestDetails> expenses;

    @SerializedName("expense_id")
    private String expenseId;





    public String getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(String postingDate) {
        this.postingDate = postingDate;
    }

    public List<ExpenseRequestDetails> getExpenses() {
        return expenses;
    }

    public void setExpenses(List<ExpenseRequestDetails> expenses) {
        this.expenses = expenses;
    }

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

}
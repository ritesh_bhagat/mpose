package com.mpose.com.mpose.eventbus;

/**
 * Created by lp-ritesh on 31/7/17.
 */

public class GenerateSalesInvoice {

    private double mGrandTotal;
    private double mDiscountValue;
    private double mOutstandingValue;
    private double mAmountPaidByUser;


    public GenerateSalesInvoice(double grandTotal, double discountValue, double outStandingValue, double amountPaidByUser) {
        this.mGrandTotal = grandTotal;
        this.mDiscountValue = discountValue;
        this.mOutstandingValue = outStandingValue;
        this.mAmountPaidByUser = amountPaidByUser;
    }

    public double getmAmountPaidByUser() {
        return mAmountPaidByUser;
    }

    public void setmAmountPaidByUser(double mAmountPaidByUser) {
        this.mAmountPaidByUser = mAmountPaidByUser;
    }

    public double getmOutstandingValue() {
        return mOutstandingValue;
    }

    public void setmOutstandingValue(double mOutstandingValue) {
        this.mOutstandingValue = mOutstandingValue;
    }

    public double getmDiscountValue() {
        return mDiscountValue;
    }

    public void setmDiscountValue(double mDiscountValue) {
        this.mDiscountValue = mDiscountValue;
    }

    public double getmGrandTotal() {
        return mGrandTotal;
    }

    public void setmGrandTotal(double mGrandTotal) {
        this.mGrandTotal = mGrandTotal;
    }
}

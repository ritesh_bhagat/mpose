package com.mpose.com.mpose.auth.model.login;

/**
 * Created by lp-ritesh on 18/7/17.
 */

import com.google.gson.annotations.SerializedName;


public class ForgotPassword {

    @SerializedName("email")
    private String email;

    @SerializedName("message")
    private LoginMessage message;

    public LoginMessage getMessage() {
        return message;
    }

    public void setMessage(LoginMessage message) {
        this.message = message;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

package com.mpose.com.mpose.eventbus;

/**
 * Created by abc on 8/21/2017.
 */

public class ShowCustomeToolbarEvent {

    private boolean mIsVisible;

    public ShowCustomeToolbarEvent(boolean isVisible) {
        this.mIsVisible = isVisible;
    }

    public boolean isVisible() {
        return mIsVisible;
    }
}

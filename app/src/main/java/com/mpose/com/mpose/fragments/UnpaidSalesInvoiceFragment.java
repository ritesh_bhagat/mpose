package com.mpose.com.mpose.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mpose.com.mpose.R;
import com.mpose.com.mpose.adapters.UnpaidSalesInvoiceAdapter;
import com.mpose.com.mpose.di.HasComponent;
import com.mpose.com.mpose.di.component.DashboardComponent;
import com.mpose.com.mpose.eventbus.ShowCustomeToolbarEvent;
import com.mpose.com.mpose.eventbus.UpdateSIPaymentEntryStatus;
import com.mpose.com.mpose.salesinvoice.actionlistener.IUnpaidSalesInvoiceActionListener;
import com.mpose.com.mpose.salesinvoice.models.unpaid.UnpaidSalesInvoice;
import com.mpose.com.mpose.salesinvoice.view.UnpaidSalesInvoiceView;
import com.mpose.com.mpose.utils.CommonAlertDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Anjali on 27-09-2017.
 */

public class UnpaidSalesInvoiceFragment extends BaseFragment implements UnpaidSalesInvoiceView,
        HasComponent<DashboardComponent> {

    @Bind(R.id.unpaid_invoice_recycler_view)
    RecyclerView mRecyclerView;

    @Bind(R.id.empty_invoices)
    ImageView mEmptyInvoicesIv;

    @Bind(R.id.img_filter)
    ImageView mFilterImage;

    @Bind(R.id.filter_sales_invoice)
    EditText mFilterInputText;

    @Bind(R.id.filter_close_icon)
    ImageView mFilterCloseIcon;

    @Bind(R.id.filter_invoices_layout)
    RelativeLayout filterLayout;

    @Inject
    IUnpaidSalesInvoiceActionListener mUnpaidSalesInvoiceActionListener;

    private DashboardComponent mDashboardComponent;
    private UnpaidSalesInvoiceAdapter mUnpaidSalesInvoiceAdapter;
    private List<UnpaidSalesInvoice> mUnpaidSalesInvoiceList = new ArrayList<>();
    List<UnpaidSalesInvoice> filteredList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_unpaid_sales_invoice, container, false);
        ButterKnife.bind(this, rootView);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.getComponent(DashboardComponent.class).inject(this);
        mUnpaidSalesInvoiceActionListener.setView(this);
        mUnpaidSalesInvoiceAdapter = new UnpaidSalesInvoiceAdapter(getActivity(), mUnpaidSalesInvoiceList);
        mRecyclerView.setAdapter(mUnpaidSalesInvoiceAdapter);

        mFilterInputText.addTextChangedListener(forFilterInvoices);

        if (isAdded()) {
            EventBus.getDefault().post(new ShowCustomeToolbarEvent(false));
        }
    }

    TextWatcher forFilterInvoices = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                String input = s.toString();
                filteredList = new ArrayList<>();
                for (int i = 0; i < mUnpaidSalesInvoiceList.size(); i++) {
                    if (mUnpaidSalesInvoiceList.get(i).getCustomer().trim().toLowerCase().contains(input.toLowerCase().trim())) {
                        filteredList.add(mUnpaidSalesInvoiceList.get(i));
                    } else if (mUnpaidSalesInvoiceList.get(i).getErpInvoiceId().trim().toLowerCase().contains(input.toLowerCase().trim())) {
                        filteredList.add(mUnpaidSalesInvoiceList.get(i));
                    } else if (mUnpaidSalesInvoiceList.get(i).getPosId().trim().toLowerCase().contains(input.toLowerCase().trim())) {
                        filteredList.add(mUnpaidSalesInvoiceList.get(i));
                    } else if (mUnpaidSalesInvoiceList.get(i).getStatus().trim().toLowerCase().contains(input.toLowerCase().trim())) {
                        filteredList.add(mUnpaidSalesInvoiceList.get(i));
                    }
                }
                setNewList(filteredList);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void setNewList(List<UnpaidSalesInvoice> filteredList) {
        mUnpaidSalesInvoiceAdapter.setFilteredList(filteredList);
        mUnpaidSalesInvoiceAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.img_filter)
    public void onFilterIconClick() {
        filterLayout.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.filter_close_icon)
    public void closeFilterLayout() {
        filterLayout.setVisibility(View.GONE);
        mFilterInputText.setText("");
        mUnpaidSalesInvoiceAdapter.notifyDataSetChanged();
        hideKeyboard();
    }

    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(
                Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(filterLayout.getWindowToken(), 0);
    }


    @Override
    public void onStart() {
        super.onStart();
        setActionBarTitle();
        initLayoutComponent();
        mUnpaidSalesInvoiceActionListener.getUnPaidSalesInvoiceList();
    }

    private void setActionBarTitle() {
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_unpaid_sales_invoice);
        }
    }

    private void initLayoutComponent() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mEmptyInvoicesIv.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void redirectToLogin() {

    }

    @Override
    public DashboardComponent getComponent() {
        return mDashboardComponent;
    }

    @Override
    public void showNetworkConnectionError() {
        if (getActivity() != null) {
            CommonAlertDialog.ShowAlertDialog(getActivity(), getResources().getString(R.string.internet_connection_title),
                    getResources().getString(R.string.internet_connection_message), R.style.dialog_animation_fade);
        }
    }

    @Override
    public void showUnpaidSalesInvoiceList(List<UnpaidSalesInvoice> salesInvoices) {
        if (getActivity() != null && salesInvoices.size() > 0) {
            mUnpaidSalesInvoiceList = salesInvoices;
            mEmptyInvoicesIv.setVisibility(View.GONE);
            setNewList(mUnpaidSalesInvoiceList);
            mUnpaidSalesInvoiceAdapter.notifyDataSetChanged();
        } else {
            mEmptyInvoicesIv.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handlePaymentEntryEvent(UpdateSIPaymentEntryStatus updateSIPaymentEntryStatus) {
        mUnpaidSalesInvoiceActionListener.getUnPaidSalesInvoiceList();
    }
}

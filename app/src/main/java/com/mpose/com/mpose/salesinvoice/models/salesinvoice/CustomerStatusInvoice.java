package com.mpose.com.mpose.salesinvoice.models.salesinvoice;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lp-ritesh on 20/7/17.
 */

public class CustomerStatusInvoice extends RealmObject{


    @SerializedName("customer_id")
    private String customerId;

    @PrimaryKey
    @SerializedName("customer_name")
    private String customerName;

    @SerializedName("customer_status")
    private String status;

    @SerializedName("grand_total")
    private String grandTotal;

    @SerializedName("customer_invoice_number")
    private RealmList<InvoiceNumberModel> invoiceNumber;


    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public RealmList<InvoiceNumberModel> getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(RealmList<InvoiceNumberModel> invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }
}

package com.mpose.com.mpose.expense.views;

import com.mpose.com.mpose.common.BaseView;
import com.mpose.com.mpose.expense.models.addexpense.CustomerExpenseModel;

import io.realm.RealmList;

/**
 * Created by lp-ritesh on 21/8/17.
 */

public interface ExpenseClaimView extends BaseView {
    void callAdapter(RealmList<CustomerExpenseModel> mRealmList);
}

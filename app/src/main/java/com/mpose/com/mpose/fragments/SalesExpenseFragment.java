package com.mpose.com.mpose.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mpose.com.mpose.R;

import butterknife.ButterKnife;

/**
 * Created by abc on 8/18/2017.
 */

public class SalesExpenseFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_customer_invoice_list, container, false);
       // ButterKnife.bind(this, rootView);
        return rootView;
    }
}

package com.mpose.com.mpose.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mpose.com.mpose.R;
import com.mpose.com.mpose.pos.models.FilterProducts;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by lp-ritesh on 28/12/17.
 */

public class FilterProductsAdapter extends RecyclerView.Adapter<FilterProductsAdapter.ViewHolder> {
    private static final String TAG = FilterProductsAdapter.class.getSimpleName();

    private Context mContext;
    private RealmList<FilterProducts> mProductRealmList;
    private Realm mRealm;

    public FilterProductsAdapter(Context context, RealmList<FilterProducts> list) {
        this.mContext = context;
        this.mProductRealmList = list;
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    public FilterProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.filter_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FilterProductsAdapter.ViewHolder holder, int position) {
        final FilterProducts product = mProductRealmList.get(position);
        holder.mGroupName.setText(product.getGroupName());
        holder.mCheckBox.setOnCheckedChangeListener(null);
        holder.mCheckBox.setChecked(product.isSelected());
        holder.mLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!holder.mCheckBox.isChecked()) {
                    holder.mCheckBox.setChecked(true);
                    mProductRealmList.add(product);
                } else {
                    holder.mCheckBox.setChecked(false);
                    mProductRealmList.remove(product);
                }
            }
        });

//        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    product.setSelected(true);
//                } else {
//                    product.setSelected(false);
//                }
//            }
//        });
//        holder.mCheckBox.setChecked(product.isSelected());
//        holder.mCheckBox.setChecked(mProductRealmList.get(position).isSelected());

        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, final boolean isChecked) {
                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        FilterProducts filterProduct = mProductRealmList.get(holder.getAdapterPosition());
                        filterProduct.setSelected(isChecked);
                        realm.insertOrUpdate(filterProduct);
                    }
                });
            }
        });
    }

    public RealmList<FilterProducts> getFilterList() {
        if (mProductRealmList != null) {
            return mProductRealmList;
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return mProductRealmList.size();
    }

    public void setNewList(RealmList<FilterProducts> productRealmList) {
        this.mProductRealmList = productRealmList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.chkEnable)
        CheckBox mCheckBox;

        @Bind(R.id.tvTitle)
        TextView mGroupName;

        @Bind(R.id.filter_layout)
        LinearLayout mLinearLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

package com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkapi;

/**
 * Created by lp-ritesh on 26/7/17.
 */

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class SalesInvoiceBulkApiModel {

    @SerializedName("data")
    private List<SalesRequestData> data;

    public List<SalesRequestData> getData() {
        return data;
    }

    public void setData(List<SalesRequestData> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return data.toString();
    }


}


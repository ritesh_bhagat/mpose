package com.mpose.com.mpose.home.actionlistener;

import com.mpose.com.mpose.pos.models.Customer;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkapi.SalesRequestData;
import com.mpose.com.mpose.sync.model.bulkpaymentrequest.BulkPaymentData;

import java.util.List;

/**
 * Created by lp-ritesh on 4/8/17.
 */

public interface IHomeActionListener<T> {

    void setView(T t);

    List<Customer> readCustomerFromDB();


    void logOut();

    void setSelectedCustomerIdToDB(String name);

    void saveCustomerIntoDB(String name);

    String getUserName();

    boolean checkIfUserAvailable(String name);

    void populatePaymentBulkRequestData();

    boolean checkIfCustomerExist(String name);

    void handleCustomerCreate(String customerName);

    void populateSalesInvoiceCreationData();

    void callSalesInvoiceBulkApi(List<SalesRequestData> salesRequestDataList);

    void callCreateSalesInvoiceBulkApi(List<SalesRequestData> salesRequestDataList);

    void populateExpenseData();

    void checkSalesInvoicesStatus();

    void populateUnpaidSalesInvoiceData(BulkPaymentData bulkPaymentData);
}

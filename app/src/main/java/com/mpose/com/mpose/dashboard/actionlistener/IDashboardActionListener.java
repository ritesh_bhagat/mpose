package com.mpose.com.mpose.dashboard.actionlistener;

/**
 * Created by lp-ritesh on 5/7/17.
 */

public interface IDashboardActionListener<T> {

    void setView(T t);

    void getDashBoardChartData(String startDate, String endDate);

    void createCustomerDB();

    void resume();

}

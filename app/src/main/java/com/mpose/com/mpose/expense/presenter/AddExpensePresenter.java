package com.mpose.com.mpose.expense.presenter;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.mpose.com.mpose.auth.model.login.Login;
import com.mpose.com.mpose.common.Presenter;
import com.mpose.com.mpose.dashboard.model.possetting.POSSettingData;
import com.mpose.com.mpose.expense.actionlistener.AddExpenseActionListener;
import com.mpose.com.mpose.expense.models.addexpense.CustomerExpenseModel;
import com.mpose.com.mpose.expense.views.AddExpenseView;
import com.mpose.com.mpose.utils.AppConstants;
import com.mpose.com.mpose.utils.DateTimeUtil;
import com.mpose.com.mpose.utils.ExpenseIdUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by lp-ritesh on 19/8/17.
 */

public class AddExpensePresenter extends Presenter implements AddExpenseActionListener<AddExpenseView> {

    private static final String TAG = AddExpensePresenter.class.getSimpleName();
    private Context mContext;
    private AddExpenseView mAddExpenseView;
    private Realm mRealm;

    @Inject
    public AddExpensePresenter(Context mContext) {
        this.mContext = mContext;
        this.mRealm = Realm.getDefaultInstance();
    }

    @Override
    public void setView(AddExpenseView addExpenseView) {
        this.mAddExpenseView = addExpenseView;

    }

    @Override
    public String getLoggedInUserName() {
        RealmQuery<Login> query = mRealm.where(Login.class);
        Login login = query.findFirst();
        if (login != null) {
            return login.getFullName();
        }
        return null;
    }

    @Override
    public void getAllExpenseTypes() {
        List<String> stringList = new ArrayList<>();
        RealmResults<POSSettingData> results = mRealm.where(POSSettingData.class).findAll();
        for (int i = 0; i < results.get(0).getExpenseType().size(); i++) {
            if (results.get(0).getExpenseType().size() == i) {
                return;
            } else {
                stringList.add(results.get(0).getExpenseType().get(i).getStringObject());
            }
        }
        mAddExpenseView.setValuesToSpinner(stringList);

    }

    @Override
    public void saveExpenseDetialsToDB() {

        final String mExpenseAddedDate = DateTimeUtil.currentDate(AppConstants.DATE_TIME_FORMAT);

        final String androidId = Settings.Secure.getString(mContext.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        final String description = mAddExpenseView.getDescription();
        final String doe = mAddExpenseView.getDateOfExpense();
        final String fromEmp = mAddExpenseView.getFromEmployee();
        final String expenseType = mAddExpenseView.getExpenseType();
        final double claimAmount = mAddExpenseView.getClaimAmount();
        final int expenseId = ExpenseIdUtils.getNewExpenseId();
        final String currentTimeStamp = DateTimeUtil.getCurrentTimeStamp();


        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                CustomerExpenseModel addExpenseModel = realm.createObject(CustomerExpenseModel.class, String.valueOf(expenseId));
                addExpenseModel.setClaimAmount(claimAmount);
                addExpenseModel.setClaimDescription(description);
                addExpenseModel.setExpenseType(expenseType);
                addExpenseModel.setPostingDate(doe);
                addExpenseModel.setFromEmployee(fromEmp);
                addExpenseModel.setCurrentTimeStamp(currentTimeStamp);
                addExpenseModel.setDeviceID(androidId);
                addExpenseModel.setExpenseTimeStamp(mExpenseAddedDate);
                addExpenseModel.setExpenseIdSync(false);

            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {

                RealmResults<CustomerExpenseModel> results = mRealm.where(CustomerExpenseModel.class).findAll();
                Log.d(TAG, "onSuccess: " + results.size());

                for (CustomerExpenseModel model : results) {
                    Log.d(TAG, "onSuccess: " + model.getExpenseType() + " " + model.getStringExpenseID() + " " + model.getClaimAmount());
                }
                mAddExpenseView.onExpenseCreationSuceess();
                Intent intent = new Intent("AUTO_SYNC_ACTION");
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {

                Log.e(TAG, "onError: ", error);
            }
        });
    }
}

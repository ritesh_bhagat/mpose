package com.mpose.com.mpose.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mpose.com.mpose.R;
import com.mpose.com.mpose.adapters.ExpenseClaimAdapter;
import com.mpose.com.mpose.di.HasComponent;
import com.mpose.com.mpose.di.component.DashboardComponent;
import com.mpose.com.mpose.eventbus.ShowCustomeToolbarEvent;
import com.mpose.com.mpose.expense.actionlistener.ExpenseClaimActionListener;
import com.mpose.com.mpose.expense.models.addexpense.CustomerExpenseModel;
import com.mpose.com.mpose.expense.views.ExpenseClaimView;

import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by abc on 8/18/2017.
 */

public class ExpenseClaimFragment extends BaseFragment implements ExpenseClaimView,
        HasComponent<DashboardComponent> {

    private static final String TAG = ExpenseClaimFragment.class.getSimpleName();

    @Bind(R.id.expense_recycler_view)
    RecyclerView mExpenseRecyclerView;

    @Bind(R.id.txt_total_claimed_amnt_value)
    TextView mTotalClaimedAmount;

    @Inject
    ExpenseClaimActionListener mExpenseClaimActionListener;

    private DashboardComponent mDashboardComponent;
    private Realm mRealmInstance;
    private ExpenseClaimAdapter mExpenseClaimAdapter;
    private RealmList<CustomerExpenseModel> mResultList = new RealmList<>();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRealmInstance = Realm.getDefaultInstance();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_expense_claim, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.getComponent(DashboardComponent.class).inject(this);

        mExpenseClaimActionListener.setView(this);

        setToolBarTitle();
        initLayoutComponent();
        fetchAllSortedExpenseClaimListFromDB();

        setTotalClaimedAmount(mExpenseClaimActionListener.getTotalClaimAmount());

        initAdapter();
        setActionBarTitle();

        if (isAdded()) {
            EventBus.getDefault().post(new ShowCustomeToolbarEvent(false));
        }
    }

    private void fetchAllSortedExpenseClaimListFromDB() {
        mExpenseClaimActionListener.getAllExpenseClaimList();
        mExpenseClaimActionListener.fetchSortedList();
    }

    @Override
    public void callAdapter(RealmList<CustomerExpenseModel> mRealmList) {
        setExpenseClaimAdapter(mRealmList);
    }

    private void setExpenseClaimAdapter(RealmList<CustomerExpenseModel> mRealmList) {
        this.mResultList.addAll(mRealmList);
        mExpenseClaimAdapter = new ExpenseClaimAdapter(getActivity(), mResultList);
        mExpenseRecyclerView.setAdapter(mExpenseClaimAdapter);
    }

    private void setActionBarTitle() {
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.expense_claim_title));
        }
    }

    private void initAdapter() {

    }

    private void notifyExpenseClaimAdapter() {
        mExpenseClaimAdapter.notifyDataSetChanged();
    }

    private void initLayoutComponent() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mExpenseRecyclerView.setLayoutManager(layoutManager);

        mExpenseRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void setToolBarTitle() {
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_sales_invoice);
        }
    }

    public void setTotalClaimedAmount(double amount) {
        mTotalClaimedAmount.setText(getResources().getString(R.string.rs) + " " +
                new DecimalFormat("#.##").format(amount));
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void redirectToLogin() {

    }

    @Override
    public DashboardComponent getComponent() {
        return mDashboardComponent;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}

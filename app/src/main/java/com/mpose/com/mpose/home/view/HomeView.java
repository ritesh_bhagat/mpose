package com.mpose.com.mpose.home.view;

import com.mpose.com.mpose.common.BaseView;
import com.mpose.com.mpose.pos.models.Customer;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkapi.SalesRequestData;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkresponse.SalesInvoiceBulkApiResponse;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkresponse.SalesInvoiceResponseData;
import com.mpose.com.mpose.sync.model.bulkpaymentrequest.BulkPaymentData;

import java.util.List;

import io.realm.RealmList;

/**
 * Created by lp-ritesh on 3/8/17.
 */

public interface HomeView extends BaseView {

    void onSuccessLogout();

    void onSuccessSaveCustomerIdToDB(String name);

    void notifyToAdapter();

    void apiCallBulkPayment(List<BulkPaymentData> bulkPaymentDataList);

    void setCustomerToDB(String name);

    void showCustomerAlreadyPresentError();

//    void setCustomerDataToView(String customerName);

    void callCreateSalesInvoiceApi(List<SalesRequestData> salesRequestDataList);

    void changeStatusOfSyncInvoices(RealmList<SalesInvoiceResponseData> body);

    void callCreateSalesReturnInvoiceApi(List<SalesRequestData> salesRequestDataList);

    void hideSyncDialog();

    void showSucessMessage();

    void showSyncErrorMessage();

    void showSyncDialog();


    void showOnFailureAlert();

    void showAlertDialogBeforeLogout(boolean flag);

    void showSessionExpiryDialog();
}

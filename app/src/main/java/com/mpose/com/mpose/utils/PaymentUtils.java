package com.mpose.com.mpose.utils;

import com.mpose.com.mpose.sync.model.bulkpaymentrequest.BulkPaymentData;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by lp-ritesh on 9/8/17.
 */

public class PaymentUtils {

    private static int PAYMENT_ID_START = 300001;

    public static int getNewPaymentId() {
        Realm mRealm = Realm.getDefaultInstance();
        RealmResults<BulkPaymentData> resultList = mRealm.where(BulkPaymentData.class)
                .findAllSorted(AppConstants.PAYMENT_ID, Sort.DESCENDING);
        if (resultList.size() <= 0) {
            return PAYMENT_ID_START;
        }

        BulkPaymentData payment = resultList.first();
        if (payment == null) {
            return PAYMENT_ID_START;
        } else {
            int paymentId = -1;
            try {
                paymentId = Integer.parseInt(payment.getPaymentId());
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
            }
            return paymentId + 1;
        }
    }


}

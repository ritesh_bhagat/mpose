package com.mpose.com.mpose.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.mpose.com.mpose.R;
import com.mpose.com.mpose.activity.WebViewActivity;
import com.mpose.com.mpose.adapters.CustomerSalesInvoiceAdapter;
import com.mpose.com.mpose.dashboard.model.companydetails.Data;
import com.mpose.com.mpose.di.HasComponent;
import com.mpose.com.mpose.di.component.DashboardComponent;
import com.mpose.com.mpose.eventbus.ShowCustomeToolbarEvent;
import com.mpose.com.mpose.pos.models.CustomerSalesInvoice;
import com.mpose.com.mpose.pos.models.ProductLite;
import com.mpose.com.mpose.salesinvoice.actionlistener.ICustomerSalesInvoiceActionListener;
import com.mpose.com.mpose.salesinvoice.view.CustomerSalesInvoiceView;
import com.mpose.com.mpose.utils.AppConstants;
import com.mpose.com.mpose.utils.CommonAlertDialog;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;

/**
 * Created by lp-ritesh on 6/7/17.
 */

public class CustomerSalesInvoiceFragment extends BaseFragment implements CustomerSalesInvoiceView,
        HasComponent<DashboardComponent>, CustomerSalesInvoiceAdapter.ItemCountCallback,
        CompoundButton.OnCheckedChangeListener, AdapterView.OnItemSelectedListener {

    private static final String TAG = CustomerSalesInvoiceFragment.class.getSimpleName();

    @Bind(R.id.txt_customer_id)
    TextView mCustomerId;

    @Bind(R.id.sp_exapnse_type)
    Spinner mSpinnerInvoiceNumber;

    @Bind(R.id.check_box_item)
    CheckBox mCheckBox;

    @Bind(R.id.txt_total_value)
    TextView mTotalValue;

    @Bind(R.id.txt_rate_value)
    TextView mRateValue;

    @Bind(R.id.txt_quantity_value)
    TextView mQuantityValue;

    @Bind(R.id.sales_invoice_recycler_view)
    RecyclerView mSalesRecyclerView;

    @Bind(R.id.button_credit_note)
    TextView mCreditNote;

    @Bind(R.id.credit_note_progress_bar)
    ProgressBar mProgressBarCreditNote;

    @Bind(R.id.customer_sale_invoice_creation_date)
    TextView customerSalesCreationDate;

    @Bind(R.id.sale_return_invoice)
    TextView saleInvoiceReturn;

    @Bind(R.id.txt_service_tax)
    TextView vatServiceTax;

    @Bind(R.id.txt_service_tax_value)
    TextView vatServiceTaxPercent;

    @Bind(R.id.return_against_layout)
    LinearLayout returnAgainstLayout;

    @Bind(R.id.txt_discount_value)
    TextView mDiscountValue;

    @Bind(R.id.txt_discount_label)
    TextView mDiscountLabel;

    @Bind(R.id.txt_outstanding_amt_label)
    TextView mOutstandingAmountLabel;

    @Bind(R.id.txt_outstanding_amt_value)
    TextView mOutstandingAmountValue;

    private DashboardComponent mDashboardComponent;
    private Realm mRealm;
    private CustomerSalesInvoiceAdapter mSalesInvoiceAdapter;
    private RealmList<CustomerSalesInvoice> customerRealmList = new RealmList<>();
    private Activity mActivity;
    private WifiManager mWifiManager;
    private Dialog mPrintDialog;
    private static final String ROW_ITEM = "<tr><td>%s</td><td>%s</td><td>%d</td><td>%s</td></tr>";


    @Inject
    ICustomerSalesInvoiceActionListener mCustomerSalesInvoiceActionListener;

    private double mProductTotal;

    private double mGrandTotal;

    private int mInvoiceNumber;

    private String mStatus;

    private String mCustomerName;
    private String customerID;
    private String mStringInvoiceNumber;

    private File mPdfFile;

    private List<CustomerSalesInvoice> allCartItemsList = new ArrayList<>();

    private RealmList<ProductLite> mLiteList = new RealmList<>();
    private List<ProductLite> mCurrentCreditList = new ArrayList<>();
    private List<String> mInvoiceList = new ArrayList<>();
    private boolean mItemCount;
    private String mExternalStorageDirectory;

    private List<ProductLite> mSalesInvoiceList;
    private String mInvoiceCreationDate;
    private Double mOutStandingAmount;
    private Double mDiscount;
    private int mTaxAmount;
    private String mStringCustomerId;

    public CustomerSalesInvoiceFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_customer_sales_invoice, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.getComponent(DashboardComponent.class).inject(this);

        mRealm = Realm.getDefaultInstance();
        initRecyclerLayout();

        mInvoiceNumber = getInvoiceIntent();
        mGrandTotal = getGrandTotalIntent();
        mStatus = getPaymentStatus();
        manageCreditNoteVisibility(mStatus);


        mCustomerSalesInvoiceActionListener.setView(this);

        mSalesInvoiceList = mCustomerSalesInvoiceActionListener.getCustomerInvoiceListFromDB(mInvoiceNumber);

        mStringInvoiceNumber = mCustomerSalesInvoiceActionListener.getStringInvoiceNumberFromDB(mInvoiceNumber);
        setCustomerSalesInvoiceAdapter(mSalesInvoiceList);

        mSalesInvoiceAdapter.setCallBack(this);

        setCustomerSalesInvoiceDetails();

        mSpinnerInvoiceNumber.setOnItemSelectedListener(this);

        setActionBarTitle();

        mCustomerSalesInvoiceActionListener.fetchVatPercentFromDB();

        mCustomerSalesInvoiceActionListener.fetchTotalCalculation(mSalesInvoiceList);

        mCheckBox.setOnCheckedChangeListener(this);

        if (isAdded()) {
            EventBus.getDefault().post(new ShowCustomeToolbarEvent(false));
        }

    }

    private void manageCreditNoteVisibility(String status) {
        if (status.equals(AppConstants.INVOICE_RETURN) ||
                status.equals(AppConstants.CREDIT_NOTE_ISSUED)) {
            mCreditNote.setVisibility(View.INVISIBLE);
        } else {
            mCreditNote.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.search_item).setVisible(false);
        menu.findItem(R.id.img_create_customer).setVisible(false);
        menu.findItem(R.id.img_print_invoice).setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        final View view;
        switch (id) {
            case R.id.img_print_invoice:
//                callPrintScreens();

                readHtmlFile();
            default:
        }
        return true;
    }

    private void readHtmlFile() {
        try {
            Resources res = getResources();
            InputStream in_s = res.openRawResource(R.raw.invoice);

            byte[] b = new byte[in_s.available()];
            in_s.read(b);

            String htmlString = new String(b);
            Log.d(TAG, "readHtmlFile: " + htmlString);
            StringBuffer stringBuffer = new StringBuffer();

            for (ProductLite productLite : mSalesInvoiceList) {
                double netTotal = productLite.getPrice() * productLite.getItemCount();
                Log.d(TAG, "readHtmlFile: " + productLite.getPrice());
                stringBuffer.append(String.format(ROW_ITEM, productLite.getItemName(), String.valueOf(getResources().getString(R.string.rs) + " " + productLite.getPrice()), productLite.getItemCount(), String.valueOf(getResources().getString(R.string.rs) + " " + netTotal)));
            }

            String stringCompanyName = null;
            String stringCompanyWebsite = null;
            String stringCompanyEmail = null;
            String stringCompanyFax = null;
            String stringCompanyPhone = null;
            String stringCompanyAddress = null;

            RealmQuery<Data> realmQuery = mRealm.where(Data.class);
            Data data = realmQuery.findFirst();
            if (data != null) {
                stringCompanyName = data.getCompany();
                stringCompanyWebsite = data.getWebsite();
                stringCompanyEmail = data.getEmail();
                stringCompanyFax = data.getFax();
                stringCompanyPhone = data.getPhone();
                stringCompanyAddress = data.getAddress();
            }


            htmlString = htmlString.replaceAll("%rows%", stringBuffer.toString());


            String stringInvoiceDate = String.valueOf(mInvoiceCreationDate);
            String stringBufferCustId = mStringCustomerId;
            String stringInvoiceNumber = String.valueOf(mStringInvoiceNumber);
            String stringOutstandingAmount = String.valueOf(mOutStandingAmount);
            String stringProductTotal = String.valueOf(mProductTotal);
            String stringDiscount = String.valueOf(mDiscount);
            String stringTax = String.valueOf(mTaxAmount);


            htmlString = htmlString.replaceAll("%COMPANYNAME%", stringCompanyName);
            htmlString = htmlString.replaceAll("%PHONENO%", stringCompanyPhone);
            htmlString = htmlString.replaceAll("%EMAILID%", stringCompanyEmail);
            htmlString = htmlString.replaceAll("%WEBSITE%", stringCompanyWebsite);
            htmlString = htmlString.replaceAll("%FAXID%", stringCompanyFax);
            htmlString = htmlString.replaceAll("%ADDRESS%", stringCompanyAddress);

            htmlString = htmlString.replaceAll("%CUSTID%", stringBufferCustId);
            htmlString = htmlString.replaceAll("%REFNO%", stringInvoiceNumber);
            htmlString = htmlString.replaceAll("%DATE%", stringInvoiceDate);
            htmlString = htmlString.replaceAll("%OUTAMOUNT%", stringOutstandingAmount);
            htmlString = htmlString.replaceAll("%Total%", stringProductTotal);
            htmlString = htmlString.replaceAll("%DISCOUNT%", stringDiscount);
            htmlString = htmlString.replaceAll("%TAX%", stringTax);


            Log.d(TAG, "formatedHtmlFile: " + htmlString);

            openWebViewActivity(htmlString);

        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "readHtmlFile: Error: can't show help.");
        }

    }

    private void openWebViewActivity(String htmlString) {
        Intent intent = new Intent(getActivity(), WebViewActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.INVOICE_FORMAT, htmlString);
        intent.putExtras(bundle);
        startActivity(intent);
    }


//    private void callPrintScreens() {
//        mObservable.attach(CustomerSalesInvoiceFragment.this);
//        mPrintUtility.print(mPdfFile);
//    }

    @Override
    public void setOutstandingAmount(Double outStandingAmount) {
        updateOutstandingValue(outStandingAmount);
    }

    private void setCustomerSalesInvoiceDetails() {

        mCustomerSalesInvoiceActionListener.fetchCustomerIdFromDB(mInvoiceNumber);

        mCustomerSalesInvoiceActionListener.getCustomerSaleInvoiceCreationDateFromDB(mInvoiceNumber);

        mCustomerSalesInvoiceActionListener.fetchInvoicesForSpinner(mCustomerName);

        mCustomerSalesInvoiceActionListener.fetchReturnInvoice(mInvoiceNumber);

        mCustomerSalesInvoiceActionListener.fetchOutstandingAmount(mInvoiceNumber);
    }

    private void setActionBarTitle() {
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(mCustomerName);
        }
    }

    private void setCustomerSalesInvoiceAdapter(List<ProductLite> salesInvoiceList) {
        mSalesInvoiceAdapter = new CustomerSalesInvoiceAdapter(getActivity(), mInvoiceNumber,
                CustomerSalesInvoiceFragment.this, salesInvoiceList);
        mSalesRecyclerView.setAdapter(mSalesInvoiceAdapter);
    }

    private void initRecyclerLayout() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mSalesRecyclerView.setLayoutManager(layoutManager);
        mSalesRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void setInvoiceListToSpinner(List<String> invoiceList) {
        mInvoiceList.clear();
        mInvoiceList.addAll(invoiceList);
        setSpinnerAdapter(mInvoiceList);
    }

    private void setSpinnerAdapter(List<String> mInvoiceList) {
        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, mInvoiceList);
        mSpinnerInvoiceNumber.setAdapter(stringArrayAdapter);
        mSpinnerInvoiceNumber.setSelection(stringArrayAdapter.getPosition(String.valueOf(mInvoiceNumber)));
    }

    @Override
    public void setReturnAgainstInvoice(String returnInvoice) {
        if (returnInvoice != null) {
            returnAgainstLayout.setVisibility(View.VISIBLE);
            saleInvoiceReturn.setText(" : " + returnInvoice);
        } else {
            returnAgainstLayout.setVisibility(View.GONE);
        }

    }

    @Override
    public void showTaxDetailsToView(int vatTaxAmount, String desc) {
        setVatServiceTaxToView(vatTaxAmount, desc);
    }

    private void setVatServiceTaxToView(int vatTaxAmount, String desc) {
        mTaxAmount = vatTaxAmount;
        vatServiceTax.setText(desc);
        vatServiceTaxPercent.setText(String.valueOf(vatTaxAmount));
    }

    @Override
    public void setCustomerNameToView(String name, String stringCustomerID) {
        mCustomerName = name;
        mStringCustomerId = stringCustomerID;
    }

    @Override
    public void setCustomerIDToView(String custID) {
        customerID = custID;
        mCustomerId.setText(custID);
    }

    public void setCustomerSalesCreationDate(String creationDate) {
        mInvoiceCreationDate = creationDate;
        customerSalesCreationDate.setText(String.valueOf(creationDate));
    }

    @Override
    public void redirectToLogin() {

    }

    @Override
    public void setTotalCalculation(double grandTotalWithGst, Double additionalDiscountPercentage,
                                    Double outStandingAmount) {
        updateGrandTotalValue(grandTotalWithGst);
        updateDiscountValue(additionalDiscountPercentage);
        updateOutstandingValue(outStandingAmount);
//        updateRateTotal(rateTotal);
    }

    private void updateOutstandingValue(Double outStandingAmount) {
        mOutStandingAmount = outStandingAmount;
        if (outStandingAmount != null) {
            mOutstandingAmountValue.setText(getResources().getString(R.string.rs) + " " + String.valueOf(outStandingAmount));
        } else {
            mOutstandingAmountValue.setText(getResources().getString(R.string.rs) + " " + String.valueOf(0.0));
        }
    }

    private void updateDiscountValue(Double additionalDiscountPercentage) {
        mDiscount = additionalDiscountPercentage;
        if (additionalDiscountPercentage != null) {
            mDiscountLabel.setVisibility(View.VISIBLE);
            mDiscountValue.setVisibility(View.VISIBLE);
            mDiscountValue.setText(String.valueOf(additionalDiscountPercentage) + " " + "%");
        } else {
            mDiscountLabel.setVisibility(View.GONE);
            mDiscountValue.setVisibility(View.GONE);
        }
    }

    public void updateGrandTotalValue(double grandTotalWithGst) {
        // do not show grand total with gst show final value from SI
        mProductTotal = grandTotalWithGst;
        mTotalValue.setText(getResources().getString(R.string.rs) + " " + String.valueOf(mProductTotal));
    }

    public void updateRateTotal(double rateTotal) {
        // do not show rate total
        // mRateValue.setText(getResources().getString(R.string.rs) + " " + String.valueOf(rateTotal));
    }

    @OnClick(R.id.button_credit_note)
    public void creditNote() {
        if (mCreditNote.getText().toString().equals(getString(R.string.save_button))) {
            if (!mItemCount) {
                CommonAlertDialog.ShowAlertDialog(getActivity(), getResources().getString(R.string.si_return_error_title),
                        getResources().getString(R.string.si_return_error_message), 0);
                return;
            }
            mCurrentCreditList.addAll(mSalesInvoiceAdapter.getUpdatedList());
            mCustomerSalesInvoiceActionListener.createCustomerSalesInvoiceForReturn(mCurrentCreditList, mInvoiceNumber);

        } else {
            mCreditNote.setText(getActivity().getResources().getString(R.string.save_button));
            mSalesInvoiceAdapter.changeCheckboxState();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public DashboardComponent getComponent() {
        return mDashboardComponent;
    }

    @Override
    public void isItemCountGreater(boolean flag) {
        mItemCount = flag;
    }

    public int getInvoiceIntent() {
        return getArguments().getInt(AppConstants.INVOICE_NUMBER_FIELD_NAME);
    }

    public double getGrandTotalIntent() {
        return getArguments().getDouble(AppConstants.GRAND_TOTAL_FIELD_NAME);
    }

    private String getPaymentStatus() {
        return getArguments().getString(AppConstants.PAYMENT_STATUS);
    }


    @Override
    public void setSalesInvoiceDate(String invoiceDate) {
        setCustomerSalesCreationDate(invoiceDate);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            mSalesInvoiceAdapter.checkAll(true);
        } else {
            mSalesInvoiceAdapter.checkAll(false);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        int invoiceNumber = Integer.parseInt(parent.getSelectedItem().toString());
        List<ProductLite> list = mCustomerSalesInvoiceActionListener.getCustomerInvoiceListFromDB(invoiceNumber);
        mSalesInvoiceList.clear();
        mSalesInvoiceList.addAll(list);
        mSalesInvoiceAdapter.notifyDataSetChanged();
        mCustomerSalesInvoiceActionListener.fetchTotal(invoiceNumber);
        mCustomerSalesInvoiceActionListener.fetchReturnInvoice(invoiceNumber);
        mCustomerSalesInvoiceActionListener.getCustomerSaleInvoiceCreationDateFromDB(invoiceNumber);
        manageCreditNoteVisibility(mCustomerSalesInvoiceActionListener.getPaymentStatus(invoiceNumber));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

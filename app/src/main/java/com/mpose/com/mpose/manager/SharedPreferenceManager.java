package com.mpose.com.mpose.manager;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by lp-ritesh on 26/5/17.
 */

public class SharedPreferenceManager {

    private static SharedPreferenceManager mSharedPreferenceManager;

    private Context mContext;

    private SharedPreferences.Editor mEditor;

    private SharedPreferences mPreference;

    private final String sharedPreferenceKey = "MPOS_APP";

    public static final String IS_CUSTOMER_SELECTED = "is_customer_selected";


    private SharedPreferenceManager(Context context) {
        this.mContext = context.getApplicationContext();
        this.mPreference = mContext.getSharedPreferences(sharedPreferenceKey, Context.MODE_PRIVATE);
        this.mEditor = mPreference.edit();
    }

    public static synchronized SharedPreferenceManager getSharedPreferenceManager(Context context) {
        if (mSharedPreferenceManager == null) {
            mSharedPreferenceManager = new SharedPreferenceManager(context);
        }
        return mSharedPreferenceManager;
    }

    public SharedPreferences.Editor getmEditor() {
        return mEditor;
    }

    public SharedPreferences getmPreference() {
        return mPreference;
    }


    /**
     * @param domainName
     */
    public void setDomainName(String domainName) {
        mEditor.putString(SharedPrefConstants.DOMAIN_NAME, domainName);
        mEditor.commit();
    }

    /**
     * @return
     */
    public String getDomainName() {
        return mPreference.getString(SharedPrefConstants.DOMAIN_NAME, null);
    }

    /**
     * @param isUserLoggedIn
     */
    public void setUserLoggedIn(boolean isUserLoggedIn) {
        mEditor.putBoolean(SharedPrefConstants.USER_LOGGED_IN, isUserLoggedIn);
        mEditor.commit();
    }

    /**
     * @return
     */
    public boolean getUserLoggedIn() {

        return mPreference.getBoolean(SharedPrefConstants.USER_LOGGED_IN, false);
    }


    /**
     * @param sid
     */
    public void setSID(String sid) {
        mEditor.putString(SharedPrefConstants.SID, sid);
        mEditor.commit();

    }

    /**
     * @return
     */
    public String getSID() {
        return mPreference.getString(SharedPrefConstants.SID, null);
    }


    /**
     * @param syncDateTime
     */
    public void setSyncDateTime(String syncDateTime) {
        mEditor.putString(SharedPrefConstants.SYNC_DATE_TIME, syncDateTime);
        mEditor.commit();
    }

    /**
     * @return
     */
    public String getSyncDateTime() {
        return mPreference.getString(SharedPrefConstants.SYNC_DATE_TIME, null);
    }

    /**
     * @param isFirstTimeCalled
     */
    public void setIsFirstTimeCalled(boolean isFirstTimeCalled) {
        mEditor.putBoolean(SharedPrefConstants.IS_FIRST_TIME_CALLED, isFirstTimeCalled);
        mEditor.commit();
    }


    /**
     * @return
     */
    public boolean getIsFirstTimeCalled() {
        return mPreference.getBoolean(SharedPrefConstants.IS_FIRST_TIME_CALLED, false);
    }


    /**
     * @param totalCountFromApi
     */
    public void setTotalCountFromApi(int totalCountFromApi) {
        mEditor.putInt(SharedPrefConstants.TOTAL_COUNT_FROM_API, totalCountFromApi);
        mEditor.commit();
    }

    /**
     * @return
     */
    public int getTotalCountFromApi() {
        return mPreference.getInt(SharedPrefConstants.TOTAL_COUNT_FROM_API, 0);
    }

    public void setSelectedCustomer(String selectedCustomer) {
        mEditor.putString(SharedPrefConstants.SELECTED_CUSTOMER, selectedCustomer);
        mEditor.commit();
    }

    /**
     * @return
     */
    public String getSelectedCustomer() {
        return mPreference.getString(SharedPrefConstants.SELECTED_CUSTOMER, null);
    }


    /**
     * @param isCustomerSelected
     */
    public void setIsCustomerSelected(boolean isCustomerSelected) {
        mEditor.putBoolean(SharedPrefConstants.IS_CUSTOMER_SELECTED, isCustomerSelected);
        mEditor.commit();
    }

    /**
     * @return
     */

    public boolean getIsCustomerSelected() {
        return mPreference.getBoolean(SharedPrefConstants.IS_CUSTOMER_SELECTED, false);
    }

    /**
     * @param fullName
     */
    public void setUserFullName(String fullName) {
        mEditor.putString(SharedPrefConstants.USER_FULL_NAME, fullName);
        mEditor.commit();
    }

    /**
     * @return
     */
    public String getUserFullName() {
        return mPreference.getString(SharedPrefConstants.USER_FULL_NAME, null);
    }

    /**
     *
     * @param userName
     */
    public void setUserName(String userName) {
        mEditor.putString(SharedPrefConstants.USER_NAME, userName);
        mEditor.commit();
    }

    /**
     *
     * @return
     */
    public String getUserName() {
        return mPreference.getString(SharedPrefConstants.USER_NAME, null);
    }

    public void removeEntry(String key) {
        mEditor.remove(key);
        mEditor.commit();

    }


    public void clearAllPreferences() {
        mEditor.clear();
        mEditor.commit();
    }
}
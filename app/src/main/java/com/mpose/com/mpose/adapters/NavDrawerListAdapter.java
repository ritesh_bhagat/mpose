package com.mpose.com.mpose.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mpose.com.mpose.R;
import com.mpose.com.mpose.eventbus.NavigationDrawerEvent;
import com.mpose.com.mpose.utils.AppConstants;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by lp-ritesh on 4/7/17.
 */

public class NavDrawerListAdapter extends RecyclerView.Adapter<NavDrawerListAdapter.ViewHolder> {

    private final ArrayList<String> keyset = new ArrayList<>();

    private Context mContext;
    private LinkedHashMap<String, Drawable> mNavList;

    public NavDrawerListAdapter(Context mContext, LinkedHashMap<String, Drawable> mNavList) {
        this.mContext = mContext;
        this.mNavList = mNavList;
        keyset.addAll(this.mNavList.keySet());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.nav_drawer_recycler_view_item_row, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.navTitle.setText(keyset.get(position));
        holder.navImageView.setImageDrawable(mNavList.get(keyset.get(position)));

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                EventBus.getDefault().post(new NavigationDrawerEvent(keyset.get(position)));

//                Toast.makeText(mContext, "click" + keyset.get(position), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mNavList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.nav_txt_title)
        TextView navTitle;

        @Bind(R.id.nav_image_icon)
        ImageView navImageView;

        @Bind(R.id.nav_item_layout)
        LinearLayout linearLayout;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }


    }
}

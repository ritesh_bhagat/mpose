package com.mpose.com.mpose.eventbus;

/**
 * Created by lp-ritesh on 8/7/17.
 */

public class ManageViewEvent {


    private boolean mIsCalledFromGrid;

    public ManageViewEvent(boolean flag) {
        this.mIsCalledFromGrid = flag;
    }

    public boolean isIsCalledFromGrid() {
        return mIsCalledFromGrid;
    }

    public void setIsCalledFromGrid(boolean isCalledFromGrid) {
        this.mIsCalledFromGrid = isCalledFromGrid;
    }


}

package com.mpose.com.mpose.expense.models.expenseresponse;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class ExpenseResponseMessage extends RealmObject{

    @SerializedName("status")
    private String status;
    @SerializedName("erp_expense_id")
    private String erpExpenseId;
    @SerializedName("pos_expense_id")
    private String posExpenseId;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErpExpenseId() {
        return erpExpenseId;
    }

    public void setErpExpenseId(String erpExpenseId) {
        this.erpExpenseId = erpExpenseId;
    }

    public String getPosExpenseId() {
        return posExpenseId;
    }

    public void setPosExpenseId(String posExpenseId) {
        this.posExpenseId = posExpenseId;
    }

}
package com.mpose.com.mpose.utils;

import com.mpose.com.mpose.expense.models.addexpense.CustomerExpenseModel;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class ExpenseIdUtils {

    public static final int EXPENSE_ID_START = 50001;


    public static int getNewExpenseId() {
        Realm mRealm = Realm.getDefaultInstance();
        RealmResults<CustomerExpenseModel> resultList = mRealm.where(CustomerExpenseModel.class)
                .findAllSorted(AppConstants.EXPENSE_ID_FIELD_NAME, Sort.DESCENDING);
        if (resultList.size() <= 0) {
            return EXPENSE_ID_START;
        }

        CustomerExpenseModel expense = resultList.first();
        if (expense == null) {
            return EXPENSE_ID_START;
        } else {
            int expenseId = -1;
            try {
                expenseId = expense.getExpenseID();
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
            }
            return expenseId + 1;
        }
    }

}
package com.mpose.com.mpose.expense.views;

import com.mpose.com.mpose.common.BaseView;

import java.util.List;

/**
 * Created by lp-ritesh on 19/8/17.
 */

public interface AddExpenseView extends BaseView {

    void setValuesToSpinner(List<String> list);

    String getDescription();

    String getDateOfExpense();

    String getFromEmployee();

    String getExpenseType();

    double getClaimAmount();

    void onExpenseCreationSuceess();
}

package com.mpose.com.mpose.dashboard.presenter;

import android.content.Context;
import android.util.Log;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.mpose.com.mpose.common.Presenter;
import com.mpose.com.mpose.dashboard.actionlistener.IDashboardActionListener;
import com.mpose.com.mpose.dashboard.model.report.ReportData;
import com.mpose.com.mpose.dashboard.model.report.ReportDetailModel;
import com.mpose.com.mpose.dashboard.model.report.ReportStatusMessage;
import com.mpose.com.mpose.dashboard.view.DashboardView;
import com.mpose.com.mpose.di.PerActivity;
import com.mpose.com.mpose.manager.SharedPreferenceManager;
import com.mpose.com.mpose.retrofit.ApiClient;
import com.mpose.com.mpose.retrofit.ApiInterface;
import com.mpose.com.mpose.utils.AppConstants;
import com.mpose.com.mpose.utils.NetworkConncetionUtility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lp-ritesh on 5/7/17.
 */

@PerActivity
public class DashboardPresenter extends Presenter implements IDashboardActionListener<DashboardView> {

    private static final String TAG = DashboardPresenter.class.getSimpleName();
    private Context mContext;
    private DashboardView mDashboardView;
    private ReportStatusMessage mReportDetailModel;
    private ArrayList<BarEntry> mChartEntries = new ArrayList<>();
    private List<String> mPostingDatesList = new ArrayList<>();
    private BarDataSet barDataSet;
    private BarData barData;
    private RealmList<ReportData> reportDataRealmList;


    @Inject
    DashboardPresenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void resume() {
        super.resume();
        if (!NetworkConncetionUtility.getInstance(mContext).isConnected()) {
            reportDataRealmList = getLocalChartDataFromDB();
            showLocalData(reportDataRealmList);
        } else {
            if (mReportDetailModel == null) {
                getDashBoardChartData(mDashboardView.getStartDate(), mDashboardView.getEndDate());
            } else {
                reportDataRealmList = getLocalChartDataFromDB();
                showLocalData(reportDataRealmList);
                getDashBoardChartData(mDashboardView.getStartDate(), mDashboardView.getEndDate());
            }

        }


//        getDashBoardChartData(mDashboardView.getStartDate(), mDashboardView.getEndDate());
    }

    private RealmList<ReportData> getLocalChartDataFromDB() {
        RealmList<ReportData> list = new RealmList<>();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<ReportData> realmResults = realm.where(ReportData.class).findAll();
        list.clear();
        list.addAll(realmResults);
        return list;
    }

    @Override
    public void setView(DashboardView dashboardView) {
        this.mDashboardView = dashboardView;
    }

    @Override
    public void getDashBoardChartData(String startDate, String endDate) {
        if (!NetworkConncetionUtility.getInstance(mContext).isConnected()) {
            mDashboardView.showNetworkConnectionError();
            return;
        }

        mDashboardView.showLoading();
        String sid = SharedPreferenceManager.getSharedPreferenceManager(mContext).getSID();

        ApiInterface apiInterface = ApiClient.getClient(mContext).create(ApiInterface.class);
        Call<ReportDetailModel> modelCall = apiInterface.getPOSReport(sid, startDate, endDate);
        modelCall.enqueue(new Callback<ReportDetailModel>() {
            @Override
            public void onResponse(Call<ReportDetailModel> call, Response<ReportDetailModel> response) {

                if (response.isSuccessful()) {
                    mReportDetailModel = response.body().getMessage();
                    saveChartDataToDB(response.body().getMessage());
                    manageChartResponse(response.body().getMessage());
//                    mDashboardView.onPosChartSucessResponse(response.body());
                } else {
                    if (response.code() == AppConstants.RESPONSE_FORBIDDEN) {
                        mDashboardView.showSessionExpiryDialog();
                    }
                    Log.d(TAG, " forbidden " + response.code());
                }
                mDashboardView.hideLoading();
            }

            @Override
            public void onFailure(Call<ReportDetailModel> call, Throwable t) {
                Log.e(TAG, "Chart - " + t.getMessage());
                mDashboardView.hideLoading();
                mDashboardView.showOnFailureAlert();
            }
        });

    }

    private void saveChartDataToDB(final ReportStatusMessage message) {
        final Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(ReportStatusMessage.class);
                realm.delete(ReportData.class);
                realm.insertOrUpdate(message);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                RealmResults<ReportStatusMessage> realmResults = realm.where(ReportStatusMessage.class).findAll();
                for (ReportStatusMessage model : realmResults) {
                    Log.d(TAG, "onSuccess:ReportStatusMessage size " + model.getData().size());
                }

                RealmResults<ReportDetailModel> results = realm.where(ReportDetailModel.class).findAll();
                for (ReportDetailModel model : results) {
                    Log.d(TAG, "onSuccess:ReportDetailModel size " + model.getMessage());
                }

            }
        });


    }

    private void manageChartResponse(ReportStatusMessage body) {
        mPostingDatesList.clear();
        mChartEntries.clear();
        for (ReportData data : body.getData()) {
            String formattedDate = DateFormatter(data.getPostingDate());
            mPostingDatesList.add(formattedDate);
        }
        float value = 0;
        for (int i = 0; i < body.getData().size(); i++) {
            value = Float.parseFloat(String.valueOf(body.getData().get(i).getTotalSales()));
            if (value >= 0) {
                mChartEntries.add(new BarEntry(i, value));
            }
        }
        setDefaultData();
        mDashboardView.showChart(mChartEntries, mPostingDatesList);
    }

    @Override
    public void createCustomerDB() {
        mDashboardView.createCustomerDB();
    }

    private void showLocalData(RealmList<ReportData> reportDataModel) {
        mPostingDatesList.clear();
        mChartEntries.clear();
        for (ReportData data : reportDataModel) {
            String formattedDate = DateFormatter(data.getPostingDate());
            mPostingDatesList.add(formattedDate);
        }
        float value = 0;
        for (int i = 0; i < reportDataModel.size(); i++) {
            value = Float.parseFloat(String.valueOf(reportDataModel.get(i).getTotalSales()));
            mChartEntries.add(new BarEntry(i, value));
        }
        setDefaultData();
        mDashboardView.showChart(mChartEntries, mPostingDatesList);
    }

//    private void showPersistentData(ReportDetailModel reportDetailModel) {
//        mPostingDatesList.clear();
//        mChartEntries.clear();
//        this.mReportDetailModel = reportDetailModel;
//        for (ReportData data : reportDetailModel.getMessage().getData()) {
//            String formattedDate = DateFormatter(data.getPostingDate());
//            mPostingDatesList.add(formattedDate);
//        }
//        float value = 0;
//        for (int i = 0; i < this.mReportDetailModel.getMessage().getData().size(); i++) {
//            value = Float.parseFloat(String.valueOf(this.mReportDetailModel.getMessage().getData().get(i).getTotalSales()));
//            mChartEntries.add(new BarEntry(i, value));
//        }
//        setDefaultData();
//        mDashboardView.showChart(mChartEntries, mPostingDatesList);
//    }

    private String DateFormatter(String postingDate) {
        String outputString = null;
        try {
            SimpleDateFormat inputDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date inputD = inputDate.parse(postingDate);
            SimpleDateFormat outputDate = new SimpleDateFormat("MMM dd", Locale.ENGLISH);
            outputString = outputDate.format(inputD);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return outputString;
    }

    private void setDefaultData() {
        if (mPostingDatesList.size() == 0) {
            mPostingDatesList.add(DateFormatter(new Date().toString()));
        }
        if (mChartEntries.size() == 0) {
            mChartEntries.add(new BarEntry(0, 0));
        }
    }

}

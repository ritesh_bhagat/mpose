package com.mpose.com.mpose.pos.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by lp-ritesh on 5/7/17.
 */

public class AllProducts extends RealmObject{

    @SerializedName("message")
    private ProductMessage message;

    public ProductMessage getMessage() {
        return message;
    }

    public void setMessage(ProductMessage message) {
        this.message = message;
    }
}



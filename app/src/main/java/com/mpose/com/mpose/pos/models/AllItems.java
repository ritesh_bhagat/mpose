package com.mpose.com.mpose.pos.models;

/**
 * Created by lp-ritesh on 1/7/17.
 */

public class AllItems {

    private String itemName;
    private int itemAvailableQuantity;
    private String itemPrice;
    private String itemDiscountPrice;
    private int itemCount;
    private int itemAvailableCount;


    public AllItems(String itemName, int itemQuantity, String itemPrice, String itemDiscountPrice, int itemCount) {
        this.itemName = itemName;
        this.itemAvailableQuantity = itemQuantity;
        this.itemPrice = itemPrice;
        this.itemDiscountPrice = itemDiscountPrice;
        this.itemCount = itemCount;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemAvailableQuantity() {
        return itemAvailableQuantity;
    }

    public void setItemAvailableQuantity(int itemAvailableQuantity) {
        this.itemAvailableQuantity = itemAvailableQuantity;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemDiscountPrice() {
        return itemDiscountPrice;
    }

    public void setItemDiscountPrice(String itemDiscountPrice) {
        this.itemDiscountPrice = itemDiscountPrice;
    }

    public int getItemAvailableCount() {
        return itemAvailableCount;
    }

    public void setItemAvailableCount(int itemAvailableCount) {
        this.itemAvailableCount = itemAvailableCount;
    }

}

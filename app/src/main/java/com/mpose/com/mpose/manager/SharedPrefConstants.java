package com.mpose.com.mpose.manager;

/**
 * Created by lp-ritesh on 26/5/17.
 */

public class SharedPrefConstants {

    public static final String FCM_TOKEN = "fcm_token";

    public static final String DEVICE_TOKEN = "device_token";

    public static final String USER_LOGGED_IN = "user_logged_in";

    public final static String SID = "sid";

    public static final String IS_FIRST_TIME_CALLED = "is_first_time_called";

    public static final String SYNC_DATE_TIME = "sync_date_time";

    public static final String TOTAL_COUNT_FROM_API = "total_count_from_api";

    public static final String SELECTED_CUSTOMER = "selected_customer";

    public static final String IS_CUSTOMER_SELECTED = "is_customer_selected";

    public static final String DOMAIN_NAME = "domain_name";


    public static final String USER_FULL_NAME = "full_name";

    public static final String USER_NAME = "user_name";
}

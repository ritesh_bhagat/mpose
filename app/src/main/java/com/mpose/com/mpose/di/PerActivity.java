package com.mpose.com.mpose.di;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by lp-ritesh on 23/6/17.
 */

@Scope
@Retention(RUNTIME)
public @interface PerActivity {
}

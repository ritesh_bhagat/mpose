package com.mpose.com.mpose.salesinvoice.presenter;

import android.content.Context;
import android.util.Log;

import com.mpose.com.mpose.common.Presenter;
import com.mpose.com.mpose.di.PerActivity;
import com.mpose.com.mpose.pos.models.CustomerSalesInvoice;
import com.mpose.com.mpose.salesinvoice.actionlistener.ISalesInvoiceActionListener;
import com.mpose.com.mpose.salesinvoice.view.SalesInvoiceView;
import com.mpose.com.mpose.utils.AppConstants;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by lp-ritesh on 18/7/17.
 */

@PerActivity
public class SalesInvoicePresenter extends Presenter implements
        ISalesInvoiceActionListener<SalesInvoiceView> {

    private static final String TAG = SalesInvoicePresenter.class.getSimpleName();
    private Context mContext;
    private Realm mRealm;
    private SalesInvoiceView mSalesInvoiceView;
    private RealmList<CustomerSalesInvoice> mCustomerStatusInvoicesList;

    @Inject
    public SalesInvoicePresenter(Context mContext) {
        this.mContext = mContext;
        mRealm = Realm.getDefaultInstance();
        mCustomerStatusInvoicesList = new RealmList<>();
    }

    @Override
    public void setView(SalesInvoiceView salesInvoiceView) {
        this.mSalesInvoiceView = salesInvoiceView;
    }

    @Override
    public RealmResults<CustomerSalesInvoice> getFilteredList(String input) {
        Log.d(TAG, "getFilteredList: input " + input);
        RealmResults<CustomerSalesInvoice> results = null;

        results = mRealm.where(CustomerSalesInvoice.class)
                .beginGroup()
//                .equalTo(AppConstants.INVOICE_NUMBER_FIELD_NAME, Integer.parseInt(input))
//                .or()
                .contains(AppConstants.PAYMENT_STATUS_FIELD_NAME, input, Case.INSENSITIVE)
                .or()
                .contains(AppConstants.CUSTOMER_NAME_FIELD_NAME, input, Case.INSENSITIVE)
                .endGroup().findAll();

        Log.d(TAG, "getFilteredList: list- " + results.size());
        for (CustomerSalesInvoice salesInvoice : results) {
            Log.d(TAG, "getFilteredList: list- " + results.size());
        }
        return results;
    }

    @Override
    public void getCustomerInvoiceList() {
        RealmResults<CustomerSalesInvoice> realmResults =
                mRealm.where(CustomerSalesInvoice.class).findAll();
        mCustomerStatusInvoicesList.clear();
        mCustomerStatusInvoicesList.addAll(realmResults);
    }

    @Override
    public void sortListByTimeStamp() {
        try {
            Log.d(TAG, "sortListByTimeStamp: " + mCustomerStatusInvoicesList);
            Collections.sort(mCustomerStatusInvoicesList, new Comparator<CustomerSalesInvoice>() {
                @Override
                public int compare(CustomerSalesInvoice obj1, CustomerSalesInvoice obj2) {
                    return obj2.getInvoiceDate().compareTo(obj1.getInvoiceDate());
                }
            });
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
        if (mCustomerStatusInvoicesList.size() <= 0) {
            mSalesInvoiceView.showEmptyInvoiceImage();
        }
        mSalesInvoiceView.callAdapter(mCustomerStatusInvoicesList);

    }

    @Override
    public void checkSalesInvoicesSyncStatus() {
        RealmQuery<CustomerSalesInvoice> realmQuery = mRealm.where(CustomerSalesInvoice.class)
                .equalTo("isInvoiceSync", true);
        RealmResults<CustomerSalesInvoice> results = realmQuery.findAll();
        for (CustomerSalesInvoice invoice : results) {
            Log.d(TAG, "checkResult: invoices Synced " + invoice.getInvoiceNumber());

        }
    }

    @Override
    public List<CustomerSalesInvoice> getAllInvoicesList() {
        return mCustomerStatusInvoicesList;
    }
}

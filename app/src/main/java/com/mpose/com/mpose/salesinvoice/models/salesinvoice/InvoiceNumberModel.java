package com.mpose.com.mpose.salesinvoice.models.salesinvoice;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lp-ritesh on 21/7/17.
 */

public class InvoiceNumberModel extends RealmObject {

    @PrimaryKey
    @SerializedName("invoiceID")
    private String invoiceID;

    @SerializedName("invoiceNumber")
    private int invoiceNumber;

    public int getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(int invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceID() {
        return invoiceID;
    }

    public void setInvoiceID(String invoiceID) {
        this.invoiceID = invoiceID;
    }
}

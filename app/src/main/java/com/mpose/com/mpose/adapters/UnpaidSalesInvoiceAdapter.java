package com.mpose.com.mpose.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mpose.com.mpose.R;
import com.mpose.com.mpose.eventbus.UnpaidSalesPaymentEvent;
import com.mpose.com.mpose.salesinvoice.models.unpaid.UnpaidSalesInvoice;
import com.mpose.com.mpose.sync.model.bulkpaymentrequest.BulkPaymentData;
import com.mpose.com.mpose.utils.AppConstants;
import com.mpose.com.mpose.utils.DateTimeUtil;
import com.mpose.com.mpose.utils.PaymentUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anjali on 27-09-2017.
 */

public class UnpaidSalesInvoiceAdapter extends RecyclerView.Adapter<UnpaidSalesInvoiceAdapter.ViewHolder> {

    private static final String TAG = ExpenseClaimAdapter.class.getSimpleName();
    private Context mContext;
    private List<UnpaidSalesInvoice> mUnpaidSalesInvoiceList = new ArrayList<>();


    public UnpaidSalesInvoiceAdapter(Context context, List<UnpaidSalesInvoice> unpaidSalesInvoices) {
        mContext = context;
        mUnpaidSalesInvoiceList = unpaidSalesInvoices;

    }

    @Override
    public UnpaidSalesInvoiceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.unpaid_sales_invoice_item, parent, false);
        return new UnpaidSalesInvoiceAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UnpaidSalesInvoiceAdapter.ViewHolder holder, int position) {
        final UnpaidSalesInvoice unpaidSalesInvoice = mUnpaidSalesInvoiceList.get(position);
        holder.posId.setText(unpaidSalesInvoice.getPosId());
        holder.erpInvoiceId.setText(unpaidSalesInvoice.getErpInvoiceId());
        holder.customer.setText(unpaidSalesInvoice.getCustomer());
        holder.outstandingAmount.setText(mContext.getString(R.string.rs) + " " + String.valueOf(unpaidSalesInvoice.getOutstandingAmount()));
        holder.status.setText(unpaidSalesInvoice.getStatus());
        holder.mPayNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUnpaidDataModel(unpaidSalesInvoice);
            }
        });
    }

    private void setUnpaidDataModel(UnpaidSalesInvoice unpaidSalesInvoice) {

        int paymentId = PaymentUtils.getNewPaymentId();
        Random rand = new Random();
        final int x = rand.nextInt(paymentId++);
        String currentDate = DateTimeUtil.currentDate(AppConstants.DATE_FORMAT_YYYY_MM_DD);
        BulkPaymentData data = new BulkPaymentData();
        data.setPaymentId(x + "-" + paymentId);
        data.setInvoiceId(unpaidSalesInvoice.getPosId());
        data.setModeOfPayment("Cash");
        data.setAllocatedAmount(unpaidSalesInvoice.getOutstandingAmount());
//        data.setPaymentId(unpaidSalesInvoice.getPosId());
        data.setPostingDate(currentDate);
        EventBus.getDefault().post(new UnpaidSalesPaymentEvent(data));
    }


    @Override
    public int getItemCount() {
        if (mUnpaidSalesInvoiceList.size() > 0) {
            return mUnpaidSalesInvoiceList.size();
        }
        return 0;
    }

    public void setFilteredList(List<UnpaidSalesInvoice> filteredList) {
        mUnpaidSalesInvoiceList.clear();
        mUnpaidSalesInvoiceList.addAll(filteredList);
    }

    public void setOriginalList(List<UnpaidSalesInvoice> unpaidSalesInvoiceList) {
        Log.d(TAG, "setOriginalList: " + unpaidSalesInvoiceList.size());
        mUnpaidSalesInvoiceList.clear();
        mUnpaidSalesInvoiceList.addAll(mUnpaidSalesInvoiceList);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_pos_id_value)
        TextView posId;
        @Bind(R.id.tv_erp_invoice_id_value)
        TextView erpInvoiceId;
        @Bind(R.id.tv_customer_value)
        TextView customer;
        @Bind(R.id.tv_outstanding_amount_value)
        TextView outstandingAmount;
        @Bind(R.id.tv_status_value)
        TextView status;
        @Bind(R.id.btn_pay_now)
        Button mPayNowBtn;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
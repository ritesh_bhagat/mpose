package com.mpose.com.mpose.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.percent.PercentRelativeLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.mpose.com.mpose.MposeApplication;
import com.mpose.com.mpose.R;
import com.mpose.com.mpose.activity.BaseActivity;
import com.mpose.com.mpose.activity.HomeActivity;
import com.mpose.com.mpose.adapters.CustomerAdapter;
import com.mpose.com.mpose.adapters.POSAdapter;
import com.mpose.com.mpose.adapters.PayLayoutRecyclerViewAdapter;
import com.mpose.com.mpose.di.HasComponent;
import com.mpose.com.mpose.di.component.DashboardComponent;
import com.mpose.com.mpose.eventbus.AddItemCountEvent;
import com.mpose.com.mpose.eventbus.CustomerNameEvent;
import com.mpose.com.mpose.eventbus.GenerateSalesInvoice;
import com.mpose.com.mpose.eventbus.RefreshListEvent;
import com.mpose.com.mpose.manager.SharedPreferenceManager;
import com.mpose.com.mpose.pos.actionlistener.IPOSActionListener;
import com.mpose.com.mpose.pos.models.AllProducts;
import com.mpose.com.mpose.pos.models.Customer;
import com.mpose.com.mpose.pos.models.Product;
import com.mpose.com.mpose.pos.view.POSView;
import com.mpose.com.mpose.salesinvoice.models.salesinvoicebulkapi.SalesRequestData;
import com.mpose.com.mpose.utils.AppConstants;
import com.mpose.com.mpose.utils.CommonAlertDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by lp-ritesh on 7/7/17.
 */

public class POSFragment extends BaseFragment implements POSView,
        HasComponent<DashboardComponent>, TextWatcher, View.OnClickListener, View.OnTouchListener,
        AdapterView.OnItemClickListener {

    private static final String TAG = POSFragment.class.getSimpleName();

    @Bind(R.id.img_grid_icon)
    ImageView mGridViewIcon;

    @Bind(R.id.img_list_icon)
    ImageView mListViewIcon;

    @Bind(R.id.item_recycler_view)
    RecyclerView mRecyclerView;

    @Bind(R.id.pay_recycler_view)
    RecyclerView mPayRecyclerView;

    @Bind(R.id.txt_pay_button)
    TextView payButton;

    @Bind(R.id.txt_grand_total_value)
    TextView mGrandTotal;

    @Bind(R.id.txt_net_value)
    TextView mNetTotal;

    @Bind(R.id.pos_progress_bar)
    ProgressBar mPosProgressBar;

    @Bind(R.id.content_dashboard_layout)
    View mContentDashboardLayout;

    @Bind(R.id.percent_layout_frame)
    PercentRelativeLayout mPercentRelativeLayoutFrame;

    @Bind(R.id.pay_layout)
    View mCustomerPayLayout;

//    @Bind(R.id.search_layout)
//    View mSearchLayout;

    @Bind(R.id.empty_cart_image)
    ImageView mEmptyCart;

    @Bind(R.id.view_notch)
    ImageView mViewNotch;

    @Bind(R.id.open_notch)
    ImageView mOpenNotch;

    @Bind(R.id.dashboard_view_layout)
    LinearLayout mViewLayout;

    @Bind(R.id.total_layout)
    RelativeLayout mTotalLayout;

    @Bind(R.id.txt_customer_name_label)
    TextView mCustomerName;

    @Bind(R.id.ed_discount_value)
    EditText edDiscountValue;

    @Bind(R.id.txt_vat_percent_value)
    TextView vatPercentValue;

    @Bind(R.id.txt_vat_label)
    TextView vatPercentLabel;

    @Bind(R.id.empty_cart_lay)
    View mEmptyCartLayout;

    @Bind(R.id.empty_invoices)
    ImageView mEmptyInvoiceImage;

    @Bind(R.id.rel_customer_cart_lay)
    View mRelativeCustomerCartLayout;

    @Bind(R.id.empty_cart_product_pay_lay)
    View mEmptyCartProductPayLayout;

    @Inject
    IPOSActionListener mPosActionListener;

    private Realm mRealm;

    private POSAdapter mPosAdapter;
    private PayLayoutRecyclerViewAdapter mPayLayoutRecyclerViewAdapter;

    private StaggeredGridLayoutManager mStaggeredGridLayoutManager;
    private RecyclerView.LayoutManager mLinearLayoutManager;

    private DashboardComponent mDashboardComponent;

    private List<Product> mProductList = new ArrayList<>();

    private RealmList<Product> searchResultList;
    private List<Product> mProductCartList = new ArrayList<>();

    private MaterialDialog mMaterialDialog;
    private double mGrandTotalWithGst;
    private Animation mPreviousAnim;
    private SearchView mMaterialSearchView;

    private AutoCompleteTextView mSearchCustomer;

    private List<Customer> mCustomerList;
    private CustomerAdapter mCustomerAdapter;


    public POSFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        mProductList.clear();
        mProductList.addAll(mPosActionListener.getAllProductsFromDB());
        if (mProductCartList.size() > 0) {
            mRelativeCustomerCartLayout.setVisibility(View.VISIBLE);
            mEmptyCartProductPayLayout.setVisibility(View.INVISIBLE);
        } else {
            mEmptyCartProductPayLayout.setVisibility(View.VISIBLE);
            mRelativeCustomerCartLayout.setVisibility(View.INVISIBLE);
        }
        hideKeyboard();
        Log.d(TAG, "onResume: ");
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        setHasOptionsMenu(true);
        mRealm = Realm.getDefaultInstance();
        initMaterialDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pos, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void redirectToLogin() {
        ((BaseActivity) getActivity()).navigateToLoginScreen(getActivity());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.getComponent(DashboardComponent.class).inject(this);

        mPosActionListener.setView(this);
        setActionBarTitle();
        setLayoutManager();
        POSAdapter.mIsCalledFromGrid = false;
        setPayLayoutRecyclerViewAdapter();

        Toolbar mToolbar = ((HomeActivity) getActivity()).mToolbar;

        mSearchCustomer = (AutoCompleteTextView) mToolbar.findViewById(R.id.ed_customer_name);
        View mCustomToolbar = (View) mToolbar.findViewById(R.id.custom_content_toolbar);

//        mSearchQuery = (EditText) mToolbar.findViewById(R.id.ed_search_query);
//        mClearQuery = (ImageView) mToolbar.findViewById(R.id.img_query);

//        mClearQuery.setOnClickListener(this);

        if (mProductCartList.size() > 0) {
            mPosActionListener.checkTotalAndUpdateView();
        }


        mPosActionListener.fetchVatTaxPercentFromDB();

        setPOSAdapter(mProductList);

        mPosActionListener.resume("", "0", "30");

        mPreviousAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.left_to_center);

        setCustomerNameToPayLayout();

        if (isAdded()) {
            mCustomToolbar.setVisibility(View.VISIBLE);
        }

        mCustomerList = mPosActionListener.fetchCustomerFromDB();
        showAddCustomerForFirstTime();
        setCustomerAdapter();
        mSearchCustomer.setOnTouchListener(this);
        mSearchCustomer.setOnItemClickListener(this);
        mSearchCustomer.addTextChangedListener(this);

    }

    private void setCustomerNameToPayLayout() {
        if (((MposeApplication) getActivity().getApplicationContext()).isCustomerSelected()) {
            String name = SharedPreferenceManager.getSharedPreferenceManager(getActivity())
                    .getSelectedCustomer();
            mCustomerName.setText(name);
        }
    }

    private void setPayLayoutRecyclerViewAdapter() {
        mPayLayoutRecyclerViewAdapter = new PayLayoutRecyclerViewAdapter(getActivity(),
                mProductCartList);
        mPayRecyclerView.setAdapter(mPayLayoutRecyclerViewAdapter);
    }


    private void setLayoutManager() {
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mStaggeredGridLayoutManager = new StaggeredGridLayoutManager(3, 1);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());

        mPayRecyclerView.setLayoutManager(layoutManager);
        mPayRecyclerView.setItemAnimator(new DefaultItemAnimator());

    }

    private void setActionBarTitle() {
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_pos);
        }
    }

    private void initMaterialDialog() {
        mMaterialDialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.loading_data)
                .content(R.string.please_wait)
                .progress(true, 0)
                .progressIndeterminateStyle(false)
                .build();
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mProductCartList.clear();
    }

    private void setNewList() {
        if (searchResultList.isEmpty()) {
            mEmptyCartLayout.setVisibility(View.VISIBLE);
        } else {
            mEmptyCartLayout.setVisibility(View.GONE);
        }
        mPosAdapter.setList(searchResultList);
        mPosAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.view_notch)
    public void switchView() {
        if (mContentDashboardLayout.isShown()) {
            mOpenNotch.animate().setDuration(100).rotation(0).start();
            mContentDashboardLayout.setVisibility(View.GONE);
        } else {
            mOpenNotch.animate().setDuration(100).rotation(-180).start();
            mContentDashboardLayout.setVisibility(View.VISIBLE);
        }
    }

//    @OnClick(R.id.img_query)
//    public void clearSearch() {
////        mSearchQuery.setText("");
//        Animation bottomUp = AnimationUtils.loadAnimation(getContext(), R.anim.slide_out_up);
////        mSearchLayout.startAnimation(bottomUp);
////        mSearchLayout.setVisibility(View.GONE);
//        notifyPosAdapter();
//        notifyPayAdapter();
//    }

    @Override
    public void onPause() {
        super.onPause();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        Log.d(TAG, "onPause: ");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        Log.d(TAG, "onDestroy: ");
        mRealm.close();
        if (mMaterialDialog.isShowing()) {
            mMaterialDialog.dismiss();
        }
    }

    @OnClick(R.id.img_grid_icon)
    public void showGridView() {
        POSAdapter.mIsCalledFromGrid = true;
        mRecyclerView.setLayoutManager(mStaggeredGridLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        notifyPosAdapter();
        animateLayout();
    }

    private void animateLayout() {
        mPreviousAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mContentDashboardLayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mViewLayout.startAnimation(mPreviousAnim);
    }

    @OnClick(R.id.img_list_icon)
    public void showListView() {
        POSAdapter.mIsCalledFromGrid = false;
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        notifyPosAdapter();
        animateLayout();
    }

    public void notifyPosAdapter() {
        mPosAdapter.notifyDataSetChanged();
    }

    public void notifyPayAdapter() {
        mPayLayoutRecyclerViewAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.txt_pay_button)
    public void payButtonClick() {
        mPosActionListener.processPayButtonAction();
    }

    @Override
    public void showSelectCustomerErroDialog() {
        CommonAlertDialog.ShowAlertDialog(getActivity(), getString(R.string.select_user),
                getString(R.string.select_user_message), R.style.dialog_animation_fade);
    }

    @Override
    public void openPaymentDialog() {
        String name = SharedPreferenceManager.getSharedPreferenceManager(getActivity()).getSelectedCustomer();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        PaymentDialogFragment fragment = new PaymentDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.CUSTOMER_NAME_FIELD_NAME, name);
        bundle.putString(AppConstants.GRAND_TOTAL_FIELD_NAME, String.valueOf(mGrandTotalWithGst));
        fragment.setArguments(bundle);
        fragment.show(fm, "PaymentDialogFragment");

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void generateSalesInvoice(GenerateSalesInvoice generateSalesInvoice) {
        saveProductDataToCartDB(generateSalesInvoice.getmGrandTotal(),
                generateSalesInvoice.getmDiscountValue(), generateSalesInvoice.getmOutstandingValue(),
                generateSalesInvoice.getmAmountPaidByUser());
    }

    @Override
    public void callSalesInvoiceApi(List<SalesRequestData> salesRequestDataList) {
        mPosActionListener.callSalesInvoiceBulkApi(salesRequestDataList);
    }

    private void saveProductDataToCartDB(double grandTotal, double discountValue, double outStandingValue, double amountPaidByuser) {
        Log.d(TAG, "saveProductDataToCartDB: ");
        mPosActionListener.saveProductDataToCartDB(grandTotal, discountValue, outStandingValue, amountPaidByuser);

        resetCount();

        CommonAlertDialog.ShowDialog(getActivity(), getResources().getString(R.string.sales_invoice),
                getResources().getString(R.string.sales_invoice_creation_msg), R.style.dialog_animation_fade);
    }

    @Override
    public void showSessionExpiryDialog() {
        showAlertDialog();
    }

    private void showAlertDialog() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getResources().getString(R.string.session_expire));
        builder.setMessage(getResources().getString(R.string.session_expire_message));
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // continue with delete
                redirectToLogin();
            }
        });
        builder.setIcon(R.mipmap.ic_alert);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void resetCount() {
        mPosActionListener.resetProductCount();
    }

    @Override
    public void clearListAndResetViews() {
        mProductCartList.clear();
        manageTotalLayoutVisibility(false);
        notifyPosAdapter();
    }

    @Override
    public void showLoading() {
        mMaterialDialog.show();
    }

    @Override
    public void hideLoading() {
        if (mMaterialDialog.isShowing()) {
            mMaterialDialog.dismiss();
        }
    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void showOnFailureAlert() {
        if (getActivity() != null) {
            CommonAlertDialog.ShowAlertDialog(getActivity(), getResources().getString(R.string.connection_timeout_title),
                    getResources().getString(R.string.connection_timeout_message), 0);
        }
    }

    @Override
    public DashboardComponent getComponent() {
        return mDashboardComponent;
    }

    @Override
    public void onProductSuccessResponse(AllProducts body) {

    }

    @Override
    public void showLocalDBData() {
        mPosAdapter.notifyDataSetChanged();
    }

    @Override
    public void setList() {
//        readProductData();
    }

    @Override
    public void onSalesInvoiceSuccess() {

    }

    @Override
    public void notifyPosAdapterAgain(RealmList<Product> realmResults) {
        // TODO: 28/12/17 need to check filter here.
        mProductList.clear();
        mProductList.addAll(realmResults);
        notifyPosAdapter();
    }

    private void setPOSAdapter(List<Product> productList) {
        mPosAdapter = new POSAdapter(getActivity(), productList);
        mRecyclerView.setAdapter(mPosAdapter);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void refreshListItems(RefreshListEvent refreshListEvent) {
        mProductList.clear();
        mProductList.addAll(mPosActionListener.getAllProductsFromDB());
       notifyPosAdapter();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void customerNameEvent(CustomerNameEvent customerNameEvent) {
        mCustomerName.setText(customerNameEvent.getCustomerName());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void addItemCount(AddItemCountEvent addItemCountEvent) {
        mPosActionListener.manageItemForCart(addItemCountEvent);
        final RealmResults<Product> data = mPosActionListener.getAllCartProductListFromDB();
        mProductCartList.clear();
        mProductCartList.addAll(data);
        if (mProductCartList.size() > 0) {
            mRelativeCustomerCartLayout.setVisibility(View.VISIBLE);
            mEmptyCartProductPayLayout.setVisibility(View.INVISIBLE);
        } else {
            mEmptyCartProductPayLayout.setVisibility(View.VISIBLE);
            mRelativeCustomerCartLayout.setVisibility(View.INVISIBLE);
        }
        mPayLayoutRecyclerViewAdapter.setList(mProductCartList);
        notifyPayAdapter();
        notifyPosAdapter();
    }

    @Override
    public void manageTotalLayoutVisibility(boolean isVisible) {
        if (isVisible) {
            mTotalLayout.setVisibility(View.VISIBLE);
        } else {
            mTotalLayout.setVisibility(View.GONE);
        }
    }

    public void manageEmptyCartVisibility(boolean isVisible) {
        if (isVisible) {
            mEmptyCart.setVisibility(View.VISIBLE);
        } else {
            mEmptyCart.setVisibility(View.GONE);
        }

    }

    @Override
    public void setValueToNetTotalView(double total) {
        updateNetTotalToTextView(total);
    }

    @Override
    public void setValueToGrandTotalView(double grandTotalWithGst) {
        Log.d(TAG, "setValueToGrandTotalView: " + grandTotalWithGst);
        updateGrandTotalToTextView(grandTotalWithGst);
    }

    @Override
    public void showTaxDetailsToView(int taxAmount, String desc) {
        setVatPercentValue(taxAmount);
        setVatPercentDescription(desc);
    }

    private void setVatPercentDescription(String desc) {
        vatPercentLabel.setText(desc + "-");
    }

    private void setVatPercentValue(int taxAmount) {
        vatPercentValue.setText(String.valueOf(taxAmount) + "%");
    }

    private double calculateGrandTotal(double total) {
        double finalTotal = 5 * total / 100;
        mGrandTotalWithGst = finalTotal + total;
        return mGrandTotalWithGst;
    }

    public void updateGrandTotalToTextView(double grandTotalToTextView) {

        mGrandTotalWithGst = grandTotalToTextView;
        mGrandTotal.setText(String.valueOf(getResources().getString(R.string.rs) + " "
                + new DecimalFormat("#.##").format(grandTotalToTextView)));
    }

    public void updateNetTotalToTextView(double netTotal) {
        mNetTotal.setText(String.valueOf(getActivity().getResources().getString(R.string.rs) + " " + netTotal));
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        try {
            if (((MposeApplication) getActivity().getApplicationContext()) != null) {
                String name = s.toString();
                ((MposeApplication) getActivity().getApplicationContext()).setCustomerSelected(false);
                ((MposeApplication) getActivity().getApplicationContext()).setCustomerName(name);
            }
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }

    @Override
    public void updateGrandTotalToView(double value) {
        updateGrandTotalToTextView(value);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public void hideKeyboard() {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(
                    Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(mContentDashboardLayout.getWindowToken(), 0);
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        hideKeyboard();
        notifyPosAdapter();
        notifyPayAdapter();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.search_item).setVisible(true);
        menu.findItem(R.id.img_create_customer).setVisible(true);
        menu.findItem(R.id.img_filter_product).setVisible(true);
        MenuItem searchItem = menu.findItem(R.id.search_item);
        mMaterialSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mMaterialSearchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        mMaterialSearchView.setFocusable(false);
        mMaterialSearchView.setIconified(true);

        AutoCompleteTextView searchTextView = (AutoCompleteTextView) mMaterialSearchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.cursor); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
            e.printStackTrace();
        }

        int options = mMaterialSearchView.getImeOptions();
        mMaterialSearchView.setImeOptions(options | EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        addOnQueryTextListener();
    }

    private void addOnQueryTextListener() {
        final List<Product> allProductsList = mPosActionListener.getAllProductsFromDB();

        mMaterialSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchResultList = new RealmList<Product>();
                for (int i = 0; i < allProductsList.size(); i++) {
                    if (allProductsList.get(i).getItemName().toLowerCase().trim()
                            .contains(newText.toLowerCase().trim())) {
                        searchResultList.add(allProductsList.get(i));
                    }
                }
                setNewList();
                return true;
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mPosActionListener.handleOptionSelectedEvent(item);
    }

    @Override
    public void openFilterDialog() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FilterDialogFragment filterDialogFragment = new FilterDialogFragment();
        filterDialogFragment.show(fragmentManager, AppConstants.FILTER_DIALOG_FRAGMENT);
    }


    @Override
    public void createCustomer() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_create_customer);
        dialog.getWindow().getAttributes().windowAnimations = R.style.dialog_animation_slide_down_up;
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                hideKeyboard();
            }
        });

        final EditText customerName = (EditText) dialog.findViewById(R.id.ed_customer_name_dialog);
        final Button createCustButton = (Button) dialog.findViewById(R.id.txt_create_customer_button);
        createCustButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = customerName.getText().toString().trim();
                if (!TextUtils.isEmpty(name.trim())) {
                    dialog.dismiss();
                    mPosActionListener.handleCustomerCreate(name);
                } else {
                    customerName.setError(getString(R.string.invalid_customer_name_error));
                }
            }
        });
    }

    @Override
    public void showCustomerAlreadyPresentError() {
        CommonAlertDialog.ShowAlertDialog(getActivity(), getString(R.string.customer_exist),
                getString(R.string.error_customer_already_present), R.style.dialog_animation_fade);
    }

    @Override
    public void setCustomerDataToView(String customerName) {
        showSearchCustomerView();
        mSearchCustomer.setText(customerName);
        mSearchCustomer.setSelection(mSearchCustomer.getText().length());
        mSearchCustomer.setSelected(true);
        callEventToShowName(customerName);
    }

    private void showSearchCustomerView() {
        mSearchCustomer.setVisibility(View.VISIBLE);
    }


    private void callEventToShowName(String name) {
        SharedPreferenceManager.getSharedPreferenceManager(getActivity())
                .setSelectedCustomer(name);
        EventBus.getDefault().post(new CustomerNameEvent(name));
        hideKeyboard();
        ((MposeApplication) getActivity().getApplicationContext()).setCustomerSelected(true);
        SharedPreferenceManager.getSharedPreferenceManager(getActivity()).setIsCustomerSelected(true);
    }

    @Override
    public void notifyToAdapter() {
        mCustomerAdapter.notifyDataSetChanged();
    }


    @Override
    public void onSucessSaveCustomerIdToDB(String name) {
        callEventToShowName(name);
    }

    private void setCustomerAdapter() {
        mCustomerAdapter = new CustomerAdapter(getActivity(), R.layout.customer_list_row_item, mCustomerList);
        mSearchCustomer.setAdapter(mCustomerAdapter);
        mSearchCustomer.setThreshold(0);
    }

    private void showAddCustomerForFirstTime() {
        if (mCustomerList.size() > 0) {
            showSearchCustomerView();
        }
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        mSearchCustomer.showDropDown();
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final String name = parent.getAdapter().getItem(position).toString();
        if (mSearchCustomer.getText().toString().equalsIgnoreCase(name)) {
            mPosActionListener.setSelectedCustomerIdToDB(name);
        }
    }
}


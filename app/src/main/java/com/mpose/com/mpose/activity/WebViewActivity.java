package com.mpose.com.mpose.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.mpose.com.mpose.R;
import com.mpose.com.mpose.utils.AppConstants;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by lp-ritesh on 6/11/17.
 */

public class WebViewActivity extends BaseActivity {

    private static final String TAG = WebViewActivity.class.getSimpleName();

    @Bind(R.id.web_view)
    WebView mWebView;

    @Bind(R.id.proceed_print)
    Button mProceedPrint;

    @Bind(R.id.cancel_print)
    Button mCancelPrint;

    private String mExternalStorageDirectory;
    private int mInvoiceNumber;
    private File mPdfFile;
    private Dialog mPrintDialog;

    private WifiManager mWifiManager;
    String mHtmlString;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);
        mHtmlString = getIntent().getStringExtra(AppConstants.INVOICE_FORMAT);

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setLoadsImagesAutomatically(true);

        mWebView.setWebViewClient(new WebViewClient());
        mWebView.loadData(mHtmlString, "text/html; charset=UTF-8", null);

        mWifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        initPrintDialog();

    }

    private void initPrintDialog() {
        mPrintDialog = new Dialog(this);
        mPrintDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = mPrintDialog.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mPrintDialog.setContentView(R.layout.dialog_progressbar);

        mPrintDialog.setCancelable(true);
        mPrintDialog.setCanceledOnTouchOutside(false);

        mPrintDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(WebViewActivity.this);

                    alert.setMessage("Do you want to cancel printing?");

                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                        }
                    });

                    alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });

                    alert.show();
                    return true;
                }
                return false;
            }
        });
    }

    @OnClick(R.id.cancel_print)
    public void handleCancelButtonClick() {
        this.finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @OnClick(R.id.proceed_print)
    public void handleProceedClick() {
        createWebPrintJob(mWebView);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void createWebPrintJob(WebView webView) {

        // Get a PrintManager instance
        PrintManager printManager = (PrintManager) this
                .getSystemService(Context.PRINT_SERVICE);

        // Get a print adapter instance
        PrintDocumentAdapter printAdapter = webView.createPrintDocumentAdapter();

        // Create a print job with name and adapter instance
        String jobName = getString(R.string.app_name) + " Document";
        PrintJob printJob = printManager.print(jobName, printAdapter,
                new PrintAttributes.Builder().build());

        // Save the job object for later status checking
//        mPrintJobs.add(printJob);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        try {
            if (mPrintDialog != null && mPrintDialog.isShowing()) {

            } else {
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

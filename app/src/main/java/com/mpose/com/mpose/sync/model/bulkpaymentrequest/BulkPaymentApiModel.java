package com.mpose.com.mpose.sync.model.bulkpaymentrequest;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lp-ritesh on 26/7/17.
 */


public class BulkPaymentApiModel {

    @SerializedName("data")
    private List<BulkPaymentData> data;

    public List<BulkPaymentData> getData() {
        return data;
    }

    public void setData(List<BulkPaymentData> data) {
        this.data = data;
    }


}

package com.mpose.com.mpose.expense.actionlistener;

import com.mpose.com.mpose.expense.models.addexpense.CustomerExpenseModel;

import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by lp-ritesh on 21/8/17.
 */

public interface ExpenseClaimActionListener<T> {

    void setView(T t);

    RealmList<CustomerExpenseModel> getAllExpenseClaimList();

    double getTotalClaimAmount();

    void fetchSortedList();
}

package com.mpose.com.mpose.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.mpose.com.mpose.services.AutoSyncIntentService;

/**
 * Created by Anjali on 26-09-2017.
 */

public class AutoSyncBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = AutoSyncBroadcastReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive: ");
        Intent service = new Intent(context, AutoSyncIntentService.class);
        context.startService(service);
    }
}

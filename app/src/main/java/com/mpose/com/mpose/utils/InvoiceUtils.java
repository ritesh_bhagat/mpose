package com.mpose.com.mpose.utils;

import android.util.Log;

import com.mpose.com.mpose.pos.models.CustomerSalesInvoice;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by lp-ritesh on 4/8/17.
 */

public class InvoiceUtils {

    private static final String TAG = InvoiceUtils.class.getSimpleName();

    private static final int INVOICE_ID_START = 200001;

    /**
     * When called, returns the appropriate invoice id that should be assigned to invoice in process.
     *
     * @return new invoiceId which is not in DB
     */
    public static int getNewInvoiceId() {

        Realm realm = Realm.getDefaultInstance();
        RealmResults<CustomerSalesInvoice> result = realm.where(CustomerSalesInvoice.class)
                .findAllSorted(AppConstants.INVOICE_NUMBER_FIELD_NAME, Sort.DESCENDING);


        if (result.size() <= 0) {
            return INVOICE_ID_START;
        }

        CustomerSalesInvoice customerSalesInvoice = result.first();

        // happens for the very first time
        if (customerSalesInvoice == null) {
            return INVOICE_ID_START;
        } else {

            int invoiceId = -1;
            try {
                invoiceId = customerSalesInvoice.getInvoiceNumber();
            } catch (NumberFormatException e) {
                Log.d(TAG, "Some shit has gone wrong, earlier invoice id had some problem");
                // TODO how do we resolve this
            }

            return invoiceId + 1;
        }
    }
}

package com.mpose.com.mpose.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mpose.com.mpose.R;
import com.mpose.com.mpose.expense.models.addexpense.CustomerExpenseModel;

import java.text.DecimalFormat;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by abc on 8/19/2017.
 */

public class ExpenseClaimAdapter extends RecyclerView.Adapter<ExpenseClaimAdapter.ViewHolder> {


    private Context mContext;
    private Realm mRealmInstance;
    private RealmList<CustomerExpenseModel> mExpenseClaimList;
    private static final String TAG = ExpenseClaimAdapter.class.getSimpleName();


    public ExpenseClaimAdapter(Context context, RealmList<CustomerExpenseModel> expenseClaimList) {

        this.mContext = context;
        this.mRealmInstance = Realm.getDefaultInstance();
        this.mExpenseClaimList = expenseClaimList;
    }


    @Override
    public ExpenseClaimAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.sales_expense_claim_item, parent, false);
        return new ExpenseClaimAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ExpenseClaimAdapter.ViewHolder holder, int position) {
        CustomerExpenseModel expenseClaim = mExpenseClaimList.get(position);

        holder.expenseClaimID.setText(String.valueOf(expenseClaim.getExpenseID()));
        holder.txtDateOfExpense.setText(expenseClaim.getPostingDate());
        holder.txtExpenseClaimType.setText(expenseClaim.getExpenseType());
        holder.txtDescription.setText(expenseClaim.getClaimDescription());
        holder.txtAmount.setText(mContext.getString(R.string.rs) + " "
                + new DecimalFormat("#.##").format(expenseClaim.getClaimAmount()));
        if (expenseClaim.getPaymentInitialte().equals(true)) {
            if (expenseClaim.getExpenseIdSync().equals(true)) {
                holder.expenseStatus.setImageDrawable(mContext.getResources().getDrawable(R.drawable.green_tick));
            } else {
                holder.expenseStatus.setImageDrawable(mContext.getResources().getDrawable(R.drawable.red_cross));
            }
        }

    }

    @Override
    public int getItemCount() {
        return mExpenseClaimList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.txt_date_of_expense)
        TextView txtDateOfExpense;

        @Bind(R.id.txt_expense_claim_type_row_item)
        TextView txtExpenseClaimType;

        @Bind(R.id.txt_description_row_item)
        TextView txtDescription;

        @Bind(R.id.txt_amount_row_item)
        TextView txtAmount;

        @Bind(R.id.expense_claim_id_item)
        TextView expenseClaimID;

        @Bind(R.id.expense_status)
        ImageView expenseStatus;


        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
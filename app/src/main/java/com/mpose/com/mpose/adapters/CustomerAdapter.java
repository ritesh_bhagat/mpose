package com.mpose.com.mpose.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.mpose.com.mpose.R;
import com.mpose.com.mpose.pos.models.Customer;
import com.mpose.com.mpose.utils.AppConstants;

import java.util.List;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by lp-ritesh on 17/7/17.
 */

public class CustomerAdapter extends ArrayAdapter<Customer> {

    private static final String TAG = CustomerAdapter.class.getSimpleName();
    private LayoutInflater inflater;
    private RealmList<Customer> mRealmCustomerList = new RealmList<>();
    private Context mContext;
    private RealmResults<Customer> customerResult;


    public CustomerAdapter(Context context, int textResId, List<Customer> customerList) {
        super(context, textResId, customerList);
        mRealmCustomerList.addAll(customerList);
        inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    private Filter mFilter = new Filter() {

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return resultValue.toString();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults filterResults = new FilterResults();
            if (constraint == null) {
                return filterResults;
            }

            Realm realm = Realm.getDefaultInstance();

            RealmQuery<Customer> customerQuery = null;
            if (constraint.length() <= 0 || TextUtils.isEmpty(constraint.toString())) {
                customerQuery = realm.where(Customer.class);
            } else {
                customerQuery = realm.where(Customer.class)
                        .contains(AppConstants.CUSTOMER_NAME_FIELD_NAME, constraint.toString().trim(),
                                Case.INSENSITIVE);
            }

            customerResult = customerQuery.findAllSorted(AppConstants.CUSTOMER_NAME_FIELD_NAME);

            List<Customer> customerList = realm.copyFromRealm(customerResult);

            filterResults.values = customerList;
            filterResults.count = customerList.size();

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            if (results == null || results.count <= 0 || results.values == null) {
                Log.d(TAG, "there ain't no result");
                return;
            }

            clear();
            addAll((List<Customer>)results.values);
            notifyDataSetChanged();
        }
    };

    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.customer_list_row_item, null);
        }
        final Customer customer = getItem(position);
        TextView name = (TextView) view.findViewById(R.id.customer_list_name_item_row);
        TextView id = (TextView) view.findViewById(R.id.customerId);
        name.setText(customer.getCustomerName());
        id.setText(String.valueOf(customer.getCustomerId()));
        return view;
    }


    @Nullable
    @Override
    public Customer getItem(int position) {
        return super.getItem(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return mFilter;
    }


    public void setList(RealmList<Customer> list) {

    }

}

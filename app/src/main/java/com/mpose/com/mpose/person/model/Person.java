package com.mpose.com.mpose.person.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lp-ritesh on 28/6/17.
 */

public class Person {

    @SerializedName("data")
    private List<PersonData> data = null;

    public List<PersonData> getData() {
        return data;
    }

    public void setData(List<PersonData> data) {
        this.data = data;
    }

}




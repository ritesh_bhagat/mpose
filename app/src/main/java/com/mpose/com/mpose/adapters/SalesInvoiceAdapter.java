package com.mpose.com.mpose.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mpose.com.mpose.R;
import com.mpose.com.mpose.eventbus.CustomerInvoiceCallEvent;
import com.mpose.com.mpose.pos.models.CustomerSalesInvoice;

import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by lp-ritesh on 20/7/17.
 */

public class SalesInvoiceAdapter extends RecyclerView.Adapter<SalesInvoiceAdapter.ViewHolder> {


    private Context mContext;
    private Realm mRealmInstance;
    private List<CustomerSalesInvoice> mCustomerStatusInvoicesList = new ArrayList<>();

    public SalesInvoiceAdapter(Context context, List<CustomerSalesInvoice> customerStatusInvoicesList) {
        this.mContext = context;
        this.mRealmInstance = Realm.getDefaultInstance();
        this.mCustomerStatusInvoicesList = customerStatusInvoicesList;

//        getCustomerInvoiceListFromDB();
    }

    private void getCustomerInvoiceList() {
        if (mRealmInstance.isClosed()) {
            mRealmInstance = Realm.getDefaultInstance();
        }
        RealmResults<CustomerSalesInvoice> realmResults =
                mRealmInstance.where(CustomerSalesInvoice.class).findAll();
        mCustomerStatusInvoicesList.clear();
        mCustomerStatusInvoicesList.addAll(realmResults);
        sortListByTimeStamp();
        notifyDataSetChanged();
    }

    private void sortListByTimeStamp() {
        try {
            Collections.sort(mCustomerStatusInvoicesList, new Comparator<CustomerSalesInvoice>() {
                @Override
                public int compare(CustomerSalesInvoice obj1, CustomerSalesInvoice obj2) {
                    return obj2.getInvoiceDate().compareTo(obj1.getInvoiceDate());
                }
            });
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.sales_invoice_item_row, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CustomerSalesInvoice invoice = mCustomerStatusInvoicesList.get(position);
        holder.customerName.setText(invoice.getCustomerName());
        holder.payStatus.setText(invoice.getStatus());
        holder.payGrandTotal.setText(mContext.getString(R.string.rs) + " " + new DecimalFormat("#.##").format(invoice.getGrandTotal()));
        holder.invoiceNumber.setText(String.valueOf(invoice.getErpInvoiceId()));
        if (invoice.getPaymentInitialte().equals(true)) {
            if (invoice.getInvoiceSync().equals(true)) {
                holder.linearLayout.setBackgroundColor(mContext.getResources().getColor(R.color.green_300));
//                holder.invoiceSyncStatus.setImageDrawable(mContext.getResources().getDrawable(R.drawable.green_tick));
            } else {
                holder.invoiceSyncStatus.setImageDrawable(mContext.getResources().getDrawable(R.drawable.red_cross));
            }
        }
    }

    @Override
    public int getItemCount() {
        return mCustomerStatusInvoicesList.size();
    }

    public void setFilteredList(RealmList<CustomerSalesInvoice> searchResultList) {
        this.mCustomerStatusInvoicesList.clear();
        this.mCustomerStatusInvoicesList.addAll(searchResultList);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.txt_cust_name_row_item)
        TextView customerName;

        @Bind(R.id.txt_status_row_item)
        TextView payStatus;

        @Bind(R.id.txt_grand_total_row_item)
        TextView payGrandTotal;

        @Bind(R.id.txt_invoice_no_row_item)
        TextView invoiceNumber;

        @Bind(R.id.si_sync_status)
        ImageView invoiceSyncStatus;

        @Bind(R.id.layout_si)
        LinearLayout linearLayout;

//        @BindView(R.id.si_payment_status)
//        ImageView paymentStatus;


        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int invoiceNumber = mCustomerStatusInvoicesList.get(getAdapterPosition()).getInvoiceNumber();
                    double grandTotal = mCustomerStatusInvoicesList.get(getAdapterPosition()).getGrandTotal();
                    String status = mCustomerStatusInvoicesList.get(getAdapterPosition()).getStatus();
                    EventBus.getDefault().post(new CustomerInvoiceCallEvent(invoiceNumber, grandTotal, status));

                }
            });
        }
    }
}
